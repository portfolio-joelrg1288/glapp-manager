import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/models/data_model.dart';

class NotificationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFDD83A2),
        title: Text(
          'Notificaciones',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      backgroundColor: Color(0xFFF0F2F7),
      body: Center(
        child: _buildNotification(context),
      ),
    );
  }

  Widget _buildNotification(BuildContext context) {
    final dataModel = Provider.of<DataModel>(context, listen: false);
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.all(15),
      margin: EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                dataModel.notification['date'],
                style: TextStyle(
                  fontSize: 16,
                  color: Color(0xFF278B00),
                  fontWeight: FontWeight.w700,
                ),
              ),
              IconButton(
                onPressed: () => print('delete notification'),
                icon: Icon(
                  Icons.delete_forever,
                  color: Colors.black,
                ),
              ),
            ],
          ),
          Text(
            dataModel.notification['body'],
            style: TextStyle(
              fontSize: 13,
              color: Color(0xFF343F4B),
              fontWeight: FontWeight.w400,
            ),
          ),
          Text(
            'Lugar: ${dataModel.notification['place']}',
            style: TextStyle(
              fontSize: 13,
              color: Color(0xFF343F4B),
              fontWeight: FontWeight.w400,
            ),
          ),
          Text(
            'Hora ${dataModel.notification['hour']}',
            style: TextStyle(
              fontSize: 13,
              color: Color(0xFF343F4B),
              fontWeight: FontWeight.w400,
            ),
          )
        ],
      ),
    );
  }
}