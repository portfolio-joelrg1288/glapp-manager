import 'package:flutter/material.dart';
import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/utils/constants.dart';
import 'package:glapp_manager/src/models/filters_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/models/professional_model.dart';
import 'package:glapp_manager/src/models/services_tax_model.dart';
import 'package:glapp_manager/src/providers/billing_provider.dart';
import 'package:glapp_manager/src/providers/services_tax_provider.dart';

class IncomesPage extends StatefulWidget {
  @override
  _IncomesPageState createState() => _IncomesPageState();
}

class _IncomesPageState extends State<IncomesPage> with SingleTickerProviderStateMixin {
  Size screenSize;
  FiltersModel filtersModel;
  ProfessionalModel professionalModel;
  List<TextEditingController> _controllers2 = [];
  TabController _tabController;
  BillingProvider billingProvider = BillingProvider();
  bool _isInfoReady = false;
  bool _isBillingAvailable = false;
  bool _isBillingLoaded = false;
  bool _isServicesAvailable = false;
  bool _isServicesLoaded = false;
  bool _confirmDelete = false;
  ServicesTaxProvider servicesTaxProvider = ServicesTaxProvider();
  List<ServicesTaxModel> _servicesList = new List();
  Map<dynamic, double> _billings = new Map();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    if(!_tabController.indexIsChanging) _callMethod();
    _tabController.addListener(() { 
      if(_tabController.indexIsChanging) _callMethod();
    });
  }

  @override
  void dispose() {
    _reset();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    filtersModel = Provider.of<FiltersModel>(context);
    professionalModel = Provider.of<ProfessionalModel>(context);
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: DefaultTabController(
        length: 2,
        child: Column(
          children: <Widget>[
            TabBar(
              isScrollable: false,
              controller: _tabController,
              tabs: <Widget>[
                _isInfoReady ? Tab(text: 'Tarifa servicios') : Tab(text: ''),
                _isInfoReady ? Tab(text: 'Facturación') : Tab(text: 'Cargando...')
              ],
              labelColor: Color(0xFF343F4B),
              indicatorColor: Color(0xFFDD84A2),
              labelStyle: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w700,
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.white,
                child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _tabController,
                  children: [
                    _isServicesAvailable ?_buildServicesTileList() : Center(child: Container(child: Text('No hay informacion disponible'))),
                    _isBillingAvailable ? _buildBilling(context) : Center(child: Container(child: Text('No hay informacion disponible')))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildServicesTileList() {
    return ListView.builder(
      itemCount: _servicesList.length,
      itemBuilder: (BuildContext context, int index) {
        return _serviceTile(
          name: _servicesList[index].principalService, 
          subName: _servicesList[index].serviceName,
          id: _servicesList[index].id,
          man: _servicesList[index].man,
          woman: _servicesList[index].woman,
          child: _servicesList[index].child,
          index: index
        );
      }
    );
  }

  Widget _serviceTile({String name, String subName, String id, dynamic man, dynamic woman, dynamic child, int index}) =>
    Dismissible(
      confirmDismiss: (direction) async {
        await _confirmDismiss(id);
        if(_confirmDelete) {
          setState(() { 
            _servicesList.removeAt(index); 
            _getServices();
          });
          _confirmDelete = false;
        }
        return;
      },
      direction: DismissDirection.endToStart,
      key: ValueKey(id),
      background: Container(
        color: Color(0xFFA3DCBE),
        child: Padding(
          padding: const EdgeInsets.only(right: 32.0),
          child: Icon(
            Icons.delete,
            color: Color(0xFFDD84A2),
            size: 32,
          ),
        ),
        alignment: Alignment.centerRight,
      ),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5),
        color: Colors.white,
        child: ListTile(
          title: Text(name),
          subtitle: Text(subName),
          trailing: Text('Hombre: \$$man\nMujer: \$$woman\nNiño: \$$child')
        ),
      ),
    );

  Widget _buildBilling(BuildContext context) {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              physics: BouncingScrollPhysics(),
              child: Column(
                children: <Widget>[
                  Container(
                    color: Colors.white,
                    width: double.infinity,
                    child: GenericSelect(
                      showHint: true,
                      labelText: 'Año',
                      items: filtersModel.years,
                      selected: professionalModel.year,
                      changeDropButton: filtersModel.years.length > 1
                          ? (val) => professionalModel.year = val
                          : null,
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    color: Color(0xFFEFF2F7),
                    padding: EdgeInsets.all(15),
                    child: Column(children: _buildMonthlyIncomes(context)),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _buildMonthlyIncomes(BuildContext context) {
    List<Widget> result = [];
    Constants.of(context).months.forEach((month) {
      final index = Constants.of(context).months.indexOf(month);
      final income = professionalModel.incomes[index] ?? '';
      _controllers2.add(new TextEditingController(text: '$income'));
      result.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: screenSize.width * 0.4,
            child: Text(
              month,
              style: TextStyle(
                fontSize: 12,
                color: Color(0xFF47525E),
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          Container(
            width: screenSize.width * 0.5,
            child: TextField(
              readOnly: true,
              onTap: () {
                professionalModel.month = index;
                Navigator.pushNamed(context, 'month-incomes');
              },
              controller: _controllers2.last,
              decoration: InputDecoration(
                filled: true,
                isDense: true,
                fillColor: Colors.white,
                enabledBorder: InputBorder.none,
                focusedBorder: InputBorder.none,
                labelStyle: TextStyle(color: Colors.grey, fontSize: 12),
              ),
              style: TextStyle(color: Colors.black, fontSize: 12),
            ),
          ),
        ],
      ));
      result.add(SizedBox(height: 10));
    });
    result.removeLast();
    return result;
  }

  void _callMethod() {
    switch(_tabController.index) {
      case 0:
        _getServices();
        break;
      case 1:
        _getBilling();
        break;
    }
  }

  void _getBilling() async {
    setState(() { _isInfoReady = false; });
    if(!_isBillingLoaded) {
      final resp = await billingProvider.getBilling(filtersModel.years[0].toString());
      if(!resp.success) {
        showAlert(context, 'Error', resp.message);
        setState(() {
          _isBillingAvailable = false;
          _isInfoReady = true;
        });
      } else {
        var aux = resp.body['data'];
        if(aux.length > 0) {
          _showBilling(aux['meses']);
        } else {
          setState(() {
            _isBillingAvailable = false;
            _isInfoReady = true;
          });
        }
      }
    } else _showBilling([]);
  }

  void _showBilling(dynamic resp) {
    if(resp.length > 0) {
      _billings.clear();
      for(int i = 0; i < resp.length; i++) 
        _billings.addAll({i : resp[i]['ingreso'].toDouble()});
    }

    setState(() {
      professionalModel.incomes.clear();
      professionalModel.incomes.addAll(_billings);  
      _isBillingAvailable = true;  
      _isBillingLoaded = true; 
      _isInfoReady = true; 
    });
  }

  Future<void> _getServices() async {
    setState(() { _isInfoReady = false; });
    if(!_isServicesLoaded) {
      final resp = await servicesTaxProvider.getServices();
      if(resp.success) {
        if(resp.body['data'].length > 0) {
          resp.body['data'].forEach((data) {
            _servicesList.add(ServicesTaxModel.fromJsonMap(data));
         });
        _isServicesAvailable = true;
        } else 
          _isServicesAvailable = false;
        setState(() { _isInfoReady = true; });
        _isServicesLoaded = true;
      } else {
        Navigator.of(this.context).pop(true);
        showAlert(this.context, 'Error', resp.message);      
      }
    } else setState(() { _isInfoReady = true; });
  }

  Future<void> _confirmDismiss(String id) async {
    return await showDialog(
      context: this.context,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.white,
          content: Text("¿Está seguro que desea eliminar el servicio?", textAlign: TextAlign.center),
          actions: <Widget>[
            FlatButton(
              onPressed: () => Navigator.of(context).pop(false),
              child: Text("Volver", style: Theme.of(context).textTheme.button),
            ),
            FlatButton(
                onPressed: () async { 
                  final resp = await servicesTaxProvider.deleteService(id);

                  if(resp.success) {
                    _confirmDelete = true;
                    Navigator.of(context).pop(false);
                    showAlert(context, 'Exito', 'Servicio Eliminado');
                  } else {
                    Navigator.of(context).pop(false);
                    showAlert(context, 'Error', resp.message);
                  }
                },
                child: Text("Eliminar", style: Theme.of(context).textTheme.button)),
          ],
        );
      },
    );
  } 

  void _reset() {
    _billings.clear();
    _servicesList.clear();
    _isInfoReady = false;
    _isBillingLoaded = false;
    _isBillingAvailable = false;
    _isServicesAvailable = false;
    _isServicesLoaded = false;
  }
}
