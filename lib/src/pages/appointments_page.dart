import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/providers/appointments_provider.dart';
import 'package:glapp_manager/src/providers/user_actions_provider.dart';
import 'package:provider/provider.dart';
import 'package:collection/collection.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/intl.dart';

import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/utils/constants.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/models/filters_model.dart';
import 'package:glapp_manager/src/models/business_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/models/menu_item_model.dart';
import 'package:glapp_manager/src/models/appointment_model.dart';

class AppointmentsPage extends StatefulWidget {
  @override
  _AppointmentsPageState createState() => _AppointmentsPageState();
}

class _AppointmentsPageState extends State<AppointmentsPage> {
  String lastDay = '';
  CalendarController _calendarController;
  ScrollController _scrollController;
  Map<String, dynamic> appointments = {};
  double _scrollPosition;
  _scrollListener() {
    _scrollPosition = _scrollController.position.pixels;
    setState(() {
      _calendarController.setSelectedDay(_selectedDayList(_scrollPosition));
    });
  }
  AppointmentsProvider appointmentsProvider = new AppointmentsProvider();
  bool _isLoading = false;
  final UserActionsProvider userActionsProvider = new UserActionsProvider();
  Map<dynamic, String> _employees = {'0': 'Todos los empleados'};
  String _selectedtedEmployee = '0';

  @override
  void initState() {
    _readAppointments();
    _calendarController = CalendarController();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _calendarController.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final dataModel = Provider.of<DataModel>(context);
    final filtersModel = Provider.of<FiltersModel>(context);
    final businessModel = Provider.of<BusinessModel>(context);
    final menuItemsModel = Provider.of<MenuItemModel>(context);
    final appointmentModel = Provider.of<AppointmentModel>(context);
    changeStatusDark();
    Map<DateTime, List<dynamic>> _events = new Map();
    List dateList = new List();
    for (var item in appointments.entries) {
      String date = item.key;
      String sDate = date.substring(0,4) + '-' + date.substring(5, 7) + '-' + date.substring(8);
      DateTime fDate = DateTime.parse(sDate);
      _events[fDate] = [""];
      dateList.add(sDate);
    }
    return (_isLoading)
    ? Center(child: CircularProgressIndicator(),)
    :Column(
      children: <Widget>[
        if (!dataModel.isProfessional)
          Container(
            width: double.infinity,
            child: GenericSelect(
              labelText: 'Empleado',
              color: Colors.transparent,
              showHint: true,
              items: _employees,
              selected: _selectedtedEmployee,
              changeDropButton: (value) {
                setState(() {
                  _selectedtedEmployee = value;
                });
              },
            ),
          ),
        if (dataModel.isProfessional) SizedBox(height: 10),
        Column(
          children: <Widget>[
            TableCalendar(
              locale: 'es_ES',
              events: _events,
              weekendDays: [],
              headerVisible: false,
              initialCalendarFormat: CalendarFormat.week,
              calendarStyle: CalendarStyle(
                highlightSelected: true,
                selectedColor: Colors.blue,
                todayColor: Colors.transparent,
                todayStyle: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.w700,
                ),
                markersMaxAmount: 1,
                markersColor: Colors.blue,
              ),
              calendarController: _calendarController,
              onDaySelected: (date, events) {
                _goToElement(date);
              },
            ),
            RawMaterialButton(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              elevation: 0.0,
              fillColor: Colors.blue,
              splashColor: Colors.blueAccent,
              constraints: BoxConstraints(
                minHeight: 5.0,
                maxHeight: 5.0,
                minWidth: 50.0,
                maxWidth: 50.0
              ),
              onPressed: () {},
              shape: const StadiumBorder(),
            )
          ],
        ),
        Expanded(
          child: Container(
            color: Color(0xFFe6e8eb),
            child: SingleChildScrollView(
              controller: _scrollController,
              child: Column(
                children: _buildAppointments(appointmentModel, menuItemsModel),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _readAppointments() async {
    final ResponseModel resp = await appointmentsProvider.getAppointments();
    List<dynamic> aux = [];
    setState(() {
      _isLoading = true;
    });
    final res = await userActionsProvider.getEmployees();
    for (var e in res.body['empleados']) {
      _employees.putIfAbsent(e['id'], () => e['nombre']);
    }   
    for (var dia in resp.body['reservas']) {
      for (var reserva in dia['reserva']) {
        int _totalTime = 0;
        List _serviceArray = [];
        String sDate = dia['fechaReserva'].substring(6) + '-' + dia['fechaReserva'].substring(3, 5) + '-' + dia['fechaReserva'].substring(0, 2) + ' 00:00:00.000';
        DateTime fDate = DateTime.parse(sDate);
        int _totalPrice = 0;
        for (var servicio in reserva['services']) {
          _totalTime += servicio['tiempo'];
          _serviceArray.add(servicio['nombre']);
          _totalPrice += servicio['precio'];
        }
        var appointment = new AppointmentModel()
          ..appointmentId = reserva['id_cita'] == null ? reserva['id'] : reserva['id_cita']
          ..payment = reserva['payment']
          ..place = reserva['address']
          ..status = traslateFromSpanishStatus(reserva['status'])
          ..client = reserva['client']
          ..endDate = fDate.toIso8601String()
          ..startDate = fDate.toIso8601String()
          ..startHour = reserva['startDate']
          ..endHour = _endTimeSum(reserva['startDate'], _totalTime)
          ..serviceList = _serviceArray
          ..totalPrice = _totalPrice
          ..discountPrice = reserva['total']
          ..comments = reserva['comments']
          ..image = reserva['image']
          ..employeeId = reserva['id_empleado']
          ..employeeName = reserva['nombre_empleado'];
        aux.add(json.decode(json.encode(appointment)));
      }
    }
    this.setState(() {
      appointments = groupBy(aux, (obj) => obj['startDate'].split('T')[0]);
      // SORT APPOINTMENTS
      final sortedAppointments = Map.fromEntries(appointments.entries.toList()
        ..sort((e1,e2) => e1.key.compareTo(e2.key)));
      appointments.clear();
      appointments = Map<String,dynamic>.from(sortedAppointments);
      // END SORT APPOINTMENTS
      _scrollController = ScrollController(
        initialScrollOffset: _startListPosition(DateTime.now()),
      );
      _scrollController.addListener(_scrollListener);
      _isLoading = false;
    });
  }

  List<Widget> _buildAppointments(
      AppointmentModel appointmentModel, MenuItemModel menuItemsModel) {
    List<Widget> result = [SizedBox(height: 5)];
    lastDay = '';
    appointments.keys.forEach((day) => appointments[day].forEach((item) {
      if (menuItemsModel.selected == 'all') {
        if (_employees[_selectedtedEmployee] == 'Todos los empleados') 
          result.add(_buildAppointment(item, day, appointmentModel));
        else if(_employees[_selectedtedEmployee] == item['employeeName'])
          result.add(_buildAppointment(item, day, appointmentModel));
      } else if(menuItemsModel.selected == item['status']) {
        if (_employees[_selectedtedEmployee] == 'Todos los empleados') 
          result.add(_buildAppointment(item, day, appointmentModel));
        else if(_employees[_selectedtedEmployee] == item['employeeName'])
          result.add(_buildAppointment(item, day, appointmentModel));
      }
    }));
    return result;
  }

  Widget _buildAppointment(
    Map<String, dynamic> appointment,
    String day,
    AppointmentModel appointmentModel,
  ) {
    bool showDay = false;
    String today = day.split('-')[2];
    if (lastDay.length == 0) {
      lastDay = today;
      showDay = true;
    } else {
      if (lastDay != today) {
        lastDay = today;
        showDay = true;
      } else {
        showDay = false;
      }
    }
    DateTime endDate = DateTime.parse(appointment['endDate']);
    DateTime startDate = DateTime.parse(appointment['startDate']);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment:
          showDay ? MainAxisAlignment.spaceBetween : MainAxisAlignment.end,
      children: <Widget>[
        if (showDay)
          Container(
            width: MediaQuery.of(context).size.width * 0.15,
            child: Column(
              children: <Widget>[
                Text(
                  lastDay,
                  style: TextStyle(
                    fontSize: 26,
                    color: Color(0xFF5A6978),
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  Constants.of(context)
                      .days[startDate.weekday - 1]
                      .substring(0, 3)
                      .replaceAll('é', 'e')
                      .replaceAll('á', 'a'),
                  style: TextStyle(
                    fontSize: 18,
                    color: Color(0xFF969FAA),
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ),
        Container(
          margin: EdgeInsets.only(bottom: 15),
          width: MediaQuery.of(context).size.width * 0.83,
          padding: EdgeInsets.only(left: 5, bottom: 5, top: 3, right: 5),
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              left: BorderSide(
                width: 2,
                color: getAppointmentStatusColor(appointment['status']),
              ),
            ),
          ),
          child: RawMaterialButton(
            onPressed: appointment['status'] == 'cancelled'
                ? null
                : () {
                    appointmentModel
                      ..appointmentId = appointment['appointmentId']
                      ..payment = appointment['payment']
                      ..place = appointment['place']
                      ..status = appointment['status']
                      ..rating = appointment['rating']
                      ..client = appointment['client']
                      ..service = appointment['service']
                      ..endDate = appointment['endDate']
                      ..startDate = appointment['startDate']
                      ..startHour = appointment['startHour']
                      ..endHour = appointment['endHour']
                      ..serviceList = appointment['serviceList']
                      ..totalPrice = appointments['totalPrice']
                      ..discountPrice = appointment['discountPrice']
                      ..comments = appointment['comments']
                      ..image = appointment['image']
                      ..employeeName = appointment['employeeName'];
                    Navigator.of(context).pushNamed('appointment-page');
                  },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width * 0.45,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        translateAppointmentStatus(appointment['status']),
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(
                        'Servicio: ${_servicesFormat(appointment['serviceList'])}',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFF343F4B),
                          fontWeight: FontWeight.w300,
                        ),
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                      Text(
                        'Cliente: ${appointment['client']}',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFF343F4B),
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                      Text(
                        '${appointment['startDate'].split('T')[0]}',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color(0xFF343F4B),
                          fontWeight: FontWeight.w300,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        '${appointment['startHour']}',
                        style: TextStyle(
                          fontSize: 18,
                          color: Color(0xFF278B00),
                          fontWeight: FontWeight.w700,
                        ),
                        textAlign: TextAlign.end,
                      ),
                      Text(
                        '${appointment['endHour']}',
                        style: TextStyle(
                          fontSize: 14,
                          color: Color(0xFF278B00),
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.end,
                      ),
                      SizedBox(height: 5),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Image(
                            width: 20,
                            color: appointment['payment'] == 'T'
                                ? Colors.black
                                : Colors.black12,
                            image: AssetImage('assets/icons/card.png'),
                          ),
                          SizedBox(width: 5),
                          Image(
                            width: 20,
                            color: appointment['payment'] == 'E'
                                ? Colors.black
                                : Colors.black12,
                            image: AssetImage('assets/icons/cash.png'),
                          ),
                          SizedBox(width: 5),
                          Image(
                            width: 20,
                            color: appointment['status'] == 'finished'
                                ? Colors.black
                                : Colors.black12,
                            image: AssetImage('assets/icons/star.png'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  double _startListPosition(DateTime todayDate){
    String nDate = DateFormat("yyyy-MM-dd").format(DateTime.parse(todayDate.toString()));
    int total = 0;
    for (var item in appointments.entries) {
      if (item.key == nDate) break;
      total += item.value.length;
    }
    return 95.0 * total;
  }

  String _endTimeSum(String startHour, int endHour) {
    int hour = int.parse(startHour.substring(0, 2));
    int minutes = int.parse(startHour.substring(3));
    return '${hour + endHour~/60}:${(endHour%60 == 0) ? '00' :minutes + endHour%60}';
  }

  String _servicesFormat(List services) {
    String _formatedServices = '';
    services.forEach((element) {_formatedServices += '$element ';});
    return _formatedServices;
  }

  DateTime _selectedDayList(double position){
    int total = 0;
    for (var item in appointments.entries) {
      if (95.0 * total >= position) {
        String date = item.key;
        String sDate = date.substring(0,4) + '-' + date.substring(5, 7) + '-' + date.substring(8);
        DateTime fDate = DateTime.parse(sDate);
        return fDate;
      }
      total += item.value.length;
    }
    return DateTime.now();
  }

   void _goToElement(DateTime date) {
    String nDate = DateFormat("yyyy-MM-dd").format(DateTime.parse(date.toString()));
    int total = 0;
    for (var item in appointments.entries) {
      if (item.key == nDate) break;
      total += item.value.length;
    }
    if (_scrollController.hasClients) {
      _scrollController.animateTo(95.00 * total,
      duration: const Duration(milliseconds: 300),
      curve: Curves.easeOut
      );
    }
  }

}
