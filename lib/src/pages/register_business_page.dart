import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import "package:google_maps_webservice/places.dart";
import 'package:email_validator/email_validator.dart';

import 'package:glapp_manager/src/providers/user_actions_provider.dart';
import 'package:glapp_manager/src/utils/validators.dart';
import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/models/error_model.dart';
import 'package:glapp_manager/src/models/filters_model.dart';
import 'package:glapp_manager/src/models/business_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/widgets/country_fields.dart';
import 'package:glapp_manager/src/models/country_field_model.dart';
import 'package:glapp_manager/src/providers/auth_provider.dart';
import 'package:glapp_manager/src/google_places/flutter_google_places.dart';
import 'package:glapp_manager/src/google_places/src/flutter_google_places.dart';

class RegisterBussinesPage extends StatelessWidget {
  String _userCity;
  String _placeId;
  ErrorModel errorModel;
  FiltersModel filtersModel;
  BusinessModel businessModel;
  CountryFieldModel countryFieldModel;
  AuthProvider authProvider = AuthProvider();
  UserActionsProvider userActionsProviders = UserActionsProvider();
  final FocusNode _rutFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _addressFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _countryFocus2 = FocusNode();
  final FocusNode _countryFocus1 = FocusNode();
  final FocusNode _legalNameFocus = FocusNode();
  final FocusNode _commercialNameFocus = FocusNode();
  final FocusNode _repeatPasswordFocus = FocusNode();
  final List<String> _colombianCities = ['Cali', 'Bogotá', 'Medellín'];
  final TextEditingController _legalNameController = TextEditingController();
  final TextEditingController _commercialNameController =
      TextEditingController();
  final TextEditingController _rutController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _repeatPasswordController =
      TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _countryCustomFieldController1 =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    _rutFocus.addListener(_rutFocusChange);
    _emailFocus.addListener(_emailFocusChange);
    errorModel = Provider.of<ErrorModel>(context);
    Size _screenSize = MediaQuery.of(context).size;
    _addressFocus.addListener(_addressFocusChange);
    _passwordFocus.addListener(_passwordFocusChange);
    _phoneFocus.addListener(_phoneNumberFocusChange);
    filtersModel = Provider.of<FiltersModel>(context);
    final dataModel = Provider.of<DataModel>(context);
    _legalNameFocus.addListener(_legalNameFocusChange);
    businessModel = Provider.of<BusinessModel>(context);
    countryFieldModel = Provider.of<CountryFieldModel>(context);
    _repeatPasswordFocus.addListener(_repeatPasswordFocusChange);
    _commercialNameFocus.addListener(_commercialNameFocusChange);
    getPasswordController(_passwordController);
    getContextProvider(context);
    changeStatusDark();
    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              IconButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () => Navigator.of(context).pop(),
                icon: Icon(Icons.arrow_back_ios, color: Color(0xFF515151)),
              ),
              Center(
                child: Image(
                  width: 60,
                  image: AssetImage('assets/images/logo-3.png'),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 30),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(left: 20),
                        child: Text(
                          'CREA TU CUENTA DE LOCAL COMERCIAL',
                          style: TextStyle(
                            fontSize: _screenSize.height * 0.025,
                            color: Colors.black,
                            fontFamily: 'BebasNeue',
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      _buildLegalNameInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildCommercialNameInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildRutInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildBusinessSelector(context, _screenSize),
                      SizedBox(height: 30),
                      _buildEmailInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildPasswordInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildConfirmPasswordInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildPhoneInput(context, _screenSize),
                      SizedBox(height: 25),
                      _buildAddressInput(context, _screenSize),
                      SizedBox(height: 15),
                      CountryFields(
                          _countryFocus1,
                          _countryFocus2,
                          _countryCustomFieldController1,),
                      SizedBox(height: 30),
                      _buildTermsCheckbox(),
                      _buildPrivacyCheckbox(),
                    ],
                  ),
                ),
              ),
              ButtonGray(
                margin: 60,
                text: 'REGISTRARSE',
                action: () {
                  dataModel.isProfessional = false;
                  _signup(context);
                }
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLegalNameInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        focusNode: _legalNameFocus,
        controller: _legalNameController,
        onChanged: legalNameValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) =>
            onFocusChange(context, _legalNameFocus, _commercialNameFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.name,
          labelText: 'Nombre legal de tu negocio',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildCommercialNameInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _commercialNameController,
        focusNode: _commercialNameFocus,
        onChanged: commercialNameValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) =>
            onFocusChange(context, _commercialNameFocus, _rutFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.lastName,
          labelText: 'Nombre comercial de tu negocio',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildRutInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _rutController,
        focusNode: _rutFocus,
        onChanged: rutValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _rutFocus, _addressFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.rut,
          labelText: 'RUT / NIT / ID Fiscal',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildAddressInput(BuildContext context, Size screen) {
    return GestureDetector(
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: TextField(
          enabled: false,
          controller: _addressController,
          focusNode: _addressFocus,
          onChanged: addressValidator,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            isDense: true,
            errorText: errorModel.address,
            labelText: 'Dirección',
            labelStyle:
                TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 1,
                color: Color(0xFF8492A6).withOpacity(0.5),
              ),
            ),
          ),
          style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
          keyboardType: TextInputType.text,
        ),
      ),
      onTap: () async {
        String _countryPrefix = '';
        _countryPrefix = getCountryPrefix(businessModel.prefix);
        Prediction prediction = await PlacesAutocomplete.show(
          context: context,
          apiKey: 'AIzaSyBB4T59gcXepqvOWCO34dKOnbntyATO3r4',
          language: "es",
          mode: Mode.overlay,
          components: [
            new Component(Component.country, '$_countryPrefix')]);

        if (prediction != null) {
          _placeId = prediction.placeId;
          _addressController.text = prediction.description.toString();
          _cityFinder(prediction.description.toString());
          _getLocalities();
          _customField(_countryPrefix);
        } else {
          _addressController.text = '';
        }
      },
    );
  }

  void _customField(String countryPrefix) {
    if (countryPrefix == 'ar') {
      countryFieldModel.field = 'Provincia';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    } else if (countryPrefix == 'cl') {
      countryFieldModel.field = 'Region';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    } else if (countryPrefix == 'co') {
      _customColombianCities();
    }
  }

  void _customColombianCities() {
    if (this._userCity == 'Bogotá') {
      countryFieldModel.field = 'Localidad';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    } else if (this._userCity == 'Cali') {
      countryFieldModel.field = 'Comuna';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    } else if (this._userCity == 'Medellín') {
      countryFieldModel.field = 'Comuna';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    }
  }

  void _getLocalities() async {
    String t = businessModel.prefix;

    final resp = await userActionsProviders.getLocalities(t.substring((t.length - t.length) + 1, t.length));
    if(resp.body['data'] != null) {
      final res = resp.body['data'];
      countryFieldModel.localities = res[0]['comunas'];
      countryFieldModel.listValue = countryFieldModel.localities[0]['nombre'];
    }
  }

  void _cityFinder(String _enteredAddress) {
    List<String> _splitAddress = _enteredAddress.split(',');
    List<String> _newSplitAddress = List<String>();
    _splitAddress.forEach((item) {
      _newSplitAddress.add(item.trim());
    });
    _colombianCities.forEach((city) {
      if (_newSplitAddress.contains(city)) {
        this._userCity = city;
      }
    });
  }

  Widget _buildBusinessSelector(BuildContext context, Size screenSize) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.05),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Tipo de Negocio',
            style: TextStyle(color: Colors.grey),
          ),
          Align(
            alignment: Alignment.center,
            child: GenericSelect(
              showHint: true,
              horizontalPadding: 0,
              width: screenSize.width * 0.9,
              labelText: 'Tipo de negocio',
              selected: businessModel.businessType,
              items: filtersModel.allBusinessTypes,
              changeDropButton: (value) => businessModel.businessType = value,
            ),
          ),
          Container(
            height: 1,
            width: double.infinity,
            color: Color(0xFF8492A6).withOpacity(0.5),
          ),
        ],
      ),
    );
  }

  Widget _buildEmailInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _emailController,
        focusNode: _emailFocus,
        onChanged: emailValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _emailFocus, _passwordFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.email,
          labelText: 'Mail',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.emailAddress,
      ),
    );
  }

  Widget _buildPasswordInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _passwordController,
        obscureText: true,
        focusNode: _passwordFocus,
        onChanged: passwordValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) =>
            onFocusChange(context, _passwordFocus, _repeatPasswordFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.password,
          labelText: 'Contraseña',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.visiblePassword,
      ),
    );
  }

  Widget _buildConfirmPasswordInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _repeatPasswordController,
        obscureText: true,
        focusNode: _repeatPasswordFocus,
        onChanged: repeatPasswordValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) =>
            onFocusChange(context, _repeatPasswordFocus, _phoneFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.repeatPassword,
          labelText: 'Repite contraseña',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.visiblePassword,
      ),
    );
  }

  Widget _buildPhoneInput(BuildContext context, Size screen) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Teléfono',
            style: TextStyle(color: Colors.grey),
          ),
          SizedBox(
            height: screen.height * 0.01,
          ),
          PhoneSelector(
              initialValue: businessModel.prefix,
              changeDropButton: (value) => businessModel.prefix = value,
              onSubmittedTextField: (t) => onFocusChange(context, _phoneFocus, _addressFocus),
              keyboardType: TextInputType.number,
              changeTextField: phoneNumberValidator,
              errorText: errorModel.phoneNumber,
              labelText: 'Teléfono',
              focusNode: _phoneFocus,
              controller: _phoneNumberController),
        ],
      ),
    );
  }

  Widget _buildTermsCheckbox() {
    return CheckboxListTile(
      onChanged: (value) => print(value),
      dense: true,
      value: true,
      controlAffinity: ListTileControlAffinity.leading,
      title: Text(
        'Acepto términos y condiciones',
        style: TextStyle(
          decoration: TextDecoration.underline,
          color: Color(0xFF47525E),
          fontSize: 12,
          fontWeight: FontWeight.w400,
        ),
      ),
      checkColor: Colors.white,
      activeColor: Color(0xFF8290A5),
    );
  }

  Widget _buildPrivacyCheckbox() {
    return CheckboxListTile(
      onChanged: (value) => print(value),
      dense: true,
      value: true,
      controlAffinity: ListTileControlAffinity.leading,
      title: Text(
        'He leído y acepto la política de privacidad',
        style: TextStyle(
          decoration: TextDecoration.underline,
          color: Color(0xFF47525E),
          fontSize: 12,
          fontWeight: FontWeight.w400,
        ),
      ),
      checkColor: Colors.white,
      activeColor: Color(0xFF8290A5),
    );
  }

  void _legalNameFocusChange() {
    if (_legalNameController.text.trim() == '' && !_legalNameFocus.hasFocus) 
      errorModel.name = 'El nombre legal es requerido';
  }

  void _commercialNameFocusChange() {
    if (_commercialNameController.text.trim() == '' &&
        !_commercialNameFocus.hasFocus) 
      errorModel.lastName = 'El nombre comercial es requerido';
  }

  void _addressFocusChange() {
    if (_addressController.text.trim() == '' && !_addressFocus.hasFocus)
      errorModel.address = 'La dirección es requerida';
  }

  void _rutFocusChange() {
    if (_rutController.text.trim() == '' && !_rutFocus.hasFocus) 
      errorModel.rut = 'El RUT es requerido';
  }

  void _emailFocusChange() {
    if (_emailController.text.trim() == '' && !_emailFocus.hasFocus) 
      errorModel.email = 'El correo es requerido';
  }

  void _passwordFocusChange() {
    if (_passwordController.text.trim() == '' && !_passwordFocus.hasFocus) 
      errorModel.password = 'La contraseña es requerida';
    else if(_passwordController.text.trim().length < 5) 
      errorModel.password = 'La contraseña debe ser de mínimo 6 carácteres';
  }

  void _repeatPasswordFocusChange() {
    if (_repeatPasswordController.text.trim() == '' &&
        !_repeatPasswordFocus.hasFocus) 
      errorModel.repeatPassword = 'La contraseña es requerida';
  }

  void _phoneNumberFocusChange() {
    if (_phoneNumberController.text.trim() == '' && !_phoneFocus.hasFocus) 
      errorModel.phoneNumber = 'El número de teléfono es requerido';
  }

  void _signup(BuildContext context) async {
    if(_allIsFilled()) { 
      final res = await authProvider.signupLocal(_buildMap());
      if (res.success)
        Navigator.pushNamed(context, 'register-success');
      else {
        Navigator.pop(context);
        showAlert(context, 'Error', res.body['message']);
      }
    } else showAlert(context, 'Error', 'Hay campos incorrectos o vacios');
  }

  Map<String, String> _buildMap() {
    return {
      'nombre_legal': this._legalNameController.text,
      'nombre_comercial': this._commercialNameController.text,
      'rut': this._rutController.text,
      'tipo_negocio': _getBusinessType(),
      'email': this._emailController.text,
      'ciudad': getCity(getCountryPrefix(businessModel.prefix), this._userCity,
          _addressController.text),
      'localidad': countryFieldModel.listValue,
      'detalle_direccion': this._countryCustomFieldController1.text,
      'direccion': this._addressController.text,
      'password': this._passwordController.text,
      'country': getCountry(getCountryPrefix(businessModel.prefix)),
      'telefono': this._phoneNumberController.text,
      'place_id' : this._placeId,
    };
  }

  String _getBusinessType() {
    final int index = businessModel.businessType;
    return filtersModel.allBusinessTypes[index];
  }

  bool _allIsFilled() {
    return (_legalNameController.text.trim() != '' && 
     _commercialNameController.text.trim() != '' && 
     _rutController.text.trim() != '' && 
     _emailController.text.trim() != '' && 
     _countryCustomFieldController1.text.trim() != '' &&
     _addressController.text.trim() != '' && 
     _passwordController.text.trim() != '' &&
     _repeatPasswordController.text.trim() != '' &&
     _phoneNumberController.text.trim() != '' &&
     EmailValidator.validate(_emailController.text.trim()) && 
     _passwordController.text.trim().length > 5 && 
     _passwordController.text.trim() == _repeatPasswordController.text.trim()) ? 
     true : false;
  }
}