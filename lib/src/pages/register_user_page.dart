import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:email_validator/email_validator.dart';

import 'package:glapp_manager/src/providers/user_actions_provider.dart';
import 'package:glapp_manager/src/utils/validators.dart';
import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/models/error_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/widgets/country_fields.dart';
import 'package:glapp_manager/src/models/professional_model.dart';
import 'package:glapp_manager/src/models/country_field_model.dart';
import 'package:glapp_manager/src/providers/auth_provider.dart';
import 'package:glapp_manager/src/google_places/flutter_google_places.dart';
import 'package:glapp_manager/src/google_places/src/flutter_google_places.dart';

class RegisterUserPage extends StatelessWidget {
  ErrorModel errorModel;
  ProfessionalModel professionalModel;
  CountryFieldModel countryFieldModel;
  AuthProvider authProvider = AuthProvider();
  UserActionsProvider userActionsProvider = UserActionsProvider();

  final List<String> _colombianCities = ['Cali', 'Bogotá', 'Medellín'];
  String _userCity;
  String _placeId;

  final FocusNode _rutFocus = FocusNode();
  final FocusNode _nameFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _repeatPasswordFocus = FocusNode();
  final FocusNode _addressFocus = FocusNode();
  final FocusNode _countryFocus1 = FocusNode();
  final FocusNode _countryFocus2 = FocusNode();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _lastNameController = TextEditingController();
  final TextEditingController _rutController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _repeatPasswordController =
      TextEditingController();
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _addressController = TextEditingController();
  final TextEditingController _countryCutsomFieldController1 =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    errorModel = Provider.of<ErrorModel>(context);
    professionalModel = Provider.of<ProfessionalModel>(context);
    countryFieldModel = Provider.of<CountryFieldModel>(context);
    _nameFocus.addListener(_nameFocusChange);
    _addressFocus.addListener(_addressFocusChange);
    _lastNameFocus.addListener(_lastNameFocusChange);
    _rutFocus.addListener(_rutFocusChange);
    _emailFocus.addListener(_emailFocusChange);
    _passwordFocus.addListener(_passwordFocusChange);
    _repeatPasswordFocus.addListener(_repeatPasswordFocusChange);
    _phoneFocus.addListener(_phoneNumberFocusChange);
    getContextProvider(context);
    getPasswordController(_passwordController);
    changeStatusDark();
    return Scaffold(
      backgroundColor: Colors.white,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              IconButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () => Navigator.of(context).pop(),
                icon: Icon(Icons.arrow_back_ios, color: Color(0xFF515151)),
              ),
              Center(
                child: Image(
                  width: 60,
                  image: AssetImage('assets/images/logo-3.png'),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(
                    children: <Widget>[
                      SizedBox(height: 30),
                      Container(
                        width: double.infinity,
                        margin: EdgeInsets.only(left: 20),
                        child: Text(
                          'CREA TU CUENTA DE PROFESIONAL',
                          style: TextStyle(
                            fontSize: _screenSize.height * 0.025,
                            color: Colors.black,
                            fontFamily: 'BebasNeue',
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      _buildNameInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildLastNameInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildRutInput(context, _screenSize),
                      SizedBox(height: 50),
                      _buildEmailInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildPasswordInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildConfirmPasswordInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildPhoneInput(context, _screenSize),
                      SizedBox(height: 15),
                      _buildAddressInput(context, _screenSize),
                      SizedBox(height: 15),
                      CountryFields(
                          _countryFocus1,
                          _countryFocus2,
                          _countryCutsomFieldController1),
                      SizedBox(height: 30),
                      _buildTermsCheckbox(),
                      _buildPrivacyCheckbox(),
                    ],
                  ),
                ),
              ),
              ButtonGray(
                margin: 60,
                text: 'REGISTRARSE',
                action: () => _signup(context),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNameInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        focusNode: _nameFocus,
        controller: _nameController,
        onChanged: nameValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _nameFocus, _lastNameFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.name,
          labelText: 'Nombre(s)',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildLastNameInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _lastNameController,
        focusNode: _lastNameFocus,
        onChanged: lastNameValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _lastNameFocus, _rutFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.lastName,
          labelText: 'Apellido(s)',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildRutInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _rutController,
        focusNode: _rutFocus,
        onChanged: rutValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _rutFocus, _emailFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.rut,
          labelText: 'RUT',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildEmailInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _emailController,
        focusNode: _emailFocus,
        onChanged: emailValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _emailFocus, _passwordFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.email,
          labelText: 'Mail',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.emailAddress,
      ),
    );
  }

  Widget _buildPasswordInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _passwordController,
        obscureText: true,
        focusNode: _passwordFocus,
        onChanged: passwordValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) =>
            onFocusChange(context, _passwordFocus, _repeatPasswordFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.password,
          labelText: 'Contraseña',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.visiblePassword,
      ),
    );
  }

  Widget _buildConfirmPasswordInput(BuildContext context, Size screen) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        controller: _repeatPasswordController,
        obscureText: true,
        focusNode: _repeatPasswordFocus,
        onChanged: repeatPasswordValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) =>
            onFocusChange(context, _repeatPasswordFocus, _phoneFocus),
        decoration: InputDecoration(
          isDense: true,
          errorText: errorModel.repeatPassword,
          labelText: 'Repite contraseña',
          labelStyle:
              TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
        keyboardType: TextInputType.visiblePassword,
      ),
    );
  }

  Widget _buildPhoneInput(BuildContext context, Size screen) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Teléfono',
            style: TextStyle(color: Colors.grey),
          ),
          SizedBox(
            height: screen.height * 0.01,
          ),
          PhoneSelector(
              initialValue: professionalModel.prefix,
              changeDropButton: (value) => professionalModel.prefix = value,
              onSubmittedTextField: (t) => onFocusChange(context, _phoneFocus, _addressFocus),
              changeTextField: phoneNumberValidator,
              errorText: errorModel.phoneNumber,
              keyboardType: TextInputType.number,
              labelText: 'Teléfono',
              focusNode: _phoneFocus,
              controller: _phoneNumberController),
        ],
      ),
    );
  }

  Widget _buildAddressInput(BuildContext context, Size screen) {
    return GestureDetector(
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: 20),
        child: TextField(
          enabled: false,
          controller: _addressController,
          focusNode: _addressFocus,
          onChanged: addressValidator,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            isDense: true,
            errorText: errorModel.address,
            labelText: 'Dirección',
            labelStyle:
                TextStyle(color: Colors.grey, fontSize: screen.height * 0.020),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                width: 1,
                color: Color(0xFF8492A6).withOpacity(0.5),
              ),
            ),
          ),
          style: TextStyle(color: Colors.black, fontSize: screen.height * 0.020),
          keyboardType: TextInputType.text,
        ),
      ),

      onTap: () async {
        String _countryPrefix = '';
        _countryPrefix = getCountryPrefix(professionalModel.prefix);
        Prediction prediction = await PlacesAutocomplete.show(
          context: context,
          apiKey: 'AIzaSyBB4T59gcXepqvOWCO34dKOnbntyATO3r4',
          language: "es",
          mode: Mode.overlay,
          components: [
            new Component(Component.country, '$_countryPrefix')
          ]);

        if (prediction != null) {
          _placeId = prediction.placeId;
          _addressController.text = prediction.description.toString();
          _cityFinder(prediction.description.toString());
          _getLocalities();
          _customField(_countryPrefix);
        } else {
          _addressController.text = '';
        }
      },
    );
  }

  void _customField(String countryPrefix) {
    if (countryPrefix == 'ar') {
      countryFieldModel.field = 'Provincia';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    } else if (countryPrefix == 'cl') {
      countryFieldModel.field = 'Region';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    } else if (countryPrefix == 'co') {
      _customColombianCities();
    }
  }

  void _customColombianCities() {
    if (this._userCity == 'Bogotá') {
      countryFieldModel.field = 'Localidad';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    } else if (this._userCity == 'Cali') {
      countryFieldModel.field = 'Comuna';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    } else if (this._userCity == 'Medellín') {
      countryFieldModel.field = 'Comuna';
      countryFieldModel.visible = true;
      countryFieldModel.visible2 = true;
    }
  }

  void _getLocalities() async {
    String t = professionalModel.prefix;

    final resp = await userActionsProvider.getLocalities(t.substring((t.length - t.length) + 1, t.length));
    if(resp.body['data'] != null) {
      final res = resp.body['data'];
      countryFieldModel.localities = res[0]['comunas'];
      countryFieldModel.listValue = countryFieldModel.localities[0]['nombre'];
    } else {
      countryFieldModel.visible = false;
      countryFieldModel.visible2 = false;
    }
  }

  void _cityFinder(String _enteredAddress) {
    List<String> _splitAddress = _enteredAddress.split(',');
    List<String> _newSplitAddress = List<String>();
    _splitAddress.forEach((item) {
      _newSplitAddress.add(item.trim());
    });
    _colombianCities.forEach((city) {
      if (_newSplitAddress.contains(city)) this._userCity = city;
    });
  }

  Widget _buildTermsCheckbox() {
    return CheckboxListTile(
      onChanged: (value) => print(value),
      dense: true,
      value: true,
      controlAffinity: ListTileControlAffinity.leading,
      title: Text(
        'Acepto términos y condiciones',
        style: TextStyle(
          decoration: TextDecoration.underline,
          color: Color(0xFF47525E),
          fontSize: 12,
          fontWeight: FontWeight.w400,
        ),
      ),
      checkColor: Colors.white,
      activeColor: Color(0xFF8290A5),
    );
  }

  Widget _buildPrivacyCheckbox() {
    return CheckboxListTile(
      onChanged: (value) => print(value),
      dense: true,
      value: true,
      controlAffinity: ListTileControlAffinity.leading,
      title: Text(
        'He leído y acepto la política de privacidad',
        style: TextStyle(
          decoration: TextDecoration.underline,
          color: Color(0xFF47525E),
          fontSize: 12,
          fontWeight: FontWeight.w400,
        ),
      ),
      checkColor: Colors.white,
      activeColor: Color(0xFF8290A5),
    );
  }

  void _nameFocusChange() {
    if (_nameController.text.trim() == '' && !_nameFocus.hasFocus) {
      errorModel.name = 'El nombre es requerido';
    }
  }

  void _lastNameFocusChange() {
    if (_lastNameController.text.trim() == '' && !_lastNameFocus.hasFocus) {
      errorModel.lastName = 'El apellido es requerido';
    }
  }

  void _rutFocusChange() {
    if (_rutController.text.trim() == '' && !_rutFocus.hasFocus) {
      errorModel.rut = 'El RUT es requerido';
    }
  }

  void _emailFocusChange() {
    if (_emailController.text.trim() == '' && !_emailFocus.hasFocus) {
      errorModel.email = 'El correo es requerido';
    }
  }

  void _passwordFocusChange() {
    if (_passwordController.text.trim() == '' && !_passwordFocus.hasFocus) {
      errorModel.password = 'La contraseña es requerida';
    }
  }

  void _repeatPasswordFocusChange() {
    if (_repeatPasswordController.text.trim() == '' &&
        !_repeatPasswordFocus.hasFocus) {
      errorModel.repeatPassword = 'La contraseña es requerida';
    }
  }

  void _phoneNumberFocusChange() {
    if (_phoneNumberController.text.trim() == '' && !_phoneFocus.hasFocus) {
      errorModel.phoneNumber = 'El número de teléfono es requerido';
    }
  }

  void _addressFocusChange() {
    if (_addressController.text.trim() == '' && !_addressFocus.hasFocus)
      errorModel.address = 'La dirección es requerida';
  }

  void _signup(BuildContext context) async {
    if(_allIsFilled()) {
      final res = await authProvider.signupFreelance(_buildMap());
      if (res.success)
        Navigator.pushNamed(context, 'register-success');
      else {
        Navigator.pop(context);
        showAlert(context, 'Error', res.body['message']);
      } 
    } else showAlert(context, 'Error', 'Hay campos incorrectos o vacios');
  }

  Map<String, String> _buildMap() {
    return {
      'nombre': this._nameController.text,
      'apellidos': this._lastNameController.text,
      'rut': this._rutController.text,
      'email': _emailController.text,
      'ciudad': getCity(getCountryPrefix(professionalModel.prefix),
          this._userCity, _addressController.text),
      'localidad': countryFieldModel.listValue,
      'detalle_direccion': _countryCutsomFieldController1.text,
      'direccion': _addressController.text,
      'password': _passwordController.text,
      'country': getCountry(getCountryPrefix(professionalModel.prefix)),
      'telefono': _phoneNumberController.text,
      'place_id' : this._placeId,
    };
  }

  bool _allIsFilled() {
    return (_nameController.text.trim() != '' && 
     _lastNameController.text.trim() != '' && 
     _rutController.text.trim() != '' && 
     _emailController.text.trim() != '' && 
     _countryCutsomFieldController1.text.trim() != '' &&
     _addressController.text.trim() != '' && 
     _passwordController.text.trim() != '' &&
     _repeatPasswordController.text.trim() != '' &&
     _phoneNumberController.text.trim() != '' &&
     EmailValidator.validate(_emailController.text.trim()) && 
     _passwordController.text.trim().length > 5 && 
     _passwordController.text.trim() == _repeatPasswordController.text.trim()) ? 
     true : false;
  }
}