import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:glapp_manager/src/models/locality_model.dart';
import 'package:glapp_manager/src/widgets/multi_select_dialog.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/providers/account_settings_provider.dart';
import 'package:glapp_manager/src/providers/user_actions_provider.dart';
import 'package:glapp_manager/src/models/filters_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/models/professional_model.dart';
import 'package:glapp_manager/src/models/add_services_model.dart';
import 'package:glapp_manager/src/models/schedule_model.dart';
import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/models/data_model.dart';

class AccountSettingsPage extends StatefulWidget {
  @override
  _AccountSettingsPageState createState() => _AccountSettingsPageState();
}

class _AccountSettingsPageState extends State<AccountSettingsPage> with SingleTickerProviderStateMixin {
  int index = 0;
  Size screenSize;
  TimeOfDay mondayEnd;
  TimeOfDay fridayEnd;
  TimeOfDay sundayEnd;
  TimeOfDay generalEnd;
  TimeOfDay tuesdayEnd;
  TimeOfDay thursdayEnd;
  TimeOfDay saturdayEnd;
  TimeOfDay mondayStart;
  TimeOfDay fridayStart;
  TimeOfDay sundayStart;
  TimeOfDay generalStart;
  TimeOfDay tuesdayStart;
  TimeOfDay wednesdayEnd;
  TimeOfDay thursdayStart;
  TimeOfDay saturdayStart;
  TimeOfDay wednesdayStart;
  FiltersModel filtersModel;
  List<FocusNode> _focusNodes = [];
  ProfessionalModel professionalModel;
  AddServicesModel addServicesModel;
  final FocusNode _serviceFocus = FocusNode();
  List<TextEditingController> _controllers = [];
  final TextEditingController _serviceController = TextEditingController();
  AccountSettingsProvider accountSettingsProvider = new AccountSettingsProvider();
  UserActionsProvider userActionsProvider = new UserActionsProvider();
  final storage = new FlutterSecureStorage();
  List<LocalityModel> localityModel = new List();
  List<dynamic> places = new List();
  int cityValue = 0;
  int townValue = 0;
  TabController _tabController;
  bool _isServiceSelected = false;
  bool _isInfoReady = false;
  bool _changeStep = false;
  List<bool> _isAttentionTimeFilled = [false, false, false];
  List<int> _toSelect = new List();
  List<int> _selectedTowns = new List();

  final controlller1 = MoneyMaskedTextController(thousandSeparator: ',', precision: 0, leftSymbol: '\$', decimalSeparator: '');
  final controlller2 = MoneyMaskedTextController(thousandSeparator: ',', precision: 0, leftSymbol: '\$', decimalSeparator: '');
  final controlller3 = MoneyMaskedTextController(thousandSeparator: ',', precision: 0, leftSymbol: '\$', decimalSeparator: '');
  final controlller4 = MoneyMaskedTextController(precision: 0, leftSymbol: '\%', decimalSeparator: '');
  final controlller5 = MoneyMaskedTextController(precision: 0, leftSymbol: '\%', decimalSeparator: '');
  final controlller6 = MoneyMaskedTextController(precision: 0, leftSymbol: '\%', decimalSeparator: '');
  final controlller7 = TextEditingController();
  final controlller8 = TextEditingController();
  final controlller9 = TextEditingController();

  final List<String> days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

  final Map<String, String> spanishToEnglishDays = {
    'lunes' : 'monday',
    'martes' : 'tuesday',
    'miercoles' : 'wednesday',
    'jueves' : 'thursday',
    'viernes' : 'friday',
    'sabado' : 'saturday',
    'domingo' : 'sunday'
  };

  final Map<String, String> englishToSpanishDays = {
    'monday' : 'Lunes',
    'tuesday' : 'Martes',
    'wednesday' : 'Miércoles',
    'thursday' : 'Jueves',
    'friday' : 'Viernes',
    'saturday' : 'Sábado',
    'sunday' : 'Domingo'
  };

  final Map<String, String> countryPrefix = {
    'Colombia' : '57',
    'Argentina' : '54',
    'Chile' : '56'
  };

  @override
  void initState() { 
    super.initState();
    _tabController = TabController(vsync: this, length: 3);
    if(!_tabController.indexIsChanging) _loadSchedules();
    _tabController.addListener(() { 
      if(_tabController.indexIsChanging) _callMethod();
    });
  }

  @override
  void dispose() { 
    _clearControllers();
    filtersModel.resetChecks();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    filtersModel = Provider.of<FiltersModel>(context);
    professionalModel = Provider.of<ProfessionalModel>(context);
    addServicesModel = Provider.of<AddServicesModel>(context);
    mondayEnd = professionalModel.schedule['monday']['end'];
    fridayEnd = professionalModel.schedule['friday']['end'];
    sundayEnd = professionalModel.schedule['sunday']['end'];
    generalEnd = professionalModel.schedule['general']['end'];
    tuesdayEnd = professionalModel.schedule['tuesday']['end'];
    thursdayEnd = professionalModel.schedule['thursday']['end'];
    saturdayEnd = professionalModel.schedule['saturday']['end'];
    mondayStart = professionalModel.schedule['monday']['start'];
    fridayStart = professionalModel.schedule['friday']['start'];
    sundayStart = professionalModel.schedule['sunday']['start'];
    generalStart = professionalModel.schedule['general']['start'];
    tuesdayStart = professionalModel.schedule['tuesday']['start'];
    wednesdayEnd = professionalModel.schedule['wednesday']['end'];
    thursdayStart = professionalModel.schedule['thursday']['start'];
    saturdayStart = professionalModel.schedule['saturday']['start'];
    wednesdayStart = professionalModel.schedule['wednesday']['start'];
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFA3DCBE),
        title: Text(
          'Ajustes de mi cuenta',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: DefaultTabController(
          length: 3,
          child: Column(
            children: <Widget>[
              TabBar(
                controller: _tabController,
                tabs: <Widget>[
                  _isInfoReady ? Tab(text: 'Horarios') : Center(child: Text('')),
                  _isInfoReady ? Tab(child: Column(children: <Widget>[SizedBox(height: 10), Text('Lugar'), Text('de atención')],)) : Center(child: Text('Cargando')),
                  _isInfoReady ? Tab(child: Column(children: <Widget>[SizedBox(height: 10), Text('Servicios'), Text('y tarifas')],)) : Center(child: Text('')),
                ],
                labelColor: Color(0xFF343F4B),
                indicatorColor: Color(0xFFDD84A2),
                labelStyle: TextStyle(
                  fontSize: 12,
                  fontWeight: FontWeight.w700,
                ),
              ),
              Expanded(
                child: Container(
                  color: Colors.white,
                  child: TabBarView(
                    physics: NeverScrollableScrollPhysics(),
                    controller: _tabController,
                    children: [
                      _buildScheduleSettings(context),
                      _buildPlacesSettings(),
                      _buildServicesSettings(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildScheduleSettings(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Color(0xFFDADCE0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 2),
                    Container(
                      color: Colors.white,
                      width: double.infinity,
                      padding: EdgeInsets.all(15),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: screenSize.width * 0.1,
                            ),
                            child: SwitchListTile(
                              title: Text(
                                'Activar mi disponibilidad',
                                style: TextStyle(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              activeColor: Color(0xFF47525E),
                              value: professionalModel.availability,
                              onChanged: (bool value) =>
                                  professionalModel.availability = value,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                'Horario general',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xFF343F4B),
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                              _buildTimeDropdown(
                                  context: context,
                                  startTime: professionalModel
                                      .schedule['general']['start'],
                                  action: () => showTimePicker(
                                          context: context,
                                          initialTime: generalStart)
                                      .then((value) => this._updateSchedule(
                                          start: value, end: generalEnd))),
                              _buildTimeDropdown(
                                  helper: 'Hasta',
                                  context: context,
                                  startTime: professionalModel
                                      .schedule['general']['end'],
                                  action: () => showTimePicker(
                                          context: context,
                                          initialTime: generalEnd)
                                      .then((value) => this._updateSchedule(
                                          start: generalStart, end: value))),
                            ],
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 20),
                    _buildDaySchedule(
                      day: 'Lunes',
                      context: context,
                      toStartTime: mondayEnd,
                      fromStartTime: mondayStart,
                      fromAction: () => showTimePicker(
                              context: context, initialTime: mondayStart)
                          .then((value) => this._updateSchedule(
                              day: 'monday', start: value, end: mondayEnd)),
                      toAction: () => showTimePicker(
                              context: context, initialTime: mondayEnd)
                          .then((value) => this._updateSchedule(
                              day: 'monday', start: mondayStart, end: value)),
                    ),
                    SizedBox(height: 10),
                    _buildDaySchedule(
                      day: 'Martes',
                      context: context,
                      toStartTime: tuesdayEnd,
                      fromStartTime: tuesdayStart,
                      fromAction: () => showTimePicker(
                              context: context, initialTime: tuesdayStart)
                          .then((value) => this._updateSchedule(
                              day: 'tuesday', start: value, end: tuesdayEnd)),
                      toAction: () => showTimePicker(
                              context: context, initialTime: tuesdayEnd)
                          .then((value) => this._updateSchedule(
                              day: 'tuesday', start: tuesdayStart, end: value)),
                    ),
                    SizedBox(height: 10),
                    _buildDaySchedule(
                      day: 'Miércoles',
                      context: context,
                      toStartTime: wednesdayEnd,
                      fromStartTime: wednesdayStart,
                      fromAction: () => showTimePicker(
                              context: context, initialTime: wednesdayStart)
                          .then((value) => this._updateSchedule(
                              day: 'wednesday',
                              start: value,
                              end: wednesdayEnd)),
                      toAction: () => showTimePicker(
                              context: context, initialTime: wednesdayEnd)
                          .then((value) => this._updateSchedule(
                              day: 'wednesday',
                              start: wednesdayStart,
                              end: value)),
                    ),
                    SizedBox(height: 10),
                    _buildDaySchedule(
                      day: 'Jueves',
                      context: context,
                      toStartTime: thursdayEnd,
                      fromStartTime: thursdayStart,
                      fromAction: () => showTimePicker(
                              context: context, initialTime: thursdayStart)
                          .then((value) => this._updateSchedule(
                              day: 'thursday', start: value, end: thursdayEnd)),
                      toAction: () => showTimePicker(
                              context: context, initialTime: thursdayEnd)
                          .then((value) => this._updateSchedule(
                              day: 'thursday',
                              start: thursdayStart,
                              end: value)),
                    ),
                    SizedBox(height: 10),
                    _buildDaySchedule(
                      day: 'Viernes',
                      context: context,
                      toStartTime: fridayEnd,
                      fromStartTime: fridayStart,
                      fromAction: () => showTimePicker(
                              context: context, initialTime: fridayStart)
                          .then((value) => this._updateSchedule(
                              day: 'friday', start: value, end: fridayEnd)),
                      toAction: () => showTimePicker(
                              context: context, initialTime: fridayEnd)
                          .then((value) => this._updateSchedule(
                              day: 'friday', start: fridayStart, end: value)),
                    ),
                    SizedBox(height: 10),
                    _buildDaySchedule(
                      day: 'Sábado',
                      context: context,
                      toStartTime: saturdayEnd,
                      fromStartTime: saturdayStart,
                      fromAction: () => showTimePicker(
                              context: context, initialTime: saturdayStart)
                          .then((value) => this._updateSchedule(
                              day: 'saturday', start: value, end: saturdayEnd)),
                      toAction: () => showTimePicker(
                              context: context, initialTime: saturdayEnd)
                          .then((value) => this._updateSchedule(
                              day: 'saturday',
                              start: saturdayStart,
                              end: value)),
                    ),
                    SizedBox(height: 10),
                    _buildDaySchedule(
                      day: 'Domingo',
                      context: context,
                      toStartTime: sundayEnd,
                      fromStartTime: sundayStart,
                      fromAction: () => showTimePicker(
                              context: context, initialTime: sundayStart)
                          .then((value) => this._updateSchedule(
                              day: 'sunday', start: value, end: sundayEnd)),
                      toAction: () => showTimePicker(
                              context: context, initialTime: sundayEnd)
                          .then((value) => this._updateSchedule(
                              day: 'sunday', start: sundayStart, end: value)),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: 20),
              color: Colors.white,
              child: ButtonGray(
                text: 'GUARDAR',
                action: () {
                  professionalModel.availability = true;
                  _updateSchedules(context);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTimeDropdown({
    Function action,
    BuildContext context,
    String helper = 'Desde',
    TimeOfDay startTime = const TimeOfDay(hour: 15, minute: 15),
  }) {
    return RawMaterialButton(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            helper,
            style: TextStyle(
              fontSize: 10,
              color: Color(0xFF8492A6),
            ),
          ),
          Container(
            padding: EdgeInsets.only(bottom: 5),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 1, color: Color(0xFF8592A6)),
              ),
            ),
            child: Row(
              children: <Widget>[
                Text(
                    '${startTime.hour.toString().padLeft(2, '0')}:${startTime.minute.toString().padLeft(2, '0')}'),
                Icon(Icons.arrow_drop_down),
              ],
            ),
          ),
        ],
      ),
      onPressed: action,
    );
  }

  Widget _buildDaySchedule({
    String day,
    Function toAction,
    Function fromAction,
    BuildContext context,
    TimeOfDay toStartTime = const TimeOfDay(hour: 15, minute: 15),
    TimeOfDay fromStartTime = const TimeOfDay(hour: 15, minute: 15),
  }) {
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      margin: EdgeInsets.symmetric(horizontal: screenSize.width * 0.07),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: screenSize.width * 0.2,
            child: Text(
              day,
              style: TextStyle(
                fontSize: 12,
                color: Color(0xFF343F4B),
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
          _buildTimeDropdown(
            context: context,
            action: fromAction,
            startTime: fromStartTime,
          ),
          _buildTimeDropdown(
            helper: 'Hasta',
            context: context,
            action: toAction,
            startTime: toStartTime,
          )
        ],
      ),
    );
  }

  void _updateSchedule({String day = '', TimeOfDay start, TimeOfDay end}) {
    if (day.length > 0)
      professionalModel.schedule = {
        ...professionalModel.schedule,
        day: {'start': start, 'end': end},
      };
    else
      professionalModel.schedule = {
        'general': {'start': start, 'end': end},
        'monday': {'start': start, 'end': end},
        'tuesday': {'start': start, 'end': end},
        'wednesday': {'start': start, 'end': end},
        'thursday': {'start': start, 'end': end},
        'friday': {'start': start, 'end': end},
        'saturday': {'start': start, 'end': end},
        'sunday': {'start': start, 'end': end}
      };
  }

  Widget _buildPlacesSettings() {
    return SafeArea(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(15),
            color: Color(0xFF99A1AB).withOpacity(0.37),
            child: Column(
              children: <Widget>[
                GenericSelect(
                  showHint: true,
                  labelText: 'Ciudad',
                  items: filtersModel.cities,
                  selected: professionalModel.city,
                  changeDropButton: (value) { 
                    professionalModel.city = value;
                    cityValue = value;
                    _showTowns();
                  },
                ),
                SizedBox(height: 10),
                ButtonGray(
                  text: 'Seleccionar localidades',
                  action: () => _showMultiSelect(this.context),
                ),
              ],
            ),
          ),
          Container(
            color: Colors.white,
            padding: EdgeInsets.only(top: 20),
            child: ButtonGray(
              text: 'GUARDAR',
              action: () => _uploadPlaceToAttention(context),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildServicesSettings() {
    return SafeArea(
      child: Stepper(
        currentStep: index,
        type: StepperType.horizontal,
        onStepCancel: () {
          if (index == 1) {
            _clearControllers();
            this.setState(() => this.index -= 1);
          } else if(index == 0) {
            _changeStep = false;
            _isAttentionTimeFilled.forEach((e) { e = false; });
          }
        },
        onStepContinue: () async {
          if (index == 0) {
            this.setState(() => this.index += 1);
          } else {
            if(index == 1) _changeStep = true;
            if(_checkAttentionTime()) { 
              final resp = await accountSettingsProvider.addServices(addServicesModel.toJson());
              if(resp.success) {
                final dataModel = Provider.of<DataModel>(context, listen: false);
                dataModel.index = 1;
                showAlert(context, 'Exito!', resp.message);
              } else 
                showAlert(context, 'Error', resp.message);
            } else 
              showAlert(context, 'Error', 'El tiempo de atención no debe estar vacio');
          }
        },
        onStepTapped: (value) => this.setState(() { 
          this.index = value; 
          if(value == 0) { 
            filtersModel.resetChecks();    
            _changeStep = false;
            _isAttentionTimeFilled.forEach((e) { e = false; });
          }
          if(value == 1) _changeStep = true;
        }),
        controlsBuilder: (BuildContext context,
            {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
          return Container(
            color: Colors.white,
            padding: EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: index == 0
                  ? MainAxisAlignment.center
                  : MainAxisAlignment.spaceBetween,
              children: <Widget>[
                if (index > 0)
                  Container(
                    width: screenSize.width * (index == 0 ? 0.8 : 0.4),
                    child: ButtonGray(
                      margin: 0,
                      cancel: true,
                      text: 'CANCELAR',
                      action: onStepCancel,
                    ),
                  ),
                Container(
                  width: screenSize.width * (index == 0 ? 0.8 : 0.4),
                  child: _isServiceSelected ? ButtonGray(
                    margin: 0,
                    action: onStepContinue,
                    text: index == 0 ? 'CONTINUAR' : 'GUARDAR',
                  ) : Container(),
                ),
              ],
            ),
          );
        },
        physics: BouncingScrollPhysics(),
        steps: [
          _stepOne(),
          _stepTwo(),
        ],
      ),
    );
  }

  Step _stepOne() {
    return Step(
      isActive: index == 0,
      title: Text('Paso 1'),
      content: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GenericSelect(
                  horizontalPadding: 0,
                  labelText: 'Servicio',
                  width: screenSize.width * 0.5,
                  hint: Text('Selecciona servicio'),
                  items: filtersModel.allServices,
                  selected: professionalModel.service,
                  showHint: _isServiceSelected,
                  changeDropButton: (value) {
                    addServicesModel.mainService = filtersModel.allServices[value];
                    professionalModel.service = value;
                    setState(() {
                      _isServiceSelected = true;
                    });
                  }
                ),
                Container(
                  width: double.infinity,
                  child: TextField(
                    focusNode: _serviceFocus,
                    controller: _serviceController,
                    textInputAction: TextInputAction.done,
                    decoration: InputDecoration(
                      isDense: true,
                      labelText: 'Ingresa el nombre del servicio',
                      labelStyle: TextStyle(
                        color: Colors.grey,
                        fontSize: screenSize.height * 0.02,
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                          width: 1,
                          color: Color(0xFF8492A6).withOpacity(0.5),
                        ),
                      ),
                    ),
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: screenSize.height * 0.02,
                    ),
                    keyboardType: TextInputType.text,
                    onChanged: (value) => addServicesModel.serviceName = value,
                  ),
                ),
                SizedBox(height: 20),
                Text(
                  'Ingresa para quien prestas el servicio',
                  style: TextStyle(
                    fontSize: 13,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 20),
                CheckboxListTile(
                  dense: true,
                  value: filtersModel.clients['men'],
                  controlAffinity: ListTileControlAffinity.leading,
                  onChanged: (value) {
                    _serviceClientSetter(value, 'H');
                      this.setState(() => filtersModel.clients['men'] = value);
                  },
                  title: Text(
                    'HOMBRE',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFF47525E),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  checkColor: Colors.white,
                  activeColor: Color(0xFF8290A5),
                ),
                CheckboxListTile(
                  dense: true,
                  value: filtersModel.clients['women'],
                  controlAffinity: ListTileControlAffinity.leading,
                  onChanged: (value) { 
                    _serviceClientSetter(value, 'M');
                    this.setState(() => filtersModel.clients['women'] = value);
                  },
                  title: Text(
                    'MUJER',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFF47525E),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  checkColor: Colors.white,
                  activeColor: Color(0xFF8290A5),
                ),
                CheckboxListTile(
                  dense: true,
                  value: filtersModel.clients['child'],
                  controlAffinity: ListTileControlAffinity.leading,
                  onChanged: (value) {
                    _serviceClientSetter(value, 'N');
                    this.setState(() => filtersModel.clients['child'] = value);
                  },
                  title: Text(
                    'NIÑO',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFF47525E),
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  checkColor: Colors.white,
                  activeColor: Color(0xFF8290A5),
                ),
                Container(
                  width: screenSize.width * (index == 0 ? 0.8 : 0.4),
                  child: ButtonGray(
                    margin: 0,
                    action: () {
                      final dataModel = Provider.of<DataModel>(this.context, listen: false);
                      dataModel.index = 1;
                      Navigator.pushNamedAndRemoveUntil(context, 'home', (route) => false);
                    },
                    text: 'VER SERVICIOS',
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _serviceClientSetter(bool value, String clientType) {
    (value) 
    ? addServicesModel.serviceClient = clientType 
    : addServicesModel.deleteServiceClient = clientType;
  }

  Step _stepTwo() {
    return Step(
      isActive: index == 1,
      title: Text('Paso 2'),
      content: SingleChildScrollView(
        child: Column(
          children: _buildBody(),
        ),
      ),
    );
  }

  List<Widget> _buildBody() {
    _focusNodes = [];
    List<Widget> _result = [];
    _result.add(Container(
      width: double.infinity,
      child: TextField(
        keyboardType: TextInputType.number,
        enabled: false,
        focusNode: _serviceFocus,
        controller: _serviceController,
        textInputAction: TextInputAction.done,
        decoration: InputDecoration(
          isDense: true,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(
          color: Colors.black,
          fontSize: screenSize.height * 0.02,
        ),
      ),
    ));
    var aux = 0;
    var loops = 0;
    var notFill = 0;
    int addIndex;
    _controllers.addAll([
      controlller1,
      controlller2,
      controlller3,
      controlller4,
      controlller5,
      controlller6,
      controlller7,
      controlller8,
      controlller9,
    ]);
    filtersModel.clients.forEach((key, value) {
      _focusNodes.addAll([FocusNode(), FocusNode(), FocusNode()]);
      if (value) {
        String _clientType = _getClient(key).substring(0,1);
        _result.add(Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: screenSize.width * 0.4,
                ),
                Container(
                  width: screenSize.width * 0.4,
                  child: Text(
                    _getClient(key),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xFF47525E),
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                )
              ],
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: screenSize.width * 0.45,
                  child: Text(
                    'Precio',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                      color: Color(0xFF47525E),
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                Container(
                  height: 25,
                  width: screenSize.width * 0.4,
                  child: TextField(
                    // focusNode: _focusNodes[aux * 3],
                    controller: _controllers[aux],
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      isDense: true,
                      border:
                          OutlineInputBorder(borderSide: BorderSide(width: 1)),
                    ),
                    // textInputAction: TextInputAction.continueAction,
                    style: TextStyle(color: Colors.black, fontSize: 14),
                    onChanged: (value) {
                      addServicesModel.serviceDetailPrice = new ServiceDetail(
                        client: _clientType,
                        price: int.parse(value.substring(1).replaceAll(',', '')),
                      );
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: screenSize.width * 0.45,
                  child: Text(
                    'Si deseas aplicar descuento',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                      color: Color(0xFF47525E),
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                Container(
                  height: 25,
                  width: screenSize.width * 0.4,
                  child: TextField(
                    // focusNode: _focusNodes[(aux * 3) + 1],
                    controller: _controllers[aux + 3],
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      counterText: '',
                      isDense: true,
                      border:
                          OutlineInputBorder(borderSide: BorderSide(width: 1)),
                    ),
                    // textInputAction: TextInputAction.continueAction,
                    maxLength: 4,
                    style: TextStyle(color: Colors.black, fontSize: 14),
                    onChanged: (value) {
                      _clientType == 'H' ? addIndex = 0 : _clientType == 'M' ? addIndex = 1 : addIndex = 2;
                      int discount = int.parse(value.substring(1));
                      if(discount <= 100) {
                        addServicesModel.serviceDetailDiscount = new ServiceDetail(
                          client: _clientType,
                          discount: discount,
                        );
                      } else {
                        showAlert(context, 'Error', 'El porcentaje no debe ser mayor a 100');
                        _controllers[aux + addIndex + 3].text = '0';
                      }
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: screenSize.width * 0.45,
                  child: Text(
                    'Tiempo para prestar el servicio',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 15,
                      color: Color(0xFF47525E),
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                Container(
                  height: 25,
                  width: screenSize.width * 0.4,
                  child: TextField(
                    // focusNode: _focusNodes[(aux * 3) + 2],
                    controller: _controllers[aux + 6],
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      isDense: true,
                      border:
                          OutlineInputBorder(borderSide: BorderSide(width: 1)),
                    ),
                    textInputAction: TextInputAction.done,
                    style: TextStyle(color: Colors.black, fontSize: 14),
                    onChanged: (value) {
                      bool flag;
                      _clientType == 'H' ? addIndex = 0 : _clientType == 'M' ? addIndex = 1 : addIndex = 2;
                      value.length > 0 ? flag = true : flag = false;
                      _isAttentionTimeFilled[aux + addIndex] = flag;
                      addServicesModel.serviceDetailTime = new ServiceDetail(
                        client: _clientType,
                        time: int.parse(value),
                      );
                    },
                  ),
                ),
              ],
            ),
          ],
        ));
        aux += 1;
        loops += 1;
        notFill += 1;
      } else if (!value && _changeStep){
        _isAttentionTimeFilled[notFill++] = true; 
      }
    });
    aux -= loops;
    _result.add(SizedBox(height: 20));
    return _result;
  }

  String _getClient(String client) {
    switch (client) {
      case 'men':
        return 'HOMBRE';
      case 'women':
        return 'MUJER';
      case 'child':
        return 'NIÑO';
      default:
        return client;
    }
  }

  void _updateSchedules(BuildContext context) async {
    final resp = await accountSettingsProvider.updateSchedules(_buildSchedulesMap());

    if(resp.success) {
      Navigator.pop(context);
      showAlert(context, 'Exito', resp.message);
    }else {
      Navigator.pop(context);
      showAlert(context, 'Error', resp.message);
    }
  }

  void _loadSchedules() async {
    setState(() {
      _isInfoReady = false;  
    });

    final resp = await accountSettingsProvider.getSchedules();
    if(!resp.success) {
      Navigator.pop(context);
      showAlert(context, 'Error', resp.message);
    } else 
      _showSchedules(resp);
  }

  void _showSchedules(ResponseModel responseModel) {

    final scheduleModel = ScheduleModel.fromJsonMap(responseModel.body);
    
    setState(() {
      professionalModel.availability = scheduleModel.availabilty;

      for(var dayInfo in scheduleModel.schedules) {
        professionalModel.schedule[spanishToEnglishDays[dayInfo['dia']]]['start'] = TimeOfDay(hour: int.parse(dayInfo['hora_inicio'].split(':')[0]), minute: int.parse(dayInfo['hora_inicio'].split(':')[1]));
        professionalModel.schedule[spanishToEnglishDays[dayInfo['dia']]]['end'] = TimeOfDay(hour: int.parse(dayInfo['hora_fin'].split(':')[0]), minute: int.parse(dayInfo['hora_fin'].split(':')[1]));
      }

      setState(() { _isInfoReady = true; });
    });
  }

  Map<String, dynamic> _buildSchedulesMap() {
    List<dynamic> schedules = new List();

    for(var i in days) 
      schedules.add(
        {
          'dia' : englishToSpanishDays[i],
          'hora_inicio' : professionalModel.schedule[i]['start'].toString(),
          'hora_fin' : professionalModel.schedule[i]['end'].toString()
        }
      );

    return {
      'disponible' : professionalModel.availability,
      'horarios' : schedules
    };
  }

  void _uploadPlaceToAttention(BuildContext context) async {
    if(_selectedTowns.length > 0) {
      final resp = await accountSettingsProvider.updatePlace(_buildPlacesMap());

      if(resp.success) {
        Navigator.pop(context);
        showAlert(context, 'Exito', resp.message);
      } else {
        Navigator.pop(context);
        showAlert(context, 'Error', resp.message);
      }
    } else 
      showAlert(context, 'Error', 'No hay localidades seleccionadas');
  }

  void _loadPlaceToAttention() async {
    var country = json.decode(await storage.read(key: 'user'))['country'];
    
    final aux = countryPrefix[country];
    if(aux != null) country = aux;

    final resp = await accountSettingsProvider.getPlacesToAttention();
    final resp2 = await userActionsProvider.getLocalities(country);

    if(!resp.success && !resp2.success) {
      Navigator.pop(context);
      showAlert(context, 'Error', resp.message);
    } else if(resp2.body['data'].length == 0 && resp.body['data'].length == 0) {
      Navigator.pop(context);
      showAlert(context, 'Error', 'No hay información disponible');
    } else 
      _showInitialPlaces(resp, resp2);

    setState(() { _isInfoReady = true; });
  }

  void _showInitialPlaces(ResponseModel resp, ResponseModel resp2) {
    Map <String, String> avoidReps = new Map(); 
    places = resp.body['lugar_atencion'];
    int cityIndex = 0;
    int townIndex = 0;
    Map<dynamic, String> citiesMap = new Map();
    Map<dynamic, String> townsMap = new Map();

    resp2.body['data'].forEach((item) {
      localityModel.add(LocalityModel.fromJsonMap(item));
    });

    final towns = localityModel[0].comuna;

    avoidReps.clear();
    for(cityIndex = 0; cityIndex < places.length; cityIndex++) {
      if(avoidReps.length == 0 || avoidReps[places[cityIndex]['ciudad']] == null) {
        citiesMap.addAll({index : places[cityIndex]['ciudad']});
        avoidReps.addAll({places[cityIndex]['ciudad'] : places[cityIndex]['ciudad']});
      } 
    }

    for(; cityIndex < localityModel.length + places.length; cityIndex++) {
      if(avoidReps.length == 0 || avoidReps[localityModel[cityIndex - places.length].name] == null) {
        citiesMap.addAll({index : localityModel[cityIndex - places.length].name});
        avoidReps.addAll({localityModel[cityIndex - places.length].name : localityModel[cityIndex - places.length].name});
      }
    }
  
    avoidReps.clear();
    for(townIndex = 0; townIndex < places.length; townIndex++) {
      if(avoidReps.length == 0 || avoidReps[places[townIndex]['localidad']] == null) {
        townsMap.addAll({townIndex : places[townIndex]['localidad']});
        avoidReps.addAll({places[townIndex]['localidad'] : places[townIndex]['localidad']});
        _toSelect.add(townIndex);
      }
    }
    
    for(; townIndex < towns.length + places.length; townIndex++) {
      if(avoidReps.length == 0 || avoidReps[towns[townIndex - places.length]['nombre']] == null) {
        townsMap.addAll({townIndex : towns[townIndex - places.length]['nombre']});
        avoidReps.addAll({towns[townIndex - places.length]['nombre'] : towns[townIndex - places.length]['nombre']});
      }
    }

    filtersModel.cities.clear();
    filtersModel.cities.addAll(citiesMap);
    filtersModel.towns.clear();
    filtersModel.towns.addAll(townsMap);
  }

  void _showTowns() {
    int i = 0;
    Map<dynamic, String> comunasMap = new Map();
    Map<String, String> avoidReps = new Map(); 
    final selectedCityComunas = localityModel[cityValue].comuna;

    avoidReps.clear();
    _toSelect.clear();
    for(i = 0; i < places.length; i++) {
      if(avoidReps.length == 0 || avoidReps[places[i]['localidad']] == null) {
        comunasMap.addAll({i : places[i]['localidad']});
        avoidReps.addAll({places[i]['localidad'] : places[i]['localidad']});
        _toSelect.add(i);
      }
    }

    for(; i < selectedCityComunas.length + places.length; i++) { 
      if(avoidReps.length == 0 || avoidReps[localityModel[i - places.length].name] == null) {
        comunasMap.addAll({i : selectedCityComunas[i - places.length]['nombre']});
        avoidReps.addAll({selectedCityComunas[i - places.length]['nombre'] : selectedCityComunas[i - places.length]['nombre']});
      }
    }

    filtersModel.towns.clear();
    filtersModel.towns.addAll(comunasMap);
  }

  Future<void> _showMultiSelect(BuildContext context) async {
    List <MultiSelectDialogItem<int>> multiItem = List();
    int aux = filtersModel.towns.length;

    for(int v = 0; v < aux; v++){
      if(filtersModel.towns[v] != null)
        multiItem.add(MultiSelectDialogItem(v, filtersModel.towns[v]));
      else aux++;
    }

    final selectedValues = await showDialog<Set<int>>(
      context: context,
      builder: (BuildContext context) {
        return MultiSelectDialog(
          items: multiItem,
          initialSelectedValues: _toSelect.toSet(),
        );
      },
    );

    if(selectedValues != null) {
      _selectedTowns.clear();
      _selectedTowns = selectedValues.toList();
    }
  }

  Map<String, dynamic> _buildPlacesMap() {
    List<dynamic> attentionPlaces = new List();

    _selectedTowns.forEach((e) {
      attentionPlaces.add({
        'ciudad' : filtersModel.cities[professionalModel.city],
        'localidad' : filtersModel.towns[e],
        'barrio' : ''
      });
    });
    
    return <String, dynamic> {
      'lugar_atencion' : attentionPlaces
    };
  }

  void _callMethod() {
    switch(_tabController.index) {
      case 0:
        _loadSchedules();
        break;
      case 1:
        _loadPlaceToAttention();
        break;
    }
  }

  void _clearControllers() {
    for(int i = 0; i < _controllers.length - 3; i++) _controllers[i].text = '0';
    for(int i = _controllers.length - 3; i < _controllers.length; i++) _controllers[i].text = '';
  }

  bool _checkAttentionTime() {
    var aux = true;
    _isAttentionTimeFilled.forEach((e) {
      aux &= e;
    });

    return aux;
  }
}