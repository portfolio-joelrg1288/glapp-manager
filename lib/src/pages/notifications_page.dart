import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/models/professional_model.dart';
import 'package:glapp_manager/src/providers/notifications_provider.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> with SingleTickerProviderStateMixin {
  Size screenSize;
  ProfessionalModel professionalModel;
  NotificationsProvider notificationsProvider = NotificationsProvider();
  TabController _tabController;
  bool _isNotificationsReady = false;
  bool _isNotificationAvailable = false;
  bool _allNotificationsLoaded = false;
  bool _readedNotificationsLoaded = false;
  bool _unreadNotificationsLoaded = false;
  List<Map<String, dynamic>> _allNotifications = new List();
  List<Map<String, dynamic>> _readedNotifications = new List();
  List<Map<String, dynamic>> _unreadNotifications = new List();

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 3); 
      if(!_tabController.indexIsChanging) _loadNotifications();
    _tabController.addListener(() { 
      if(_tabController.indexIsChanging) _loadNotifications();
    });
  }

  @override
  void dispose() { 
    _isNotificationAvailable = false;
    _resetFlagsAndList();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    professionalModel = Provider.of<ProfessionalModel>(context);
    return GestureDetector(
      onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
      child: DefaultTabController(
        length: 3,
        child: Column(
          children: <Widget>[
            TabBar(
              controller: _tabController,
              tabs: <Widget>[
                _isNotificationsReady ? Tab(text: 'Todas') : Tab(text: ''),
                _isNotificationsReady ? Tab(text: 'Leídas') : Tab(text: 'Cargando...'),
                _isNotificationsReady ? Tab(text: 'No leídas') : Tab(text: ''),
              ],
              labelColor: Color(0xFF343F4B),
              indicatorColor: Color(0xFFDD84A2),
              labelStyle: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w700,
              ),
            ),
            Expanded(
              child: Container(
                color: Color(0xFFF0F2F7),
                child: TabBarView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: _tabController,
                  children: [
                    _notifications('all', context),
                    _notifications('read', context),
                    _notifications('unread', context),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _notifications(String status, BuildContext c) {
    final notifications = professionalModel.notifications
        .where(
            (n) => (status == 'all' ? true : (n['read'] == (status == 'read'))))
        .toList();
    notifications.sort((a, b) => b['date'].compareTo(a['date']));
    return _isNotificationAvailable ? Container(
      height: double.infinity,
      child: SingleChildScrollView(
        physics: BouncingScrollPhysics(),
        child: Column(
          children: notifications.map((e) => _buildNotification(e, c)).toList(),
        ),
      ),
    ) : Container(
      height: double.infinity,
      child: Center(
        child: Text(
          'No hay informacion disponible',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: MediaQuery.of(c).size.width * 0.05,
          ),
        ),
      ),
    );
  }

  Widget _buildNotification(Map<String, dynamic> notification, BuildContext c) {
    return GestureDetector(
      onTap: () async {
        final dataModel = Provider.of<DataModel>(c, listen: false);
        dataModel.notification = notification;
        final resp = await notificationsProvider.updateNotificationState(dataModel.notification['id']);
        if(resp.success) {
          _resetFlagsAndList();
          Navigator.pushNamed(c, 'notification');
          _loadNotifications();
        } else  showAlert(c, 'Error', resp.message);
      },
      child: Container(
        color: Colors.white,
        width: double.infinity,
        margin: EdgeInsets.only(top: 8),
        height: screenSize.height * 0.148,
        padding: EdgeInsets.only(left: 10, top: 5),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: screenSize.width * 0.7,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    notification['title'],
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(height: 0),
                  Text(
                    notification['body'],
                    style: TextStyle(
                      fontSize: 11,
                      color: Color(0xFF343F4B),
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                  if (notification['type'] == 0)
                    Text(
                      'Lugar: ${notification['place']}',
                      style: TextStyle(
                        fontSize: 12,
                        color: Color(0xFF343F4B),
                        fontWeight: FontWeight.w300,
                      ),
                    ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    notification['date'],
                    style: TextStyle(
                      fontSize: 16,
                      color: Color(0xFF278B00),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(height: 10),
                  if (notification['read'])
                    Image(image: AssetImage('assets/icons/eye.png'), width: 20),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _loadNotifications() async {
    List<Map<String, dynamic>> aux;
    setState(() { 
      _isNotificationsReady = false; 
      _isNotificationAvailable = false;
    });

    final queryParameter = _tabController.index == 0 ? 'A' : _tabController.index == 1 ? 'L' : 'N';
    if(!_allNotificationsLoaded || !_readedNotificationsLoaded || !_unreadNotificationsLoaded) {
      final resp = await notificationsProvider.getNotifications(queryParameter);      

      if(resp.success) {
        aux = _createNotifications(resp.body['data']);
        queryParameter == 'A' ? _allNotificationsLoaded = true : queryParameter == 'L' ? _readedNotificationsLoaded = true : _unreadNotificationsLoaded = true;
        queryParameter == 'A' ? _allNotifications = aux : queryParameter == 'L' ? _readedNotifications = aux : _unreadNotifications = aux; 
        
        _showNotifications(aux);
      } else 
        showAlert(context, 'Error', resp.message);
    } else {
      queryParameter == 'A' ? aux = _allNotifications : queryParameter == 'L' ? aux = _readedNotifications : aux = _unreadNotifications; 
      _showNotifications(aux); 
    }
  }

  void _showNotifications(List<Map<String, dynamic>> res) async {
    if(res.length == 0) {
      setState(() {
        _isNotificationsReady = true;
        _isNotificationAvailable = false; 
      });
    } else {
      professionalModel.notifications = res;
      await Future.delayed(Duration(milliseconds: 500), () {
          _isNotificationsReady = true;
      });

      setState(() {
        _isNotificationAvailable = true;
      });
    }
  } 

  List<Map<String, dynamic>> _createNotifications(dynamic resp) {
    List<Map<String, dynamic>> notifications = new List();
    resp.forEach((item) {
      notifications.add(
        <String, dynamic> {
          'message' : item['mensaje'].length == 0 ? '' : item['mensaje'],
          'title' : item['titulo'].length == 0 ? '' : item['titulo'],
          'body' : item['cuerpo'].length == 0 ? '' : item['cuerpo'],
          'place' : item['lugar'].length == 0 ? '' : item['lugar'],
          'date' : item['fecha'].length == 0 ? '' : item['fecha'],
          'hour' : item['hora'].length == 0 ? '' : item['hora'],
          'read' : item['vista'],
          'id' : item['_id'],
          'origin' : item['origen'],
          'destiny' : item['destino'],
          'creationDate' : item['fechaCreacion'].length == 0 ? '' : item['fechaCreacion']
        }
      );
    });

    return notifications;
  }

  void _resetFlagsAndList() {
    _isNotificationsReady = false;
    _allNotificationsLoaded = false;
    _readedNotificationsLoaded = false;
    _unreadNotificationsLoaded = false;
    _allNotifications.clear();
    _readedNotifications.clear();
    _unreadNotifications.clear();
  }
}