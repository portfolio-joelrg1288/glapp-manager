import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:glapp_manager/src/models/data_model.dart';

class SplashScreenPage extends StatefulWidget {
  @override
  _SplashScreenPageState createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  Timer _timer;
  final storage = new FlutterSecureStorage();

  @override
  void initState() {
    super.initState();
    this.validateUser();
  }

  void validateUser() async {
    final user = await storage.read(key: 'user');
    if (user != null) {
      final dataModel = Provider.of<DataModel>(context, listen: false);
      dataModel.isProfessional = json.decode(user)['user_role'] != 'Local';
      _timer = Timer(
        Duration(seconds: 3),
        () => Navigator.popAndPushNamed(context, 'home'),
      );
    } else
      _timer = Timer(
        Duration(seconds: 3),
        () => Navigator.popAndPushNamed(context, 'login'),
      );
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/splash.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Container(
            child: Center(
                child: Image.asset(
              'assets/images/my-glapp.png',
              fit: BoxFit.cover,
              height: MediaQuery.of(context).size.height * 0.15,
            )),
          )
        ],
      ),
    );
  }
}
