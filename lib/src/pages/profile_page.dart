import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import 'package:glapp_manager/src/providers/user_actions_provider.dart';
import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/utils/validators.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/models/error_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/providers/auth_provider.dart';
import 'package:glapp_manager/src/models/professional_model.dart';
import 'package:glapp_manager/src/providers/images_provider.dart';

import 'package:http/http.dart' as http;
import 'dart:io';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  Size screenSize;
  ErrorModel errorModel;
  ProfessionalModel professionalModel;
  TextEditingController _rutController = TextEditingController();
  TextEditingController _nameController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  final FocusNode _rutFocus = FocusNode();
  final FocusNode _nameFocus = FocusNode();
  final FocusNode _phoneFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  AuthProvider authProvider = AuthProvider();
  final FocusNode _lastNameFocus = FocusNode();
  ImagesProvider imageProvider = ImagesProvider();
  UserActionsProvider userActionsProvider = UserActionsProvider();
  DataModel dataModel;
  bool _profileChoice = false;
  bool _carouselChoice = false;
  int quantityPhotosToPick = 0;

  @override
  void initState() {
    super.initState();
    _updateData();
  }

  @override
  Widget build(BuildContext context) {
    getContextProvider(context);
    screenSize = MediaQuery.of(context).size;
    errorModel = Provider.of<ErrorModel>(context);
    dataModel = Provider.of<DataModel>(context);
    professionalModel = Provider.of<ProfessionalModel>(context);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: <Widget>[Container()],
        backgroundColor: Color(0xFFA3DCBE),
        title: Text(
          'Mi perfil',
          style: TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      endDrawer: SideMenu(),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Stack(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: double.infinity,
              child: Column(
                children: <Widget>[
                  if (dataModel.isProfessional)
                    GestureDetector(
                      child: Container(
                        width: double.infinity,
                        height: screenSize.height * 0.22,
                        child: professionalModel.workPhotos.length == 0 &&
                                professionalModel.carouselPhotos.length == 0
                            ? Column(
                                children: <Widget>[
                                  SizedBox(height: 20),
                                  Icon(
                                    Icons.photo_camera,
                                    color: Color(0xFF47525E),
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    'Sube hasta 5 fotos de tu trabajo!',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ],
                              )
                            : _buildCarousel(),
                        color: Color(0xFFC0CCDA).withOpacity(0.39),
                      ),
                      onTap: () => _addWorkPhotos(context),
                    ),
                  if (!dataModel.isProfessional)
                    Container(
                      width: double.infinity,
                      height: screenSize.height * 0.22,
                      child: Column(
                        children: <Widget>[
                          ClipOval(
                            child: Image(
                              image: professionalModel.photo == ''
                                  ? AssetImage('assets/images/user.png')
                                  : NetworkImage(professionalModel.photo),
                              width: screenSize.height * 0.19,
                              height: screenSize.height * 0.19,
                            ),
                          ),
                          Text(
                            'Así te verán tus clientes',
                            style: TextStyle(fontSize: 10),
                          ),
                        ],
                      ),
                    ),
                  Expanded(
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              SizedBox(height: 35),
                              _buildNameInput(context),
                              SizedBox(height: 10),
                              _buildLastNameInput(context),
                              SizedBox(height: 10),
                              _buildRutInput(context),
                              SizedBox(height: 10),
                              _buildPhoneInput(context),
                              SizedBox(height: 10),
                              _buildEmailInput(context),
                              SizedBox(height: 30),
                            ],
                          ),
                          ButtonGray(
                            text: 'GUARDAR',
                            action: () => _update(context),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            if (dataModel.isProfessional)
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding:
                      EdgeInsets.only(top: screenSize.height * 0.16, right: 10),
                  child: Column(
                    children: <Widget>[
                      GestureDetector(
                        child: ClipOval(
                          child: (professionalModel.photo.length > 0 &&
                                  professionalModel.selectedPhoto == null)
                              ? Image(
                                  image: NetworkImage(professionalModel.photo),
                                  width: screenSize.width * 0.2,
                                  height: screenSize.width * 0.2,
                                )
                              : (professionalModel.selectedPhoto != null)
                                  ? Image.file(
                                      professionalModel.selectedPhoto,
                                      width: screenSize.width * 0.2,
                                      height: screenSize.width * 0.2,
                                    )
                                  : Image(
                                      image:
                                          AssetImage('assets/images/user.png'),
                                      width: screenSize.width * 0.2,
                                      height: screenSize.width * 0.2,
                                    ),
                        ),
                        onTap: () async {
                          dataModel.selectedPicture = null;
                          await imageProvider.showOptionsDialog(context, 1, 1, 1);
                          dataModel.selectedPicture != null ? await _confirmSelectedPhoto(context, 1) : Container();
                          if (dataModel.selectedPicture != null && _profileChoice) {
                            final http.StreamedResponse res = await imageProvider.uploadProfilePhoto(context);
                            if(res.statusCode == 200) {
                              professionalModel.selectedPhoto =
                                dataModel.selectedPicture;
                              showAlert(context, 'Exito', 'Exito al actualizar');
                            } else {
                              showAlert(context, 'Error', res.reasonPhrase);
                            }
                            _profileChoice = false;
                          }
                        }
                      ),
                      Text('Tu foto de perfil', style: TextStyle(fontSize: 9)),
                    ],
                  ),
                ),
              ),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigation(),
    );
  }

  Widget _buildCarousel() {
    return CarouselSlider(
      options: CarouselOptions(
        enableInfiniteScroll: false,
        height: screenSize.height * 0.2,
      ),
      items: _buildItems(),
    );
  }

  List<Widget> _buildItems() {
    List<Widget> result = [];
    final baseUrl = DotEnv().env['BASE_URL'];
    professionalModel.workPhotos
        .forEach((e) => result.add(Image(image: NetworkImage(baseUrl + e))));
    professionalModel.carouselPhotos
        .forEach((e) => result.add(Image.file(e)));
    return result;
  }

  void _addWorkPhotos(BuildContext context) async {
    final dataModel = Provider.of<DataModel>(context, listen: false);
    if ((professionalModel.workPhotos.length +
            professionalModel.carouselPhotos.length) <
        5) {
      quantityPhotosToPick = 5 - (professionalModel.workPhotos.length + professionalModel.carouselPhotos.length);
      await imageProvider.showOptionsDialog(context, 2, quantityPhotosToPick, 2);
      dataModel.selectedPictures != null ? await _confirmSelectedPhoto(context, 2) : Container();
      if (dataModel.selectedPictures != null && _carouselChoice) {
        professionalModel.carouselPhotos = dataModel.selectedPictures;
        professionalModel.carouselPhotos = professionalModel.carouselPhotos;
        _uploadCarousel();
        _carouselChoice = false;
      }
    } else 
      _confirmCarousel(context);
  }

  Widget _buildNameInput(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        focusNode: _nameFocus,
        controller: _nameController,
        onChanged: nameValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _nameFocus, _lastNameFocus),
        decoration: InputDecoration(
          isDense: true,
          labelText: 'Nombre(s)',
          labelStyle: TextStyle(color: Color(0xFF8492A6)),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(
          height: 0,
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildLastNameInput(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        focusNode: _lastNameFocus,
        controller: _lastNameController,
        onChanged: lastNameValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _lastNameFocus, _rutFocus),
        decoration: InputDecoration(
          isDense: true,
          labelText: 'Apellido(s)',
          labelStyle: TextStyle(color: Color(0xFF8492A6)),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(
          height: 0,
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildRutInput(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        focusNode: _rutFocus,
        controller: _rutController,
        onChanged: rutValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) => onFocusChange(context, _rutFocus, _phoneFocus),
        decoration: InputDecoration(
          isDense: true,
          labelText: 'RUT',
          labelStyle: TextStyle(color: Color(0xFF8492A6)),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(
          height: 0,
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
        keyboardType: TextInputType.text,
      ),
    );
  }

  Widget _buildPhoneInput(BuildContext context) {
    return Container(
      child: PhoneSelector(
        fontSize: 15,
        labelText: 'Teléfono',
        focusNode: _phoneFocus,
        onSubmittedTextField: (t) => 
          onFocusChange(context, _phoneFocus, _emailFocus),
        controller: _phoneController,
        textInputAction: TextInputAction.next, 
        keyboardType: TextInputType.number,
        errorText: errorModel.phoneNumber,
        changeTextField: phoneNumberValidator,
        initialValue: professionalModel.prefix,
        changeDropButton: (value) => professionalModel.prefix = value,
      ),
      margin: EdgeInsets.symmetric(horizontal: 20),
    );
  }

  Widget _buildEmailInput(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        enabled: false,
        focusNode: _emailFocus,
        controller: _emailController,
        onChanged: emailValidator,
        textInputAction: TextInputAction.next,
        decoration: InputDecoration(
          isDense: true,
          labelText: 'Mail',
          labelStyle: TextStyle(color: Color(0xFF8492A6)),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(
          height: 0,
          fontSize: 14,
          fontWeight: FontWeight.w400,
        ),
        keyboardType: TextInputType.emailAddress,
      ),
    );
  }

  void _update(BuildContext context) async {
    final res = await userActionsProvider.updateInfo(_buildBody());
    if (res.success) {
      _updateData();
      Navigator.pop(context);
      showAlert(context, 'Exito', 'Datos actualizados');
    } else {
      showAlert(context, 'Error', res.message);
    }
  }

  Map<String, dynamic> _buildBody() {
    return {
      "rut": _rutController.text,
      "nombre": _nameController.text,
      "telefono": _phoneController.text,
      "apellido": _lastNameController.text,
    };
  }

  void _updateData() async {
    final res = await userActionsProvider.getUserInfo();
    final String baseUrl = DotEnv().env['BASE_URL'];
    if (res.success) {
      professionalModel.reset();
      professionalModel.rut = res.body['rut'];
      professionalModel.name = res.body['nombre'];
      professionalModel.email = res.body['email'];
      professionalModel.phone = res.body['telefono'];
      professionalModel.lastName = res.body['apellido'];
      professionalModel.photo = baseUrl + res.body['profile_image'];
      res.body['images'].forEach((image) {
        professionalModel.workPhotos.add(image);
      });
      _showData();
    } else
      showAlert(context, 'Error', res.message);
  }

  void _showData() {
    setState(() {
      _nameController.text = professionalModel.name;
      _lastNameController.text = professionalModel.lastName;
      _rutController.text = professionalModel.rut;
      _phoneController.text = professionalModel.phone;
      _emailController.text = professionalModel.email;
    });
  }

  Future<void> _confirmSelectedPhoto(BuildContext context, int imageCalled) {
    Size _screenSize = MediaQuery.of(context).size;
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
      return AlertDialog(
        title: Text('Confirmación'),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            imageCalled == 1 ? Text('¿Confrimar la foto seleccionada?') : Text('¿Confirmar imagenes seleccionadas?'),
            imageCalled == 1 ? 
            Image.file(
              dataModel.selectedPicture,
              width: _screenSize.width * 0.35,
            ) : Container(),
          ]),
          actions: <Widget>[
          FlatButton(
            child: Text('Cancelar'),
            onPressed: () {
              imageCalled == 1 ? _profileChoice = false : _carouselChoice = false;
              Navigator.of(context).pop();
            },
          ),

          FlatButton(
            child: Text(imageCalled == 1 ? 'Confirmar' : 'Subir fotos'),
            onPressed: () {
              imageCalled == 1 ? _profileChoice = true : _carouselChoice = true;
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    });
  }

  void _confirmCarousel(BuildContext context) {
     showDialog(
      context: context,
      barrierDismissible: true,
      builder: (context) {
        return AlertDialog(
          title: Text('Límite de Fotos'),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('Límite de fotos alcanzado'),
                ]),
          actions: <Widget>[
            FlatButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

            FlatButton(
              child: Text('Volver a elegir'),
              onPressed: () {
                _resetCarousel();
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      });
  }

  void _resetCarousel() {
    setState(() {
      professionalModel.workPhotos.clear();
      professionalModel.carouselPhotos.clear();
      _buildItems();      
    });
  }

  Future<File> _getImageFromNetwork(String url) async {
     File file = await DefaultCacheManager().getSingleFile(url);
     return file;
  }

  Future<List<File>> _getFileList(String baseUrl) async {
    List<File> tmp = new List();

    for(int i = 0; i < professionalModel.workPhotos.length; i++)
      tmp.add(await _getImageFromNetwork(baseUrl + professionalModel.workPhotos[i]));

    professionalModel.carouselPhotos.forEach((e) { 
      tmp.add(e);
    });

    return tmp;
  }

  void _uploadCarousel() async {
    final resp = await imageProvider.uploadCarousel(await _getFileList(DotEnv().env['BASE_URL']));
    if(resp.statusCode == 200) showAlert(context, 'Exito', 'Exito al actualizar');
    else showAlert(context, 'Error', resp.reasonPhrase);
  }
}