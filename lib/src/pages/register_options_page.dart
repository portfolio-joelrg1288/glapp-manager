import 'package:flutter/material.dart';

import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/utils/reset_models.dart';

class RegisterOptionsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    getContextProvider(context);
    changeStatusDark();
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              IconButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: () => Navigator.of(context).pop(),
                icon: Icon(
                  Icons.arrow_back_ios,
                  color: Color(0xFF515151),
                ),
              ),
              SizedBox(height: 50),
              _buildOptionButton(
                text: 'Deseo registrar mi negocio (Salon, Barbería, SPA, etc.)',
                icon: 'business',
                context: context,
                action: () {
                  resetModels();
                  Navigator.of(context).pushNamed('register-business');
                }
              ),
              SizedBox(height: 50),
              _buildOptionButton(
                  text:
                      'Deseo registrarme como profesional independiente (Peluquero, barbero, manicurista, etc.)',
                  icon: 'user',
                  context: context,
                  action: () {
                    resetModels();
                    Navigator.of(context).pushNamed('register-user');
                  }
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildOptionButton(
      {Function action, String text, String icon, BuildContext context}) {
    return RawMaterialButton(
      onPressed: action,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/icons/$icon.png'),
              height: MediaQuery.of(context).size.height * 0.08,
            ),
            SizedBox(height: 15),
            Text(
              text,
              style: TextStyle(
                fontSize: 16,
                color: Color(0xFF47525E),
                fontWeight: FontWeight.w500,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        decoration: BoxDecoration(
            color: Color(0xFFF3F5F6),
            border: Border.all(color: Color(0xFF8492A6))),
        width: double.infinity,
        margin: EdgeInsets.symmetric(horizontal: 20),
        height: MediaQuery.of(context).size.height * 0.3,
      ),
    );
  }
}
