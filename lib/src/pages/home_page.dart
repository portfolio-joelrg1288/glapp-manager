import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/pages/incomes_page.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/models/menu_item_model.dart';
import 'package:glapp_manager/src/pages/appointments_page.dart';
import 'package:glapp_manager/src/pages/notifications_page.dart';
import 'package:glapp_manager/src/models/professional_model.dart';

class HomePage extends StatelessWidget {
  DataModel dataModel;
  MenuItemModel menuItemsModel;
  List<Widget> _children = [
    AppointmentsPage(),
    IncomesPage(),
    NotificationsPage(),
  ];

  @override
  Widget build(BuildContext context) {
    dataModel = Provider.of<DataModel>(context);
    menuItemsModel = Provider.of<MenuItemModel>(context);
    changeStatusDark();
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor: Color(dataModel.index == 2 ? 0xFFDD83A2 : 0xFFA3DCBE),
        iconTheme: IconThemeData(
          color: Color(dataModel.index == 2 ? 0xFF343F4B : 0xFFFFFFFF),
        ),
        title: Text(
          getAppBarTitle(dataModel.index),
          style: TextStyle(
            fontSize: 22,
            color: Color(dataModel.index == 2 ? 0xFFFFFFFF : 0xFF343F4B),
            fontWeight: FontWeight.w700,
          ),
        ),
        actions: [
          if (dataModel.index == 0)
            PopupMenuButton(
              onSelected: (value) => menuItemsModel.selected = value,
              icon: Icon(Icons.tune, size: 30.0, color: Color(0xFF343F4B)),
              itemBuilder: (BuildContext context) {
                return menuItemsModel.items.map((Map<dynamic, String> item) {
                  return PopupMenuItem<String>(
                    value: item.keys.first,
                    child: Text(
                      item.values.first,
                      style: TextStyle(
                        color: Color(0xFF3C4858),
                      ),
                    ),
                  );
                }).toList();
              },
            ),
          if (dataModel.index != 0) Container()
        ],
      ),
      endDrawer: SideMenu(),
      body: _children[dataModel.index],
      floatingActionButton: dataModel.index == 0
          ? Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FloatingActionButton(
                  heroTag: {'a': ''},
                  elevation: 0,
                  highlightElevation: 0,
                  child: Icon(Icons.add),
                  hoverColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  backgroundColor: Color(0xFFF95F62),
                  onPressed: () =>
                      Navigator.of(context).pushNamed('schedule-lock'),
                ),
                if (dataModel.isProfessional) SizedBox(height: 10),
                if (dataModel.isProfessional) _buildAvailabilityButton(context),
              ],
            )
          : null,
      bottomNavigationBar: BottomNavigation(),
    );
  }

  Widget _buildAvailabilityButton(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final professionalModel = Provider.of<ProfessionalModel>(context);
    final bool _available = professionalModel.availability;
    return FloatingActionButton.extended(
      elevation: 1,
      label: Container(
        width: width * 0.25,
        child: Text(
          _available ? 'Quitar disponibilidad' : 'Pasar a disponible',
          maxLines: 2,
          style: TextStyle(
            fontSize: 11,
            color: Color(_available ? 0xFF343F4B : 0xFFFFFFFF),
          ),
        ),
      ),
      icon: Icon(Icons.timer,
          size: 20, color: Color(_available ? 0xFF343F4B : 0xFFFFFFFF)),
      hoverColor: Colors.transparent,
      splashColor: Colors.transparent,
      backgroundColor: Color(_available ? 0xFFA3DCBE : 0xFFDD83A2),
      onPressed: () {
        if (_available)
          professionalModel.availability = false;
        else
          Navigator.pushNamed(context, 'account-settings');
      },
    );
  }
}
