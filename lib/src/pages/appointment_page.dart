import 'package:flutter/material.dart';
import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/providers/appointments_provider.dart';
import 'package:provider/provider.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/models/appointment_model.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class AppointmentPage extends StatefulWidget {
  @override
  _AppointmentPageState createState() => _AppointmentPageState();
}

class _AppointmentPageState extends State<AppointmentPage> {
  final dotenv = DotEnv();
  Size screenSize;
  AppointmentsProvider appointmentsProvider = new AppointmentsProvider();
  AppointmentModel appointmentModel;
  bool _isLoading = false;
  final _servicePrice = new MoneyMaskedTextController(thousandSeparator: '.', precision: 0, leftSymbol: '\$', decimalSeparator: '');

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    appointmentModel = Provider.of<AppointmentModel>(context);
    _servicePrice.text = appointmentModel.discountPrice.toString();
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFA3DCBE),
        iconTheme: IconThemeData(
          color: Color(0xFF343F4B), //change your color here
        ),
        title: Text(
          translateAppointmentStatus(appointmentModel.status),
          style: TextStyle(
            fontSize: 22,
            color: Color(0xFF343F4B),
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      body: (_isLoading)
      ? Center(child: CircularProgressIndicator(),)
      : Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: double.infinity,
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(30),
                  child: Text(
                    appointmentModel.client,
                    style: TextStyle(
                      fontSize: 20,
                      color: Color(0xFF47525E),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.end,
                  ),
                  color: Color(0xFFC0CCDA).withOpacity(0.39),
                ),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          _buildRow(
                            icon: appointmentModel.status == 'requested'
                                ? 'requested-1'
                                : 'calendar',
                            title: 'Estado de la cita:',
                            value: _formattedStatus(appointmentModel.status),
                          ),
                          _buildRow(
                            icon: appointmentModel.payment == 'T'
                                ? 'card-1'
                                : 'cash-1',
                            title: 'Forma de pago:',
                            value: _formattedPayment(appointmentModel.payment),
                          ),
                          _buildRow(
                            icon: appointmentModel.status == 'finished'
                                ? 'finished'
                                : 'pending',
                            value: appointmentModel.status == 'finished'
                                ? 'Pagado'
                                : 'Pendiente',
                            title: 'Estado del pago:',
                          ),
                          _buildRow(
                            icon: 'monetization_on-selected',
                            value: '${_servicePrice.text}',
                            title: 'Valor de reserva:'
                          ),
                          if (appointmentModel.employeeName != '') _buildRow(
                            icon: 'user',
                            value: '${appointmentModel.employeeName}',
                            title: 'Profesional:'
                          )
                        ],
                      ),
                      Container(
                        width: double.infinity,
                        color: Color(0xFFE6E8EB),
                        margin: EdgeInsets.symmetric(horizontal: 8),
                        padding: EdgeInsets.symmetric(
                          vertical: 8,
                          horizontal: 5,
                        ),
                        child: Column(
                          children: <Widget>[
                            _buildSummary(
                              icon: 'location',
                              title: 'Lugar y fecha de atención',
                              value:'${appointmentModel.place} \n ${appointmentModel.startDate.split('T')[0]}',
                            ),
                            SizedBox(height: 5),
                            _buildSummary(
                              icon: 'requested',
                              title: 'Servicio solicitado',
                              value: _servicesFormat(appointmentModel.serviceList)
                            ),
                            SizedBox(height: 5),
                            _buildSummary(
                              value: appointmentModel.comments,
                              icon: 'comments',
                              title: 'Comentarios del cliente',
                              finished: appointmentModel.status == 'finished',
                            ),
                          ],
                        ),
                      ),
                      if (appointmentModel.status == 'requested')
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            ButtonRed(
                              text: 'RECHAZAR',
                              width: screenSize.width * 0.3,
                              action: () => _rejectAppointmentDialog(context),
                            ),
                            ButtonGreen(
                              text: 'ACEPTAR',
                              width: screenSize.width * 0.3,
                              action: () async { 
                                setState(() {
                                  _isLoading = true;
                                });
                                final resp = await appointmentsProvider.appointmentAction(appointmentModel.appointmentId, 'aceptar');
                                setState(() {
                                  _isLoading = false;
                                });
                                Navigator.of(context).pushNamedAndRemoveUntil('home', (route) => false);
                              }
                            ),
                          ],
                        ),
                      if (appointmentModel.status == 'confirmed')
                        ButtonRed(
                          text: 'FINALIZAR ATENCIÓN',
                          width: screenSize.width * 0.7,
                          action: () => _finishAppointmentDialog(
                              context, appointmentModel),
                        ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(100.0),
              child: FadeInImage(
                placeholder: AssetImage('assets/images/user.png'),
                image: NetworkImage('${dotenv.env['BASE_URL']}${appointmentModel.image}'),
                width: screenSize.height * 0.13,
                height: screenSize.height * 0.13,
              ),
            ),
            padding: EdgeInsets.only(left: 20, top: 10),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigation(),
    );
  }

  String _formattedStatus(String status) {
    switch (status) {
      case 'requested':
        return 'Solicitada';
      case 'confirmed':
        return 'Agendada';
      case 'finished':
        return 'Terminada';
      default:
        return status;
    }
  }

  String _formattedPayment(String payment) {
    switch (payment) {
      case 'T':
        return 'Tarjeta de crédito';
      case 'E':
        return 'Efectivo';
      default:
        return payment;
    }
  }

  Widget _buildRow({String title, String value, String icon = 'business'}) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: screenSize.width * 0.33,
            child: Text(title, style: TextStyle(color: Colors.black)),
          ),
          Container(
            width: screenSize.width * 0.38,
            child: Text(
              value,
              textAlign: TextAlign.start,
              style: TextStyle(color: Colors.black),
            ),
          ),
          Image(image: AssetImage('assets/icons/$icon.png'), width: 20),
        ],
      ),
    );
  }

  Widget _buildSummary(
      {String title,
      String value,
      String icon = 'business',
      finished = false}) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(left: 10, bottom: 5),
      child: Column(
        children: <Widget>[
          Text(title, style: TextStyle(fontSize: 12)),
          SizedBox(height: 10),
          Row(
            children: <Widget>[
              finished
                  ? SmoothStarRating(
                      size: 15,
                      spacing: 0,
                      rating: 3.5,
                      starCount: 4,
                      isReadOnly: true,
                      color: Colors.black,
                    )
                  : Image(
                      image: AssetImage('assets/icons/$icon.png'),
                      width: 20,
                    ),
              Container(
                width: screenSize.width * (finished ? 0.6 : 0.8),
                child: Text(value, textAlign: TextAlign.center),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _rejectAppointmentDialog(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            '¿Estás seguro que deseas rechazar la cita?',
            style: TextStyle(fontWeight: FontWeight.w600),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'VOLVER',
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () => Navigator.pop(context),
            ),
            FlatButton(
              child: Text(
                'ACEPTAR',
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () async {
                setState(() {
                  _isLoading = true;
                });
                final ResponseModel resp = await appointmentsProvider.appointmentAction(appointmentModel.appointmentId, 'rechazar');
                setState(() {
                  _isLoading = false;
                });
                Navigator.of(context).pushNamedAndRemoveUntil('home', (route) => false);
              }
            ),
          ],
        );
      },
    );
  }

  Widget _finishAppointmentDialog(
    BuildContext context,
    AppointmentModel appointmentModel,
  ) {
    int _amount = 0;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: Text(
                'Estás finalizando la cita',
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(width: screenSize.width * 0.3),
                      Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: Image(
                            image: AssetImage('assets/icons/card-1.png'),
                            width: 20),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: Image(
                            image: AssetImage('assets/icons/cash.png'),
                            width: 20),
                      ),
                    ],
                  ),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: screenSize.width * 0.3,
                        child: Text(
                          'Método de pago',
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      Checkbox(
                        activeColor: Color(0xFF8290A5),
                        visualDensity: VisualDensity.compact,
                        value: appointmentModel.payment == 'T',
                        onChanged: (value) =>
                            setState(() => appointmentModel.payment = 'T'),
                      ),
                      Checkbox(
                        activeColor: Color(0xFF8290A5),
                        visualDensity: VisualDensity.compact,
                        value: appointmentModel.payment == 'E',
                        onChanged: (value) =>
                            setState(() => appointmentModel.payment = 'E'),
                      ),
                    ],
                  ),
                  SizedBox(height: 15),
                  Row(
                    children: <Widget>[
                      Container(
                        width: screenSize.width * 0.3,
                        child: Text(
                          'Total pagado',
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                      Container(
                        height: 25,
                        width: screenSize.width * 0.3,
                        child: TextField(
                          decoration: InputDecoration(
                            isDense: true,
                            border:
                            OutlineInputBorder(borderSide: BorderSide(width: 1)),
                          ),
                          textInputAction: TextInputAction.done,
                          keyboardType: TextInputType.number,
                          style: TextStyle(color: Colors.black, fontSize: 10),
                          onChanged: (value) => _amount = int.parse(value)
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    'VOLVER',
                    style: TextStyle(color: Colors.black),
                  ),
                  onPressed: () => Navigator.pop(context),
                ),
                FlatButton(
                  child: Text(
                    'GUARDAR',
                    style: TextStyle(color: Colors.black),
                  ),
                  onPressed: () async {
                    final body = {
                      'tipoPago': '${appointmentModel.payment}',
                      'monto': _amount
                    };
                    setState(() {
                      _isLoading = true;
                    });
                    final res = await appointmentsProvider.finishAppointment(appointmentModel.appointmentId, body);
                    setState(() {
                      _isLoading = false;
                    });
                    Navigator.of(context).pushNamedAndRemoveUntil('home', (route) => false);
                  }
                ),
              ],
            );
          },
        );
      },
    );
  }

  String _servicesFormat(List services) {
    String _formatedServices = '';
    services.forEach((element) {_formatedServices += '$element ';});
    return _formatedServices;
  }
}
