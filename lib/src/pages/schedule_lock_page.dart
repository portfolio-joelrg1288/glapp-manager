import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:glapp_manager/src/providers/appointments_provider.dart';
import 'package:glapp_manager/src/providers/user_actions_provider.dart';
import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/utils/constants.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/models/filters_model.dart';
import 'package:glapp_manager/src/models/business_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';

class ScheduleLockPage extends StatefulWidget {
  @override
  _ScheduleLockPageState createState() => _ScheduleLockPageState();
}

class _ScheduleLockPageState extends State<ScheduleLockPage> {
  DateTime endDate;
  TimeOfDay endTime;
  DateTime startDate;
  TimeOfDay startTime;
  final DateTime today = DateTime.now();
  final TimeOfDay todayTime = TimeOfDay.now();
  String _reason = 'Sin definir';
  final _storage = new FlutterSecureStorage();
  final UserActionsProvider userActionsProvider = UserActionsProvider();
  final AppointmentsProvider appointmentsProvider = new AppointmentsProvider();
  FiltersModel filtersModel;
  DataModel dataModel;
  Map<dynamic, String> _employees = {'0': 'Todos los empleados'};
  String _selectedtedEmployee = '0';

  @override
  void initState() {
    super.initState();
    _getEmployees();
    startDate = endDate = today;
    startTime = endTime = todayTime;
  }

  Future<Map<dynamic, String>> _getEmployees() async {
    // _employees = {'0': 'Todos los empleados'};
    final res = await userActionsProvider.getEmployees();
    for (var e in res.body['empleados']) {
      _employees.putIfAbsent(e['id'], () => e['nombre']);
    }   
    return _employees;
  }

  @override
  Widget build(BuildContext context) {
    dataModel = Provider.of<DataModel>(context);
    filtersModel = Provider.of<FiltersModel>(context);
    final businessModel = Provider.of<BusinessModel>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Bloqueo de agenda',
          style: TextStyle(fontWeight: FontWeight.w600),
        ),
        elevation: 0,
        backgroundColor: Color(0xFFDD83A2),
        actions: [Container()],
      ),
      endDrawer: SideMenu(),
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: SingleChildScrollView(
          physics: BouncingScrollPhysics(),
          child: Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.75,
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  if (dataModel.isProfessional)
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: () => Navigator.of(context).pop(),
                        icon: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  if (!dataModel.isProfessional)
                    Column(
                      children: <Widget>[
                        FutureBuilder(
                          future: _getEmployees(),
                          builder: (BuildContext context, AsyncSnapshot snapshot) {
                            if (!snapshot.hasData) {
                              return Center(child: CircularProgressIndicator(),);
                            } else {
                              return GenericSelect(
                                showHint: true,
                                labelText: 'Empleado',
                                color: Colors.transparent,
                                items: _employees,
                                selected: _selectedtedEmployee,
                                changeDropButton: (value) {
                                  setState(() {
                                    _selectedtedEmployee = value;
                                  });
                                }
                              );
                            }
                          },
                        ),
                        Container(
                          height: 1,
                          width: double.infinity,
                          color: Color(0xFF8492A6).withOpacity(0.5),
                          margin: EdgeInsets.symmetric(horizontal: 20),
                        ),
                      ],
                    ),
                  Container(
                    width: double.infinity,
                    color: Color(0xFFF0F2F7),
                    padding: EdgeInsets.all(10),
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    child: Column(
                      children: <Widget>[
                        _buildDateInputs(),
                        SizedBox(height: 5),
                        _buildTimeInputs(),
                        SizedBox(height: 5),
                        _buildReasonInput(),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      ButtonRed(
                        text: 'ANULAR',
                        action: () => Navigator.of(context).pop(),
                        width: MediaQuery.of(context).size.width * 0.3,
                      ),
                      SizedBox(width: MediaQuery.of(context).size.width * 0.1),
                      ButtonGreen(
                        text: 'GUARDAR',
                        action: _showDialog,
                        width: MediaQuery.of(context).size.width * 0.3,
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigation(),
    );
  }

  Widget _buildDateInputs() {
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        children: <Widget>[
          Text('Fecha', style: TextStyle(fontSize: 12)),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildStartDateDropdown(),
              _buildEndDateDropdown(),
            ],
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget _buildStartDateDropdown() {
    return RawMaterialButton(
      child: Container(
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1, color: Color(0xFF8592A6)),
          ),
        ),
        child: Row(
          children: <Widget>[
            Text(getFormattedDate(startDate)),
            Icon(Icons.arrow_drop_down),
          ],
        ),
      ),
      onPressed: () {
        showDatePicker(
                context: context,
                firstDate: today,
                initialDate: today,
                lastDate: DateTime(today.year, today.month + 1))
            .then((value) => setState(() {
                  startDate = value ?? startDate;
                }));
      },
    );
  }

  Widget _buildEndDateDropdown() {
    return RawMaterialButton(
      child: Container(
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1, color: Color(0xFF8592A6)),
          ),
        ),
        child: Row(
          children: <Widget>[
            Text(getFormattedDate(endDate)),
            Icon(Icons.arrow_drop_down),
          ],
        ),
      ),
      onPressed: () {
        showDatePicker(
                context: context,
                firstDate: startDate,
                initialDate: startDate,
                lastDate: DateTime(startDate.year, startDate.month + 1))
            .then((value) => setState(() {
                  endDate = value ?? endDate;
                }));
      },
    );
  }

  String getFormattedDate(DateTime date) {
    final String month = Constants.of(context).months[date.month - 1];
    return '${date.day}-${month.toUpperCase().substring(0, 3)}-${date.year}';
  }

  Widget _buildTimeInputs() {
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        children: <Widget>[
          Text('Hora', style: TextStyle(fontSize: 12)),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              _buildStartTimeDropdown(),
              _buildEndTimeDropdown(),
            ],
          ),
          SizedBox(height: 10),
        ],
      ),
    );
  }

  Widget _buildStartTimeDropdown() {
    return RawMaterialButton(
      child: Container(
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1, color: Color(0xFF8592A6)),
          ),
        ),
        child: Row(
          children: <Widget>[
            Text('${startTime.hour}:${_minutesFormat(startTime.minute)}'),
            Icon(Icons.arrow_drop_down),
          ],
        ),
      ),
      onPressed: () {
        showTimePicker(context: context, initialTime: todayTime)
            .then((value) => this.setState(() {
                  startTime = value ?? startTime;
                }));
      },
    );
  }

  Widget _buildEndTimeDropdown() {
    return RawMaterialButton(
      child: Container(
        padding: EdgeInsets.only(bottom: 5),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(width: 1, color: Color(0xFF8592A6)),
          ),
        ),
        child: Row(
          children: <Widget>[
            Text('${endTime.hour}:${_minutesFormat(endTime.minute)}'),
            Icon(Icons.arrow_drop_down),
          ],
        ),
      ),
      onPressed: () {
        showTimePicker(
          context: context,
          initialTime: startDate == endDate ? startTime : todayTime,
        ).then((value) => this.setState(() {
              endTime = value ?? endTime;
            }));
      },
    );
  }

  Widget _buildReasonInput() {
    return Container(
      color: Colors.white,
      width: double.infinity,
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Column(
        children: <Widget>[
          Text('Motivo del bloqueo', style: TextStyle(fontSize: 12)),
          SizedBox(height: 10),
          TextField(
            maxLines: 2,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 12),
            textInputAction: TextInputAction.done,
            decoration: InputDecoration(border: InputBorder.none),
            onChanged: (value) => _reason=value,
          ),
        ],
      ),
    );
  }

  void _showDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Detalle del bloqueo'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Día inicio: ${getFormattedDate(startDate)}'),
              Text('Día fin: ${getFormattedDate(endDate)}'),
              Text('Hora inicio: ${startTime.hour}:${_minutesFormat(startTime.minute)}'),
              Text('Hora fin: ${endTime.hour}:${_minutesFormat(endTime.minute)}'),
              if (!dataModel.isProfessional) Text('Empleado: ${_employees[_selectedtedEmployee]}'),
              Text('Motivo: $_reason'),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'CANCELAR',
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () =>
                  Navigator.popUntil(context, ModalRoute.withName('home')),
            ),
            FlatButton(
              child: Text(
                'ACEPTAR',
                style: TextStyle(color: Colors.black),
              ),
              onPressed: () async {
                final body = {
                  'empleado': (!dataModel.isProfessional)?'$_selectedtedEmployee':'',
                  'fechaReserva': '$startDate',
                  'estado': 'Bloqueo',
                  'horaReserva': '${startTime.hour}',
                  'minutoReserva': '${_minutesFormat(startTime.minute)}',
                  'horaFinReserva': '${endTime.hour}',
                  'minutoFinReserva': '${_minutesFormat(endTime.minute)}',
                  'motivo': _reason,
                  'fechaFinBloqueo': '$endDate'
                };
                final resp = await appointmentsProvider.lockSchedule(body);
                (resp.success)
                  ? Navigator.of(context).pushNamedAndRemoveUntil('home', (route) => false)
                  : showAlert(context, 'Error', '${resp.message}');
              }
            ),
          ],
        );
      },
    );
  }

  String _minutesFormat(int minutes) {
    return (minutes < 10) ?  '0$minutes' : '$minutes'; 
  }

}
