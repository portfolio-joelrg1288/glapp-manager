import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:email_validator/email_validator.dart';

import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/models/error_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/providers/auth_provider.dart';

class LoginPage extends StatelessWidget {
  double width;
  double height;
  DataModel dataModel;
  ErrorModel errorModel;
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  AuthProvider authProvider = new AuthProvider();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    errorModel = Provider.of<ErrorModel>(context);
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    _emailFocus.addListener(_emailFocusChange);
    _passwordFocus.addListener(_passwordFocusChange);
    dataModel = Provider.of<DataModel>(context, listen: false);
    changeStatusDark();
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: GestureDetector(
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 50),
                  child: Image(
                    width: width * 0.2,
                    image: AssetImage('assets/images/logo-2.png'),
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  physics: BouncingScrollPhysics(),
                  child: Column(children: <Widget>[
                    _buildEmailInput(context, height),
                    SizedBox(height: 6),
                    _buildPasswordInput(height),
                    SizedBox(height: 15),
                    Container(
                      width: double.infinity,
                      margin: EdgeInsets.only(left: 20),
                      child: Text(
                        '¿Olvidaste tu contraseña?',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.red,
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    ButtonGray(
                      text: 'LOG IN',
                      action: () => _login(context),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        margin: EdgeInsets.only(bottom: 80),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              '¿No tienes una cuenta?',
                              style: TextStyle(
                                fontSize: 12,
                              ),
                            ),
                            SizedBox(width: 10),
                            RawMaterialButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              onPressed: () => Navigator.pushNamed(
                                  context, 'register-options'),
                              child: Text(
                                'Regístrate ahora',
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Color(0xFFF95F62),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ]),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildEmailInput(BuildContext context, double size) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        focusNode: _emailFocus,
        controller: _emailController,
        onChanged: _emailValidator,
        textInputAction: TextInputAction.next,
        onSubmitted: (t) {
          onFocusChange(context, _emailFocus, _passwordFocus);
        },
        decoration: InputDecoration(
          errorText: errorModel.email,
          errorStyle: TextStyle(),
          isDense: true,
          labelText: 'Dirección de correo',
          labelStyle: TextStyle(color: Colors.grey, fontSize: size * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: size * 0.020),
        keyboardType: TextInputType.emailAddress,
      ),
    );
  }

  Widget _buildPasswordInput(double size) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: TextField(
        obscureText: true,
        focusNode: _passwordFocus,
        controller: _passwordController,
        onChanged: _passwordValidator,
        decoration: InputDecoration(
          errorText: errorModel.password,
          isDense: true,
          labelText: 'Contraseña',
          labelStyle: TextStyle(color: Colors.grey, fontSize: size * 0.020),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(
              width: 1,
              color: Color(0xFF8492A6).withOpacity(0.5),
            ),
          ),
        ),
        style: TextStyle(color: Colors.black, fontSize: size * 0.020),
        keyboardType: TextInputType.visiblePassword,
      ),
    );
  }

  void _emailFocusChange() {
    if (_emailController.text.trim() == '' && !_emailFocus.hasFocus) {
      errorModel.email = 'El correo es requerido';
    }
  }

  void _passwordFocusChange() {
    if (_passwordController.text.trim() == '' && !_passwordFocus.hasFocus) {
      errorModel.password = 'La contraseña es requerida';
    }
  }

  void _passwordValidator(String value) {
    if (value.trim() == '' || value == null) {
      errorModel.password = 'La contraseña es requerida';
    } else {
      errorModel.password = null;
    }
  }

  void _emailValidator(String value) {
    if (value.trim() == '' || value == null) {
      errorModel.email = 'El correo es requerido';
    } else if (!EmailValidator.validate(value)) {
      errorModel.email = 'El correo no es valido';
    } else {
      errorModel.email = null;
    }
  }

  void _login(BuildContext context) async {
    final body = {
      'email': _emailController.text,
      'password': _passwordController.text
    };
    final res = await authProvider.login(body);
    if (res.success) {
      dataModel.isProfessional = res.body['user_role'] != 'Local';
      errorModel.reset();
      Navigator.pushNamedAndRemoveUntil(context, 'home', (route) => false);
    } else
      showAlert(context, 'Error', res.body['message']);
  }
}
