import 'package:flutter/material.dart';
import 'package:glapp_manager/src/pages/appointments_page.dart';
import 'package:glapp_manager/src/providers/billing_provider.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/utils/helpers.dart';
import 'package:glapp_manager/src/utils/constants.dart';
import 'package:glapp_manager/src/models/filters_model.dart';
import 'package:glapp_manager/src/widgets/custom_widgets.dart';
import 'package:glapp_manager/src/models/appointment_model.dart';
import 'package:glapp_manager/src/models/professional_model.dart';

class MonthIncomesPage extends StatefulWidget {
  @override
  _MonthIncomesPageState createState() => _MonthIncomesPageState();
}

class _MonthIncomesPageState extends State<MonthIncomesPage> {
  List<AppointmentModel> appointments = new List();

  BillingProvider billingProvider = BillingProvider();

  @override
  void initState() { 
    super.initState();
    _getAppointments(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFFA3DCBE),
        iconTheme: IconThemeData(color: Color(0xFF343F4B)),
        title: Text(
          'Ingresos',
          style: TextStyle(
            fontSize: 20,
            color: Color(0xFF343F4B),
            fontWeight: FontWeight.w700,
          ),
        ),
      ),
      backgroundColor: Color(0xFFF0F2F7),
      body: Column(children:  _buildBody(context)),
    );
  }

  List<Widget> _buildBody(BuildContext context) {
    List<Widget> _result = [];
    if(appointments.length > 0) {
      _result.add(_buildSelect(context));
      final aux = appointments.map((e) => _buildAppointment(e, context)).toList();
      aux.add(SizedBox(height: 20));
      _result.add(Expanded(
      child: SingleChildScrollView(
          physics: BouncingScrollPhysics(), child: Column(children: aux)),
      ));
    } else {
     _result.add(Center(child: Container(child: Text('No hay informacion disponible'))));
    }
    return _result;
  }

  Widget _buildSelect(BuildContext context) {
    final filtersModel = Provider.of<FiltersModel>(context, listen: false);
    final professionalModel =
        Provider.of<ProfessionalModel>(context, listen: false);
    final Map<dynamic, String> _select = {
      0: '${Constants.of(context).months[professionalModel.month]}'
          ' / ${filtersModel.years[professionalModel.year]}'
    };
    return GenericSelect(
      showHint: true,
      selected: 0,
      items: _select,
      labelText: 'Año',
      width: double.infinity,
      changeDropButton: null,
    );
  }

  Widget _buildAppointment(AppointmentModel appointment, BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.only(top: 15, left: 15),
      padding: EdgeInsets.only(left: 5, bottom: 5, top: 3, right: 5),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
          left: BorderSide(
            width: 2,
            color: Color(0xFF278B00),
          ),
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.69,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Servicio: ${translateAppointmentService(appointment.service)}',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(height: 5),
                Text(
                  'Lugar: ${translateAppointmentService(appointment.place)}',
                  style: TextStyle(
                    fontSize: 12,
                    color: Color(0xFF343F4B),
                    fontWeight: FontWeight.w300,
                  ),
                ),
                Text(
                  'Cliente: ${appointment.client}',
                  style: TextStyle(
                    fontSize: 12,
                    color: Color(0xFF343F4B),
                    fontWeight: FontWeight.w300,
                  ),
                ),
                Text(
                  'Empleado: ${appointment.employeeName}',
                  style: TextStyle(
                    fontSize: 12,
                    color: Color(0xFF343F4B),
                    fontWeight: FontWeight.w300,
                  ),
                ),
                Text(
                  '${appointment.startDate.split('T')[0]}',
                  style: TextStyle(
                    fontSize: 12,
                    color: Color(0xFF343F4B),
                    fontWeight: FontWeight.w300,
                  ),
                ),
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.23,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                /*Text(
                  '\$ ${appointment.total.toStringAsFixed(2)}',
                  style: TextStyle(
                    fontSize: 17,
                    color: Color(0xFF278B00),
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.end,
                ),
                SizedBox(height: 5),*/
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Image(
                      width: 20,
                      color: appointment.payment == 'card'
                          ? Colors.black
                          : Colors.black12,
                      image: AssetImage('assets/icons/card.png'),
                    ),
                    SizedBox(width: 5),
                    Image(
                      width: 20,
                      color: appointment.payment == 'cash'
                          ? Colors.black
                          : Colors.black12,
                      image: AssetImage('assets/icons/cash.png'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _getAppointments(BuildContext context) async {
    await _callService(context);
  }

  Future<void> _callService(BuildContext context) async {
    final professionalModel = Provider.of<ProfessionalModel>(context, listen: false);
    final filtersModel = Provider.of<FiltersModel>(context, listen: false);

    String year = filtersModel.years[professionalModel.year];
    int month = (professionalModel.month + 1);

    final resp = await billingProvider.getBillingMonth(year, month.toString());

    if(resp.success) {
      if(resp.body['data'].length > 0) {
        resp.body['data'].forEach((item) {
          var appointmentTmp = AppointmentModel();
          appointmentTmp.service = item['servicio'];
          appointmentTmp.place = item['lugar'];
          appointmentTmp.client = item['nombre_cliente'];
          appointmentTmp.employeeName = item['nombre_empleado'];
          appointmentTmp.startDate = item['fecha'];
          appointmentTmp.payment = item['forma_pago'] == 'E' ? 'cash' : 'card';
          appointments.add(appointmentTmp);
        });
      }
    } else {
      Navigator.pop(context);
      showAlert(context, 'Error', resp.message);
    }

    setState(() {});
  }
}