import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/models/country_field_model.dart';
import 'package:glapp_manager/src/models/business_model.dart';
import 'package:glapp_manager/src/models/error_model.dart';

CountryFieldModel countryFieldModel;
BusinessModel businessModel;
ErrorModel errorModel;

void getContextProvider(BuildContext context) {
  countryFieldModel = Provider.of<CountryFieldModel>(context);
  businessModel = Provider.of<BusinessModel>(context); 
  errorModel = Provider.of<ErrorModel>(context);
}

void resetModels() {
  countryFieldModel.reset();
  businessModel.reset();
  errorModel.reset();
}