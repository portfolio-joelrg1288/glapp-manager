import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:email_validator/email_validator.dart';

import 'package:glapp_manager/src/models/error_model.dart';

ErrorModel errorModel;
TextEditingController passwordController;

void getContextProvider(BuildContext context) {
  errorModel = Provider.of<ErrorModel>(context);
}

void getPasswordController(TextEditingController passController) {
  passwordController = passController;
}

void legalNameValidator(String value) {
  if (value.trim() == '' || value == null) 
    errorModel.name = 'El nombre legal es requerido';
  else 
    errorModel.name = null;
}

void commercialNameValidator(String value) {
  if (value.trim() == '' || value == null)
    errorModel.lastName = 'El nombre comercial es requerido';
  else
    errorModel.lastName = null;
}

void nameValidator(String value) {
  if (value.trim() == '' || value == null) {
    errorModel.name = 'El nombre es requerido';
  } else {
    errorModel.name = null;
  }
}

void lastNameValidator(String value) {
  if (value.trim() == '' || value == null) {
    errorModel.lastName = 'El apellido es requerido';
  } else {
    errorModel.lastName = null;
  }
}

void rutValidator(String value) {
  if (value.trim() == '' || value == null)
    errorModel.rut = 'El rut es requerido';
  else
    errorModel.rut = null;
}

void addressValidator(String value) {
  if (value.trim() == '' || value == null)
    errorModel.address = 'La dirección es requerida';
  else
    errorModel.address = null;
}

void emailValidator(String value) {
  if (value.trim() == '' || value == null) {
    errorModel.email = 'El correo es requerido';
  } else if (!EmailValidator.validate(value)) {
    errorModel.email = 'El correo no es valido';
  } else {
    errorModel.email = null;
  }
}

void passwordValidator(String value) {
  if (value.trim() == '' || value == null)
    errorModel.password = 'La contraseña es requerida';
  else if(value.trim().length < 6)
    errorModel.password = 'La contraseña debe ser de mínimo 6 carácteres';
  else
    errorModel.password = null;
}

void repeatPasswordValidator(String value) {
  if (value.trim() != passwordController.text.trim())
    errorModel.repeatPassword = 'La contraseñas no coinciden';
  else
    errorModel.repeatPassword = null;
}

void phoneNumberValidator(String value) {
  if (value.trim() == '' || value == null)
    errorModel.phoneNumber = 'El teléfono es requerido';
  else
    errorModel.phoneNumber = null;
}