import 'package:flutter/material.dart';
import 'package:flutter/services.dart' as services;

void changeStatusLight() {
  services.SystemChrome.setSystemUIOverlayStyle(
      services.SystemUiOverlayStyle.light);
}

void changeStatusDark() {
  services.SystemChrome.setSystemUIOverlayStyle(
      services.SystemUiOverlayStyle.dark);
}

void onFocusChange(BuildContext context, FocusNode current, FocusNode next) {
  current.unfocus();
  FocusScope.of(context).requestFocus(next);
}

String traslateFromSpanishStatus(String status) {
  switch (status) {
    case 'Rechazada': 
      return 'cancelled';
    case 'Pendiente':
      return 'requested';
    case 'Aceptada':
      return 'confirmed';
    case 'Pagada':
      return 'finished';
    case 'Bloqueo':
      return 'block';
    default:
      return status;
  }
}

String translateAppointmentStatus(String status) {
  switch (status) {
    case 'cancelled':
      return 'Cita Cancelada';
    case 'requested':
      return 'Solicitud de Cita';
    case 'confirmed':
      return 'Cita agendada';
    case 'finished':
      return 'Cita terminada';
    case 'block':
      return 'Bloqueo';
    default:
      return status;
  }
}

String translateAppointmentService(String service) {
  switch (service) {
    case 'haircut':
      return 'Corte de cabello';
    case 'manicure':
      return 'Manicure';
    case 'pedicure':
      return 'Pedicure';
    default:
      return service;
  }
}

Color getAppointmentStatusColor(String status) {
  switch (status) {
    case 'cancelled':
      return Color(0xFFF95F62);
    case 'requested':
      return Colors.amber;
    case 'confirmed':
      return Colors.blue;
    case 'finished':
      return Color(0xFF278B00);
    case 'block': 
      return Colors.black54;
    default:
      return Color(0xFF278B00);
  }
}

String getAppBarTitle(int index) {
  switch (index) {
    case 0:
      return 'Mi Agenda';
    case 1:
      return 'Ingresos';
    case 2:
      return 'Notificaciones';
    default:
      return 'Mi Agenda';
  }
}

String getCountryPrefix(String prefix) {
  switch (prefix) {
    case '+54':
      return 'ar';
    case '+56':
      return 'cl';
    case '+57':
      return 'co';
    default:
      return null;
  }
}

String getCountry(String countryPrefix) {
  if (countryPrefix == 'ar')
    return 'Argentina';
  else if (countryPrefix == 'cl')
    return 'Chile';
  else if (countryPrefix == 'co')
    return 'Colombia';
  else
    return '';
}

String getCity(String countryPrefix, String colombianCity, String city) {
  return (countryPrefix == 'co') ? colombianCity : city.split(',')[0];
}

void showAlert(BuildContext context, String title, String message) {
  showDialog(
      context: context,
      barrierDismissible: (title == 'Exito!')? false : true,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[Text(message)]),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () {
                (title == 'Exito!') 
                ? Navigator.of(context).pushReplacementNamed('home')
                : Navigator.of(context).pop();
              },
            )
          ],
        );
      });
}
