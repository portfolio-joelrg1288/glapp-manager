import 'package:flutter/material.dart';

import 'package:glapp_manager/src/models/countries_model.dart';

class PhoneSelector extends StatelessWidget {
  final double fontSize;
  final String errorText;
  final String labelText;
  final String initialValue;
  final FocusNode focusNode;
  final Function changeTextField;
  final Function changeDropButton;
  final Function onSubmittedTextField;
  final TextInputAction textInputAction;
  final TextEditingController controller;
  final TextInputType keyboardType;

  PhoneSelector({
    @required this.labelText,
    @required this.focusNode,
    @required this.errorText,
    @required this.controller,
    @required this.initialValue,
    @required this.changeTextField,
    @required this.changeDropButton,
    @required this.onSubmittedTextField,
    @required this.keyboardType,
    this.textInputAction = TextInputAction.done,
    this.fontSize = 0,
  });

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    return Row(
      children: <Widget>[
        DropdownButtonHideUnderline(
          child: DropdownButton(
            isDense: true,
            items: _generateItems(_screenSize),
            value: initialValue,
            onChanged: changeDropButton,
          ),
        ),
        Container(
          width: _screenSize.width * 0.6,
          child: TextField(
            controller: controller,
            focusNode: focusNode,
            onChanged: changeTextField,
            textInputAction: textInputAction,
            onSubmitted: onSubmittedTextField,
            keyboardType: keyboardType,
            decoration: InputDecoration(
              isDense: true,
              errorText: errorText,
              labelText: labelText,
              labelStyle: TextStyle(
                color: Colors.grey,
                fontSize: _screenSize.height * 0.020,
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  width: 1,
                  color: Color(0xFF8492A6).withOpacity(0.5),
                ),
              ),
            ),
            style: TextStyle(
              color: Colors.black,
              fontSize: fontSize == 0 ? _screenSize.height * 0.025 : fontSize,
            ),
          ),
        ),
      ],
    );
  }

  List<DropdownMenuItem> _generateItems(Size screen) {
    List<CountriesModel> items = [
      CountriesModel(1, '+57', 'assets/images/colombia.png'),
      CountriesModel(2, '+56', 'assets/images/chile.png'),
      CountriesModel(3, '+54', 'assets/images/argentina.png'),
    ];
    List<DropdownMenuItem> result = new List();

    for (final item in items) {
      final aux = DropdownMenuItem(
        value: item.phoneCode,
        child: Row(
          children: <Widget>[
            Image(
                image: AssetImage(item.imageAsset),
                height: screen.height * 0.025),
            Text(item.phoneCode)
          ],
        ),
      );
      result.add(aux);
    }
    return result;
  }
}
