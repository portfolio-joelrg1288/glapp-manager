import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/models/country_field_model.dart';
import 'package:glapp_manager/src/models/error_model.dart';

class CountryFields extends StatelessWidget {
  Size _screenSize;
  ErrorModel errorModel;
  CountryFieldModel countryFieldModel;
  final FocusNode focusNode1;
  final FocusNode focusNode2;
  final TextEditingController textController1;

  CountryFields(this.focusNode1, this.focusNode2, this.textController1);

  @override
  Widget build(BuildContext context) {
    _screenSize = MediaQuery.of(context).size;
    countryFieldModel = Provider.of<CountryFieldModel>(context);
    errorModel = Provider.of<ErrorModel>(context);
    focusNode1.addListener(_field1FocusChange);
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(height: 5),
          Visibility(
           child: _createDropDownButton(context),
           visible: countryFieldModel.visible,
          ),
          SizedBox(height: 5),
          Visibility(
            child: _createTextField2(),
            visible: countryFieldModel.visible2,  
          ),
        ],
      )
    );
  }

  Widget _createDropDownButton(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
     return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('${countryFieldModel.field}',
          style: TextStyle(
            fontFamily: 'OpenSans-Regular',
            color: Colors.grey,
            fontSize: screenSize.height * 0.020,
          ),
        ),
        SizedBox(width: screenSize.width * 0.020),
        Container(
          width: screenSize.width * 0.9,
          child: DropdownButton(
          isExpanded: true,
          focusNode: focusNode2,
          dropdownColor: Colors.white,
            style: TextStyle(
              fontFamily: 'OpenSans-Regular',
              color: Colors.black,
              fontSize: screenSize.height * 0.020,
            ),
            
            iconEnabledColor: Colors.black,
            value: countryFieldModel.listValue,
            items: _getLocalitiesDropdown(),
            onChanged: ((opt) {
                countryFieldModel.listValue = opt;
            }),
          ),
        )
      ],
    );
  }

   Widget _createTextField2() {
    return Column(
      children: <Widget>[
        Container(
          width: _screenSize.width * 0.9,
          child: TextField(
            focusNode: focusNode2,
            controller: textController1,
            onChanged: _fieldValidator1,
            style: TextStyle(
              color: Colors.black,
              fontSize: _screenSize.height * 0.025,
              fontFamily: 'OpenSans-Regular',
            ),
            obscureText: false,
            textInputAction: TextInputAction.next,
            decoration: InputDecoration(
              labelText: 'Casa/Piso/Edificio',
              labelStyle: TextStyle(
                color: Colors.grey,
                fontSize: _screenSize.height * 0.020,
                fontFamily: 'OpenSans-Regular',
              ),
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.blue,
                  width: 2.0
                )
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.grey,
                  width: 0.5
                )
              )
            ),
          ),
        )
      ],
    );
  }

   List<DropdownMenuItem<String>> _getLocalitiesDropdown() {
    List<DropdownMenuItem<String>> list = new List();

    countryFieldModel.localities.forEach((item) { 
      list.add(DropdownMenuItem(
        child: Text(item['nombre']),
        value: item['nombre']
      ));
    });

    return list;
  }

  void _fieldValidator1(String value) {
    if (value.trim() == '' || value == null)
      errorModel.countryCustomField1 = 'Campo requerido';
    else
      errorModel.countryCustomField1 = null;
  }

  void _field1FocusChange() {
    if (textController1.text.trim() == '' && !focusNode1.hasFocus)
      errorModel.countryCustomField1 = 'Campo requerido';
  }
}