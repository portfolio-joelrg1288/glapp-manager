import 'package:flutter/material.dart';

class ButtonRed extends StatelessWidget {
  final String text;
  final double width;
  final Function action;

  ButtonRed({this.text = '', @required this.action, this.width = 100.0});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: this.action,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        child: Text(
          this.text,
          style: TextStyle(
            fontSize: 16,
            color: Colors.white,
            fontWeight: FontWeight.w700,
          ),
          textAlign: TextAlign.center,
        ),
        decoration: BoxDecoration(
          color: Color(0xFFDD83A2),
          borderRadius: BorderRadius.all(Radius.circular(3)),
        ),
        width: width,
        padding: EdgeInsets.symmetric(vertical: 10),
      ),
    );
  }
}
