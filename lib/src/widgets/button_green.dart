import 'package:flutter/material.dart';

class ButtonGreen extends StatelessWidget {
  final String text;
  final double width;
  final Function action;

  ButtonGreen({this.text = '', @required this.action, this.width = 100.0});

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: this.action,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        child: Text(
          this.text,
          style: TextStyle(
            fontSize: 16,
            fontWeight: FontWeight.w700,
          ),
          textAlign: TextAlign.center,
        ),
        decoration: BoxDecoration(
          color: Color(0xFFA3DCBE),
          borderRadius: BorderRadius.all(Radius.circular(3)),
        ),
        width: width,
        padding: EdgeInsets.symmetric(vertical: 10),
      ),
    );
  }
}
