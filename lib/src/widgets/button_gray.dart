import 'package:flutter/material.dart';

class ButtonGray extends StatelessWidget {
  final String text;
  final bool cancel;
  final double margin;
  final Function action;

  ButtonGray({
    this.text = '',
    this.margin = 20.0,
    this.cancel = false,
    @required this.action,
  });

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: this.action,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        child: Text(
          this.text,
          style: TextStyle(
            fontSize: 16,
            fontFamily: 'OpenSans',
            color: Colors.white,
            fontWeight: FontWeight.w500,
          ),
          textAlign: TextAlign.center,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(3)),
          color: Color(0xFF47525E).withOpacity(cancel ? 0.5 : 1),
        ),
        width: double.infinity,
        padding: EdgeInsets.symmetric(vertical: 15),
        margin: EdgeInsets.symmetric(horizontal: margin),
      ),
    );
  }
}
