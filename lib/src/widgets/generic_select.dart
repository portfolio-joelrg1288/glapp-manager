import 'package:flutter/material.dart';

class GenericSelect extends StatelessWidget {
  final Color color;
  final double width;
  final dynamic selected;
  final String labelText;
  final double verticalPadding;
  final double horizontalPadding;
  final Function changeDropButton;
  final Map<dynamic, String> items;
  final Text hint;
  final bool showHint;

  GenericSelect({
    this.width = 0,
    this.selected = 0,
    this.items = const {},
    @required this.labelText,
    this.verticalPadding = 15,
    this.horizontalPadding = 10,
    @required this.changeDropButton,
    this.color = const Color(0xFFFFFFFF),
    this.hint,
    this.showHint
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
      width: width != 0 ? width : double.infinity,
      margin: EdgeInsets.symmetric(horizontal: width != 0 ? 0 : 10),
      padding: EdgeInsets.symmetric(
        vertical: verticalPadding,
        horizontal: horizontalPadding,
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          hint: hint,
          isDense: true,
          value: showHint ? selected : null,
          items: _buildItems(),
          onChanged: changeDropButton,
          iconDisabledColor: Colors.black,
          disabledHint: Text(items[selected]),
        ),
      ),
    );
  }

  List<DropdownMenuItem> _buildItems() {
    List<DropdownMenuItem> result = [];
    items.forEach((key, value) {
      result.add(DropdownMenuItem(
        value: key,
        child: Text(value),
      ));
    });
    return result;
  }
}
