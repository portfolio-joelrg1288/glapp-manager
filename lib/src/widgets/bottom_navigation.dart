import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:glapp_manager/src/models/data_model.dart';

class BottomNavigation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final dataModel = Provider.of<DataModel>(context);
    return ClipRRect(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(screenSize.width * 0.05),
          topRight: Radius.circular(screenSize.width * 0.05),
      ),
      child: BottomNavigationBar(
        showSelectedLabels: false,
        showUnselectedLabels: false,
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.black,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Image(
              image: AssetImage('assets/icons/calendar_today${dataModel.index == 0 ? '-selected' : ''}.png'),
              width: screenSize.width * 0.07,
              ),
            title: SizedBox(height: 0),
          ),
          BottomNavigationBarItem(
            icon: Image(
              image: AssetImage('assets/icons/monetization_on${dataModel.index == 1 ? '-selected' : ''}.png'),
              width: screenSize.width * 0.07,
              ),
            title: SizedBox(height: 0),
          ),
          BottomNavigationBarItem(
            icon: Image(
              image: AssetImage('assets/icons/mail_outline${dataModel.index == 2 ? '-selected' : ''}.png'),
              width: screenSize.width * 0.07,
              ),
            title: SizedBox(height: 0),
          ),
         BottomNavigationBarItem(
            icon: Image(
              image: AssetImage('assets/icons/menu${dataModel.index == 3 ? '-selected' : ''}.png'),
              width: screenSize.width * 0.07,
              ),
            title: SizedBox(height: 0),
          ),
        ],
        onTap: (value) => _onSelectOption(value, context, dataModel),
      ),
    );
  }

  void _onSelectOption(int value, BuildContext context, DataModel dataModel) {
    if (value == 3) {
      Scaffold.of(context).openEndDrawer();
      return;
    }
    if (ModalRoute.of(context).settings.name != 'home') {
      dataModel.index = value;
      Navigator.popUntil(context, ModalRoute.withName('home'));
      return;
    }
    dataModel.index = value;
  }
}
