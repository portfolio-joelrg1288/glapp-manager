import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/providers/auth_provider.dart';
import 'package:glapp_manager/src/models/professional_model.dart';

class SideMenu extends StatelessWidget {
  ProfessionalModel professionalModel;
  final storage = new FlutterSecureStorage();
  AuthProvider authProvider = AuthProvider();

  @override
  Widget build(BuildContext context) {
    Size _screenSize = MediaQuery.of(context).size;
    professionalModel = Provider.of<ProfessionalModel>(context);
    final dataModel = Provider.of<DataModel>(context, listen: false);
    return Container(
      width: _screenSize.width * 0.85,
      child: Drawer(
        child: Container(
          color: Color(0xFF1E2D3E),
          child: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  color: Color(0xFF222932),
                  height: _screenSize.height * 0.28,
                  child: SafeArea(
                    child: Column(
                      children: <Widget>[
                        Align(
                          child: IconButton(
                            icon: Icon(
                              Icons.close,
                              color: Colors.white,
                            ),
                            onPressed: () => Navigator.of(context).pop(),
                          ),
                          alignment: Alignment.topRight,
                        ),
                        Image(
                          color: Colors.white,
                          width: _screenSize.height * 0.23,
                          image: AssetImage('assets/images/logo.png'),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: _screenSize.height * 0.45,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      _buildOption(
                          text:
                              '${dataModel.isProfessional ? 'Mi ' : ''}Agenda',
                          action: () {
                            dataModel.index = 0;
                            Navigator.popUntil(
                                context, ModalRoute.withName('home'));
                          }),
                      _buildOption(
                          text: 'Mi Perfil',
                          action: () {
                            if (ModalRoute.of(context).settings.name ==
                                'profile')
                              Navigator.pop(context);
                            else
                              Navigator.popAndPushNamed(context, 'profile');
                          }),
                      if (!dataModel.isProfessional)
                        _buildOption(
                            text: 'Mis Empleados',
                            action: () => Navigator.pop(context)),
                      if (dataModel.isProfessional)
                        _buildOption(
                            text: 'Ajuste de mis horarios y servicios',
                            action: () => Navigator.popAndPushNamed(
                                context, 'account-settings')),
                      _buildOption(
                          text: 'Facturación',
                          action: () {
                            dataModel.index = 1;
                            Navigator.popUntil(
                                context, ModalRoute.withName('home'));
                          }),
                      _buildOption(
                          text: 'Notificaciones',
                          action: () {
                            dataModel.index = 2;
                            Navigator.popUntil(
                                context, ModalRoute.withName('home'));
                          }),
                    ],
                  ),
                ),
                Container(
                  height: 0.5,
                  color: Colors.white,
                  width: double.infinity,
                ),
                Container(
                  width: double.infinity,
                  height: _screenSize.height * 0.2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      _buildOption(
                          text: 'Cerrar sesión',
                          action: () async {
                            dataModel.index = 0;
                            await storage.delete(key: 'user');
                            Navigator.pushNamedAndRemoveUntil(
                                context, 'login', (route) => false);
                          }),
                      _buildOption(
                          text: 'Términos y condiciones',
                          action: () => print('términos y condiciones')),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildOption({String text, Function action}) {
    return RawMaterialButton(
      onPressed: action,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      child: Container(
        width: double.infinity,
        padding: EdgeInsets.only(top: 10, bottom: 10, left: 35),
        child: Text(
          text,
          style: TextStyle(color: Colors.white, fontSize: 13),
        ),
      ),
    );
  }
}