import 'package:flutter/material.dart';

class ServiceDetail {
  String client = '';
  int price = 0;
  int discount = 0;
  int time = 0;

  ServiceDetail({this.client, this.price=0, this.discount=0, this.time=0});

  Map<String, dynamic> toJson() => {
    'aplica': client,
    'precio': price,
    'descuento': discount,
    'tiempo': time 
  };

}

class AddServicesModel with ChangeNotifier {
  String _mainService = '';
  String _serviceName = '';
  List<String> _serviceClient = [];
  List<ServiceDetail> _serviceDetail = [];

  String get mainService => this._mainService;
  set mainService(String value) {
    this._mainService = value;
    notifyListeners();
  }

  String get serviceName => this._serviceName;
  set serviceName(String value) {
    this._serviceName = value;
    notifyListeners();
  }

  List<String> get serviceClient => this._serviceClient;
  set serviceClient(value) {
    this._serviceClient.add(value);
    this._serviceDetail.add(new ServiceDetail(
      client: value,
    ));
    notifyListeners();
  }

  set deleteServiceClient(value) {
    this._serviceClient.remove(value);
    this._serviceDetail.removeWhere((element) => element.client == value);
    notifyListeners();
  }

  List<ServiceDetail> get serviceDetail => this._serviceDetail;
  set serviceDetail(value) {
    this._serviceDetail = value;
    notifyListeners();
  }

  set serviceDetailPrice(value) {
    ServiceDetail auxClient = this.serviceDetail.firstWhere((element) => element.client == value.client);
    auxClient.price = value.price;
  }

  set serviceDetailDiscount(value) {
    ServiceDetail auxClient = this.serviceDetail.firstWhere((element) => element.client == value.client);
    auxClient.discount = value.discount;
  }

  set serviceDetailTime(value) {
    ServiceDetail auxClient = this.serviceDetail.firstWhere((element) => element.client == value.client);
    auxClient.time = value.time;
  }

  Map<String , dynamic> toJson() => {
    'servicio_principal': _mainService,
    'aplica': List<String>.from(_serviceClient.map((c) => c)),
    'nombre_servicio': _serviceName,
    'precios': List<dynamic>.from(_serviceDetail.map((d) => d.toJson())),
  };

}