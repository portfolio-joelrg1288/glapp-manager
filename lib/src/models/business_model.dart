import 'package:flutter/material.dart';

class BusinessModel with ChangeNotifier {
  dynamic _town = 0;
  dynamic _city = 0;
  String _photo = '';
  dynamic _employee = -1;
  String _prefix = '+57';
  dynamic _neighborhood = 0;
  dynamic _businessType = 0;
  bool _availability = true;
  String _rut = '22.876.555';
  String _name = 'Peluquería';
  String _phone = '300567876554';
  String _email = 'peluqueria@pelos.com';
  List<Map<dynamic, double>> _services = [
    {0: 50},
    {1: 100},
    {2: 80}
  ];
  String _fantasyName = 'Peluquería S.A de C.V';
  List<Map<String, dynamic>> _notifications = [
    {
      'type': 0, // Service request
      'read': true,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 1, // Service rating
      'read': false,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 2, // Service cancellation
      'read': false,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 0, // Service request
      'read': true,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 0, // Service request
      'read': false,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 1, // Service rating
      'read': true,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    }
  ];

  String get rut => this._rut;

  String get name => this._name;

  dynamic get city => this._city;

  dynamic get town => this._town;

  String get photo => this._photo;

  String get phone => this._phone;

  String get email => this._email;

  String get prefix => this._prefix;

  dynamic get employee => this._employee;

  String get fantasyName => this._fantasyName;

  bool get availability => this._availability;

  dynamic get businessType => this._businessType;

  dynamic get neighborhood => this._neighborhood;

  List<Map<dynamic, double>> get services => this._services;

  List<Map<String, dynamic>> get notifications => this._notifications;

  set rut(String value) {
    this._rut = value;
    notifyListeners();
  }

  set name(String value) {
    this._name = value;
    notifyListeners();
  }

  set photo(String value) {
    this._photo = value;
    notifyListeners();
  }

  set town(dynamic value) {
    this._town = value;
    notifyListeners();
  }

  set city(dynamic value) {
    this._city = value;
    notifyListeners();
  }

  set phone(String value) {
    this._phone = value;
    notifyListeners();
  }

  set email(String value) {
    this._email = value;
    notifyListeners();
  }

  set prefix(String value) {
    this._prefix = value;
    notifyListeners();
  }

  set employee(dynamic value) {
    this._employee = value;
    notifyListeners();
  }

  set fantasyName(String value) {
    this._fantasyName = value;
    notifyListeners();
  }

  set availability(bool value) {
    this._availability = value;
    notifyListeners();
  }

  set businessType(dynamic value) {
    this._businessType = value;
    notifyListeners();
  }

  set neighborhood(dynamic value) {
    this._neighborhood = value;
    notifyListeners();
  }

  set services(List<Map<dynamic, double>> value) {
    this._services = value;
    notifyListeners();
  }

  set notifications(List<Map<String, dynamic>> value) {
    this._notifications = value;
    notifyListeners();
  }

  void reset() {
    town = 0;
    city = 0;
    photo = '';
    employee = -1;
    prefix = '+57';
    neighborhood = 0;
    businessType = 0;
    availability = true;
    rut = '22.876.555';
    name = 'Peluquería';
    phone = '300567876554';
    email = 'peluqueria@pelos.com';
  }
}
