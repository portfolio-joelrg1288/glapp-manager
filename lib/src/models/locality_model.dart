class LocalityModel {
  String id;
  String name;
  String country;
  List<dynamic> comuna;

  LocalityModel({
    this.id,
    this.country,
    this.name,
    this.comuna,
  });

  LocalityModel.fromJsonMap(Map<String, dynamic> json) {
    id = json['_id'];
    name = json['nombre'];
    country = json['pais'];
    comuna = json['comunas'];
  }
}