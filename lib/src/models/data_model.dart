import 'dart:io';

import 'package:flutter/material.dart';

class DataModel with ChangeNotifier {
  int _index = 0;
  File _selectedPicture;
  List<File> _selectedPictures = new List();
  bool _isProfessional = true;
  Map<String, dynamic> _notification = {
    'type': 0, // Service request
    'read': true,
    'service': 'manicure',
    'date': DateTime.now(),
    'place': 'Cra 54 d 155-65'
  };

  int get index => this._index;

  bool get isProfessional => this._isProfessional;

  File get selectedPicture => this._selectedPicture;

  List<File> get selectedPictures => this._selectedPictures;

  Map<String, dynamic> get notification => this._notification;

  set index(int value) {
    this._index = value;
    notifyListeners();
  }

  set isProfessional(bool value) {
    this._isProfessional = value;
    notifyListeners();
  }

  set selectedPicture(File value) {
    this._selectedPicture = value;
    notifyListeners();
  }

  set notification(Map<String, dynamic> value) {
    this._notification = value;
    notifyListeners();
  }

  set selectedPictures(List<File> value) {
    this._selectedPictures = value;
    notifyListeners();
  }
}
