import 'package:flutter/material.dart';

class MenuItemModel with ChangeNotifier {
  dynamic _selected = 'all';
  List<Map<dynamic, String>> _items = [
    {'all': 'Todas las citas'},
    {'requested': 'Solicitadas'},
    {'confirmed': 'Agendadas'},
    {'finished': 'Terminadas'}
  ];

  dynamic get selected => this._selected;

  List<Map<dynamic, String>> get items => this._items;

  set selected(dynamic value) {
    this._selected = value;
    notifyListeners();
  }

  set items(List<Map<dynamic, String>> value) {
    this._items = value;
    notifyListeners();
  }
}
