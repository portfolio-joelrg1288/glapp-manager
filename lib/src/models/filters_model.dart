import 'package:flutter/material.dart';

class FiltersModel with ChangeNotifier {
  Map<dynamic, String> _years = {0: '${DateTime.now().year}'};
  Map<dynamic, bool> _clients = {
    'men': false,
    'women': false,
    'child': false,
  };
  Map<dynamic, String> _towns = {
    0: 'Cuauhtémoc',
    1: 'Benito Juárez',
    2: 'Miguel Hidalgo',
    3: 'Tlalpan',
  };
  Map<dynamic, String> _cities = {
    0: 'Ciudad de México',
    1: 'Guadalajara',
    3: 'Cholula',
    2: 'Monterrey',
  };
  Map<dynamic, String> _services = {
    0: 'Corte',
    1: 'Tdynamicura',
    2: 'Peinado',
  };
  Map<dynamic, String> _allServices = {
    0: 'Peluqueria',
    1: 'Tintura',
    2: 'Maquillaje',
    3: 'Extensiones',
    4: 'Pestañas',
    5: 'Uñas',
    6: 'Depilación',
    7: 'Spa',
    8: 'Barberia',
  };
  Map<dynamic, String> _allEmployees = {
    -1: 'Todos los empleados',
    0: 'Anita Cárdenas',
    1: 'Juanito Pérez',
    2: 'Esperanza Gómez',
  };
  Map<dynamic, String> _neighborhoods = {
    0: 'Polanco',
    1: 'Mariano Escobedo',
    2: 'Bosques de Chapultepec',
    3: 'Bosques de Reforma'
  };
  Map<dynamic, String> _allBusinessTypes = {
    0: 'Barbería',
    1: 'SPA',
    2: 'Salón de belleza',
    3: 'Centro de estética',
    4: 'Peluquería infantil',
    5: 'Peluquería',
  };

  Map<dynamic, String> get years => this._years;

  Map<dynamic, String> get towns => this._towns;

  Map<dynamic, bool> get clients => this._clients;

  Map<dynamic, String> get cities => this._cities;

  Map<dynamic, String> get services => this._services;

  Map<dynamic, String> get allServices => this._allServices;

  Map<dynamic, String> get allEmployees => this._allEmployees;

  Map<dynamic, String> get neighborhoods => this._neighborhoods;

  Map<dynamic, String> get allBusinessTypes => this._allBusinessTypes;

  set years(Map<dynamic, String> value) {
    this._years = value;
    notifyListeners();
  }

  set towns(Map<dynamic, String> value) {
    this._towns = value;
    notifyListeners();
  }

  set clients(Map<dynamic, bool> value) {
    this._clients = value;
    notifyListeners();
  }

  set cities(Map<dynamic, String> value) {
    this._cities = value;
    notifyListeners();
  }

  set services(Map<dynamic, String> value) {
    this._services = value;
    notifyListeners();
  }

  set allServices(Map<dynamic, String> value) {
    this._allServices = value;
    notifyListeners();
  }

  set allEmployees(Map<dynamic, String> value) {
    this._allEmployees = value;
    notifyListeners();
  }

  set neighborhoods(Map<dynamic, String> value) {
    this._neighborhoods = value;
    notifyListeners();
  }

  set allBusinessTypes(Map<dynamic, String> value) {
    this._allBusinessTypes = value;
    notifyListeners();
  }

  void resetChecks() {
    _clients['men'] = false;
    _clients['women'] = false;
    _clients['child'] = false;
  }
}
