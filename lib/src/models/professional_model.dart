import 'dart:io';

import 'package:flutter/material.dart';

class ProfessionalModel with ChangeNotifier {
  dynamic _year = 0;
  dynamic _town = 0;
  dynamic _city = 0;
  String _photo = '';
  dynamic _month = 0;
  File _selectedPhoto;
  dynamic _service = 0;
  String _name = 'Juan';
  String _prefix = '+57';
  dynamic _neighborhood = 0;
  bool _availability = true;
  String _rut = '22.876.555';
  List<String> _workPhotos = [];
  String _lastName = 'Peluquero';
  String _phone = '300567876554';
  List<File> _carouselPhotos = [];
  String _email = 'peluquerojuan@gmail.com';
  Map<dynamic, double> _services = {0: 50, 1: 100, 2: 80};
  Map<dynamic, double> _incomes = {0: 100, 1: 100, 2: 200};
  Map<String, Map<String, TimeOfDay>> _schedule = {
    'general': {'start': TimeOfDay.now(), 'end': TimeOfDay.now()},
    'monday': {'start': TimeOfDay.now(), 'end': TimeOfDay.now()},
    'tuesday': {'start': TimeOfDay.now(), 'end': TimeOfDay.now()},
    'wednesday': {'start': TimeOfDay.now(), 'end': TimeOfDay.now()},
    'thursday': {'start': TimeOfDay.now(), 'end': TimeOfDay.now()},
    'friday': {'start': TimeOfDay.now(), 'end': TimeOfDay.now()},
    'saturday': {'start': TimeOfDay.now(), 'end': TimeOfDay.now()},
    'sunday': {'start': TimeOfDay.now(), 'end': TimeOfDay.now()}
  };
  List<Map<String, dynamic>> _notifications = [
    {
      'type': 0, // Service request
      'read': true,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 1, // Service rating
      'read': false,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 2, // Service cancellation
      'read': false,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 0, // Service request
      'read': true,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 0, // Service request
      'read': false,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    },
    {
      'type': 1, // Service rating
      'read': true,
      'service': 'manicure',
      'date': DateTime.now(),
      'place': 'Cra 54 d 155-65'
    }
  ];

  String get rut => this._rut;

  String get name => this._name;

  dynamic get year => this._year;

  dynamic get town => this._town;

  dynamic get city => this._city;

  String get photo => this._photo;

  String get phone => this._phone;

  String get email => this._email;

  dynamic get month => this._month;

  String get prefix => this._prefix;

  dynamic get service => this._service;

  String get lastName => this._lastName;

  bool get availability => this._availability;

  File get selectedPhoto => this._selectedPhoto;

  dynamic get neighborhood => this._neighborhood;

  List<String> get workPhotos => this._workPhotos;

  Map<dynamic, double> get incomes => this._incomes;

  Map<dynamic, double> get services => this._services;

  List<File> get carouselPhotos => this._carouselPhotos;

  Map<String, Map<String, TimeOfDay>> get schedule => this._schedule;

  List<Map<String, dynamic>> get notifications => this._notifications;

  set rut(String value) {
    this._rut = value;
    notifyListeners();
  }

  set name(String value) {
    this._name = value;
    notifyListeners();
  }

  set city(dynamic value) {
    this._city = value;
    notifyListeners();
  }

  set town(dynamic value) {
    this._town = value;
    notifyListeners();
  }

  set year(dynamic value) {
    this._year = value;
    notifyListeners();
  }

  set photo(String value) {
    this._photo = value;
    notifyListeners();
  }

  set phone(String value) {
    this._phone = value;
    notifyListeners();
  }

  set email(String value) {
    this._email = value;
    notifyListeners();
  }

  set month(dynamic value) {
    this._month = value;
    notifyListeners();
  }

  set prefix(String value) {
    this._prefix = value;
    notifyListeners();
  }

  set service(dynamic value) {
    this._service = value;
    notifyListeners();
  }

  set lastName(String value) {
    this._lastName = value;
    notifyListeners();
  }

  set availability(bool value) {
    this._availability = value;
    notifyListeners();
  }

  set selectedPhoto(File value) {
    this._selectedPhoto = value;
    notifyListeners();
  }

  set neighborhood(dynamic value) {
    this._neighborhood = value;
    notifyListeners();
  }

  set workPhotos(List<String> value) {
    this._workPhotos = value;
    notifyListeners();
  }

  set incomes(Map<dynamic, double> value) {
    this._incomes = value;
    notifyListeners();
  }

  set services(Map<dynamic, double> value) {
    this._services = value;
    notifyListeners();
  }

  set carouselPhotos(List<File> value) {
    this._carouselPhotos = value;
    notifyListeners();
  }

  set notifications(List<Map<String, dynamic>> value) {
    this._notifications = value;
    notifyListeners();
  }

  set schedule(Map<String, Map<String, TimeOfDay>> value) {
    this._schedule = value;
    notifyListeners();
  }

  void reset() {
    rut = null; 
    email = null; 
    phone = null; 
    lastName = null; 
    photo = null; 
    workPhotos.clear();
    carouselPhotos.clear();
    notifyListeners();
  }
}