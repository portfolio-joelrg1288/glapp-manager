import 'package:flutter/material.dart';

import 'package:glapp_manager/src/providers/generic_provider.dart';

class RequestModel {
  final String path;
  final dynamic body;
  final HTTPMethod method;
  final Map<String, String> headers;

  RequestModel({
    this.body,
    @required this.path,
    @required this.method,
    this.headers = const {},
  });
}
