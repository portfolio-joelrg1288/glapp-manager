class ResponseModel {
  final bool success;
  final dynamic body;
  final int statusCode;
  final String message;

  ResponseModel({
    this.message = '',
    this.success = true,
    this.body = const {},
    this.statusCode = 200,
  });
}
