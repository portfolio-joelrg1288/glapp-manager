import 'package:flutter/material.dart';

class AppointmentModel with ChangeNotifier {
  double _total = 0.0;
  double _rating = 0.0;
  String _payment = 'card';
  String _status = 'cancelled';
  String _service = 'manicure';
  String _client = 'Esperanza Gómez';
  String _place = 'Domicilio Cra 54D #135-65';
  String _endDate = DateTime.now().toIso8601String();
  String _startDate = DateTime.now().toIso8601String();
  String _startHour = '12:00';
  String _endHour = '12:00';
  List _serviceList = [];
  String _appointmentId = '';
  int _totalPrice = 0;
  int _discountPrice = 0;
  String _comments = '';
  String _image = '';
  String _employeeId = '';
  String _employeeName = '';

  String get place => this._place;

  double get total => this._total;

  double get rating => this._rating;

  String get status => this._status;

  String get client => this._client;

  String get service => this._service;

  String get endDate => this._endDate;

  String get payment => this._payment;

  String get startDate => this._startDate;

  String get startHour => this._startHour;

  String get endHour => this._endHour;

  List get serviceList => this._serviceList;

  String get appointmentId => this._appointmentId;

  int get totalPrice => this._totalPrice;

  int get discountPrice => this._discountPrice;

  String get comments => this._comments;

  String get image => this._image;

  String get employeeId => this._employeeId;

  String get employeeName => this._employeeName;


  set place(String value) {
    this._place = value;
    notifyListeners();
  }

  set total(double value) {
    this._total = value;
    notifyListeners();
  }

  set rating(double value) {
    this._rating = value;
    notifyListeners();
  }

  set status(String value) {
    this._status = value;
    notifyListeners();
  }

  set client(String value) {
    this._client = value;
    notifyListeners();
  }

  set service(String value) {
    this._service = value;
    notifyListeners();
  }

  set endDate(String value) {
    this._endDate = value;
    notifyListeners();
  }

  set payment(String value) {
    this._payment = value;
    notifyListeners();
  }

  set startDate(String value) {
    this._startDate = value;
    notifyListeners();
  }

  set startHour(String value) {
    this._startHour = value;
    notifyListeners();
  }

  set endHour(String value) {
    this._endHour = value;
    notifyListeners();
  }

  set serviceList(List value) {
    this._serviceList = value;
    notifyListeners();
  }

  set appointmentId(String value) {
    this._appointmentId = value;
    notifyListeners();
  }

  set totalPrice(int value) {
    this._totalPrice = value;
    notifyListeners();
  }

  set discountPrice(int value) {
    this._discountPrice =value;
    notifyListeners();
  }

  set comments(String value) {
    this._comments = value;
    notifyListeners();
  }

  set image(String value) {
    this._image = value;
    notifyListeners();
  }

  set employeeId(String value) {
    this._employeeId = value;
    notifyListeners();
  }

  set employeeName(String value) {
    this._employeeName = value;
    notifyListeners();
  }

  Map<String, dynamic> toJson() => {
        'status': _status,
        'place': _place,
        'client': _client,
        'rating': _rating,
        'service': _service,
        'endDate': _endDate,
        'payment': _payment,
        'startDate': _startDate,
        'startHour': _startHour,
        'endHour': _endHour,
        'serviceList': _serviceList,
        'appointmentId': _appointmentId,
        'totalPrice': _totalPrice,
        'discountPrice': _discountPrice,
        'comments': _comments,
        'image': _image,
        'employeeId': _employeeId,
        'employeeName': _employeeName,
      };
}
