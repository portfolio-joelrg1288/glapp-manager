class ScheduleModel {
  bool availabilty;
  List<dynamic> schedules;

  ScheduleModel({
    this.availabilty,
    this.schedules
  });

  ScheduleModel.fromJsonMap(Map<String, dynamic> json) {
    availabilty = json['disponible'];
    schedules = json['horarios'];
  }
}