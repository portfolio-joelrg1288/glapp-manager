import 'package:flutter/material.dart';

class ErrorModel with ChangeNotifier {
  bool _terms;
  String _name;
  String _rut;
  bool _privacy;
  String _email;
  String _address;
  String _lastName;
  String _password;
  String _phoneNumber;
  String _repeatPassword;
  String _countryCustomField1;
  String _countryCustomField2;

  String get rut => this._rut;

  String get name => this._name;

  bool get terms => this._terms;

  String get email => this._email;

  bool get privacy => this._privacy;

  String get address => this._address;

  String get password => this._password;

  String get lastName => this._lastName;

  String get phoneNumber => this._phoneNumber;

  String get repeatPassword => this._repeatPassword;

  String get countryCustomField1 => this._countryCustomField1;

  String get countryCustomField2 => this._countryCustomField2;

  set rut(String value) {
    this._rut = value;
    notifyListeners();
  }

  set name(String value) {
    this._name = value;
    notifyListeners();
  }

  set terms(bool value) {
    this._terms = value;
    notifyListeners();
  }

  set email(String value) {
    this._email = value;
    notifyListeners();
  }

  set privacy(bool value) {
    this._privacy = value;
    notifyListeners();
  }

  set address(String value) {
    this._address = value;
    notifyListeners();
  }

  set password(String value) {
    this._password = value;
    notifyListeners();
  }

  set lastName(String value) {
    this._lastName = value;
    notifyListeners();
  }

  set phoneNumber(String value) {
    this._phoneNumber = value;
    notifyListeners();
  }

  set repeatPassword(String value) {
    this._repeatPassword = value;
    notifyListeners();
  }

  set countryCustomField1(String value) {
    this._countryCustomField1 = value;
    notifyListeners();
  }

  set countryCustomField2(String value) {
    this._countryCustomField2 = value;
    notifyListeners();
  }

  void reset() {
    terms = null;
    name = null;
    rut = null;
    privacy = null;
    email = null;
    address = null;
    lastName = null;
    password = null;
    phoneNumber = null;
    repeatPassword = null;
    countryCustomField1 = null;
    countryCustomField2 = null;
  }
}
