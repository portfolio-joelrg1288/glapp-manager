class ServicesTaxModel {
  String principalService;
  String serviceName;
  String id;
  dynamic man;
  dynamic woman;
  dynamic child;

  ServicesTaxModel({
    this.principalService = '',
    this.serviceName = '',
    this.id = '',
    this.man = 0,
    this.woman = 0,
    this.child = 0
  });

  ServicesTaxModel.fromJsonMap(Map<String, dynamic> json) {
    principalService = json['servicio_principal'];
    serviceName = json['nombre_servicio'];
    id = json['id'];
    man = json['hombre'];
    woman = json['mujer'];
    child = json['niño'];
  }
}