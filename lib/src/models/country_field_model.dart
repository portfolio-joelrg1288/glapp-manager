import 'package:flutter/material.dart';

class CountryFieldModel extends ChangeNotifier {
  List<dynamic> _localities = new List();
  String _field = '';
  String _listValue = '';
  bool _visible = false;
  bool _visible2 = false;

  List<dynamic> get localities => this._localities;

  String get field => this._field;

  String get listValue => this._listValue;

  bool get visible => this._visible;

  bool get visible2 => this._visible2;

  set localities(List<dynamic> value) {
    this._localities = value;
    notifyListeners();
  }

  set field(String value) {
    this._field = value;
    notifyListeners();
  }

  set listValue(String value) {
    this._listValue = value;
    notifyListeners();
  }

  set visible(bool value) {
    this._visible = value;
    notifyListeners();
  }

  set visible2(bool value) {
    this._visible2 = value;
    notifyListeners();
  }

  void reset() {
    localities.clear();
    field = '';
    visible = false;
    visible2 = false;
    notifyListeners();
  }
}