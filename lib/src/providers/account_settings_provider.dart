import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:glapp_manager/src/models/response_model.dart';
import 'generic_provider.dart';

class AccountSettingsProvider {
  final dotenv = DotEnv();
  final storage = new FlutterSecureStorage();
  final GenericProvider genericProvider = new GenericProvider();

  Future<ResponseModel> addServices(Map<String, dynamic> body) async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${dotenv.env['SERVICE_ENDPOINT']}';
    final resp = await genericProvider.post(path, body: body);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> getSchedules() async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['SCHEDULES_ENDPOINT']}';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> updateSchedules(Map<String, dynamic> body) async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['SCHEDULES_ENDPOINT']}';
    final resp = await genericProvider.put(path, body: body);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  } 

  Future<ResponseModel> getPlacesToAttention() async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['PLACES_TO_ATTENTION']}';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> updatePlace(Map<String, dynamic> body) async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['PLACES_TO_ATTENTION']}';
    final resp = await genericProvider.post(path, body: body);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }
}