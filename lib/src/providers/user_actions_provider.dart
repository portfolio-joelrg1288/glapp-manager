import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/providers/generic_provider.dart';

class UserActionsProvider {
  final dotenv = DotEnv();
  final GenericProvider genericProvider = new GenericProvider();

   Future<ResponseModel> getUserInfo() async {
    final path =
      '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${dotenv.env['INFO_ENDPOINT']}';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200);
  }

  Future<ResponseModel> updateInfo(Map<String, dynamic> body) async {
    final path =
      '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${dotenv.env['INFO_ENDPOINT']}';
    final resp = await genericProvider.put(path, body: body);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200);
  }

  Future<ResponseModel> getLocalities(String countryPhonePrefix) async {
    final path =
      '${dotenv.env['API_URL']}${dotenv.env['PROVINCES_ENDPOINT']}?${dotenv.env['COUNTRY_GET']}=$countryPhonePrefix';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> getEmployees() async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['EMPLOYEES_ENDPOINT']}';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> setLocation(Map<String, dynamic> body) async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${dotenv.env['UBICATION_ENDPOINT']}';
    final resp = await genericProvider.post(path, body: body);
    print(resp.reasonPhrase);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

}