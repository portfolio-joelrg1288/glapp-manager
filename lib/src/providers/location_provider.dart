import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/providers/user_actions_provider.dart';
import 'package:provider/provider.dart';

class LocationProvider {

  FlutterSecureStorage _storage = new FlutterSecureStorage();
  // Defines the geolocalization service. desiredAccuracy refers to location's precision. distanceFilter
  // controls the minimum distance the device needs to move before the update is emitted.
  final positionStream = getPositionStream(desiredAccuracy: LocationAccuracy.high, distanceFilter: 50);
  StreamSubscription<Position> positionStreamSubscription;
  UserActionsProvider userProvider = new UserActionsProvider(); // provider to send the profesional's location to the server

  // ======================================================================
  // Singleton
  // ======================================================================
  static LocationProvider _instance; // Unique instance of LocationProvider
  LocationProvider._internal();
  factory LocationProvider(){
    if(_instance == null)
      _instance = new LocationProvider._internal();
    return _instance;
  }
  // ======================================================================

  void initGeolocation(BuildContext context) async{
    final DataModel dataModel = Provider.of<DataModel>(context);// Gets the user data
    final user = await _storage.read(key: 'user');
    final token = user != null ? json.decode(user)['token'] : null;

    //If there is token in secureStorage and the user logged is a freelancer
    //the app will listen to the location coordinates and will set in the server
    if(token != null && dataModel.isProfessional){
      positionStreamSubscription = positionStream.listen((position) {
        if(position != null){
          userProvider.setLocation({
            "latitud": position.latitude,
            "longitud": position.longitude
          });
        }
      });
    } else {
      //If there is no token and there is an active subscription, the subscription will be cancelled
      positionStreamSubscription?.cancel();
    }
  }
}