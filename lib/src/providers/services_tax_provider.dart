import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/providers/generic_provider.dart';

class ServicesTaxProvider {
  DotEnv dotEnv = DotEnv();
  GenericProvider genericProvider = GenericProvider();

  Future<ResponseModel> getServices() async {
    final path =
    '${dotEnv.env['API_URL']}${dotEnv.env['SERVICES_ENDPOINT']}';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200
    );
  }

  Future<ResponseModel> deleteService(String id) async {
    final path = 
    '${dotEnv.env['API_URL']}${dotEnv.env['SERVICE_ENDPOINT']}/$id';
    final resp = await genericProvider.delete(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200
    );
  }
}