import 'dart:io';
import 'dart:convert';
import 'package:http/http.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:glapp_manager/src/models/request_model.dart';

enum HTTPMethod { GET, POST, PATCH, DELETE, PUT}

class GenericProvider extends SimpleProvider {
  @override
  Future<Response> sendRequest(RequestModel req) {
    return super.sendRequest(req);
  }
}

class SimpleProvider {
  final Client client;
  final storage = new FlutterSecureStorage();

  SimpleProvider({httpClient})
      : client = httpClient == null ? new Client() : httpClient;

  dynamic getMethodCall(HTTPMethod method) {
    switch (method) {
      case HTTPMethod.POST:
        return this.client.post;
      case HTTPMethod.GET:
        return this.client.get;
      case HTTPMethod.DELETE:
        return this.client.delete;
      case HTTPMethod.PATCH:
        return this.client.patch;
      case HTTPMethod.PUT:
        return this.client.put;
      default:
        return this.client.get;
    }
  }

  Future<Response> get(String path, {Map<String, String> headers}) {
    return sendRequest(
      new RequestModel(method: HTTPMethod.GET, path: path, headers: headers),
    );
  }

  Future<Response> post(String path,
      {dynamic body, Map<String, String> headers}) {
    return sendRequest(
      new RequestModel(
        body: body,
        path: path,
        headers: headers,
        method: HTTPMethod.POST,
      ),
    );
  }

  Future<Response> delete(String path, {Map<String, String> headers}) {
    return sendRequest(
      new RequestModel(method: HTTPMethod.DELETE, path: path, headers: headers),
    );
  }

  Future<Response> patch(String path,
      {dynamic body, Map<String, String> headers}) {
    return sendRequest(
      new RequestModel(
        body: body,
        path: path,
        headers: headers,
        method: HTTPMethod.PATCH,
      ),
    );
  }

  Future<Response> put(String path, {dynamic body, Map<String, String> headers}) {
    return sendRequest(
      new RequestModel(
        body: body,
        path: path,
        headers: headers,
        method: HTTPMethod.PUT,
      ),
    );
  }

  Future<Response> sendRequest(RequestModel req) async {
    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
      HttpHeaders.acceptCharsetHeader: 'application/json',
      HttpHeaders.acceptEncodingHeader: 'application/json',
    };
    String value = await _getToken();
    if (value != null) {
      headers.addAll({HttpHeaders.authorizationHeader: 'Bearer $value'});
    }
    if (req.headers != null) headers.addAll(req.headers);
    Function methodClient = getMethodCall(req.method);
    if (req.body != null)
      return methodClient(req.path,
          headers: headers, body: json.encode(req.body));
    else
      return methodClient(req.path, headers: headers);
  }

  Future<String> _getToken() async {
    final user = await storage.read(key: 'user');
    if (user != null)
      return json.decode(user)['token'];
    else
      return null;
  }
}
