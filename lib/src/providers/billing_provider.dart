import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/providers/generic_provider.dart';

class BillingProvider {
  DotEnv dotEnv = DotEnv();
  GenericProvider genericProvider = GenericProvider();

  Future<ResponseModel> getBilling(String queryParameter) async {
    final path = 
    '${dotEnv.env['API_URL']}${dotEnv.env['PROFESSIONALS_ENDPOINT']}/${dotEnv.env['BILLING_ENDPOINT']}?year=$queryParameter';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200
    );
  }

  Future<ResponseModel> getBillingMonth(String year, String month) async {
    final path = 
    '${dotEnv.env['API_URL']}${dotEnv.env['PROFESSIONALS_ENDPOINT']}/${dotEnv.env['BILLING_MONTH_ENDPOINT']}${dotEnv.env['MONTH_ENDPOINT']}?year=$year&mes=$month';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200
    );
  }
}