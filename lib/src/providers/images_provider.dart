import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:flutter_absolute_path/flutter_absolute_path.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart';
import 'dart:convert';

import 'package:glapp_manager/src/models/data_model.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class ImagesProvider {
  DotEnv dotEnv = DotEnv();
  FlutterSecureStorage storage = new FlutterSecureStorage();

  Future<void> showOptionsDialog(BuildContext context, int options, int quantityPhotosToPick, int fromWhere) {
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Center(child: Text('Selección de Foto')),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
          actions: <Widget>[
            options == 1 ? IconButton(
              padding: MediaQuery.of(context).viewInsets +
                const EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
              icon: Icon(Icons.photo_camera),
              iconSize: 70,
              onPressed: () {
                _takePicture(context);
              },
            ) : Center(
                child: Container(
                  padding: MediaQuery.of(context).viewInsets +
                    const EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
                  child: Text( 'Solo podrás añadir $quantityPhotosToPick fotos')
                ),
              ),
            IconButton(
              padding: MediaQuery.of(context).viewInsets +
                const EdgeInsets.symmetric(horizontal: 25.0, vertical: 20.0),
              icon: Icon(Icons.photo_album),
              iconSize: 70,
              onPressed: () {
                _openGallery(context, quantityPhotosToPick, fromWhere);
              },
            ),
          ],
        );
      }
    );
  }

  void _openGallery(BuildContext context, int quantityPhotosToPick, int fromWhere) async {
    var imageList;
    try {
      imageList = await MultiImagePicker.pickImages(
        maxImages: quantityPhotosToPick,
        enableCamera: false 
        );
    } on Exception catch (e) {
      imageList = null;
    }

    final dataModel = Provider.of<DataModel>(context, listen: false);
    if (imageList == null) {
      dataModel.selectedPictures = null;
      dataModel.selectedPicture = null;
      Navigator.of(context).pop();
      return;
    }else {
      final aux = await _convertAssetToFile(imageList);
      fromWhere == 1 ? dataModel.selectedPicture = aux[0] : dataModel.selectedPictures = aux;
    }
    
    Navigator.of(context).pop();
  }

  Future<List<File>> _convertAssetToFile(var imageList) async {
    List<File> files = new List();
    for(int i = 0; i < imageList.length; i++) {
      final path = await FlutterAbsolutePath.getAbsolutePath(imageList[i].identifier);
      files.add(File(path));
    }

    return files;
  }

  void _takePicture(BuildContext context) async {
    final dataModel = Provider.of<DataModel>(context, listen: false);
    final image = await ImagePicker().getImage(
        source: ImageSource.camera,
        maxHeight: 1024,
        maxWidth: 1024,
        imageQuality: 100);
    if (image == null) return;
    else dataModel.selectedPicture = File(image.path);
    Navigator.of(context).pop();
  }

  Future<StreamedResponse> uploadProfilePhoto(BuildContext context) async {
    final dataModel = Provider.of<DataModel>(context, listen: false);

    final path = 
      '${dotEnv.env['API_URL']}${dotEnv.env['PROFESSIONAL_ENDPOINT']}${dotEnv.env['IMAGES_ENDPOINT']}${dotEnv.env['PROFILE_ENDPOINT']}';
    var request = MultipartRequest('POST', Uri.parse(path));
    final user = await storage.read(key: 'user');
    final token = json.decode(user)['token'];
    final Map<String, String> headers = {HttpHeaders.authorizationHeader : 'Bearer $token'};
    
    request.headers.addAll(headers);
    request.files.add(await MultipartFile.fromPath('file', dataModel.selectedPicture.path));
    
    var res = await request.send();
    
    return res;
  }

  Future<StreamedResponse> uploadCarousel(var files) async {
    final path = 
      '${dotEnv.env['API_URL']}${dotEnv.env['PROFESSIONAL_ENDPOINT']}${dotEnv.env['IMAGES_ENDPOINT']}${dotEnv.env['MOBILE_ENDPOINT']}';
    
    final user = await storage.read(key: 'user');
    final token = json.decode(user)['token'];

    var request = MultipartRequest('POST', Uri.parse(path));

    final Map<String, String> headers = {
      HttpHeaders.authorizationHeader : 'Bearer $token',
      HttpHeaders.contentTypeHeader : 'multipart/form-data'
    };
    
    request.headers.addAll(headers);
 
    for(int i = 0; i < files.length; i++) {
      request.files.add(await MultipartFile.fromPath(
        'onb${i + 1}', 
        files[i].path, 
        filename: files[i].path.split('/').last));
    }
    
    var response = await request.send();

    return response;
  }
}