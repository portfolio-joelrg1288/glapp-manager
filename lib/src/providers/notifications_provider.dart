import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/providers/generic_provider.dart';

class NotificationsProvider {
  final dotEnv = DotEnv();
  final GenericProvider genericProvider = new GenericProvider();

  Future<ResponseModel> getNotifications(String queryParameter) async {
    final path = 
    '${dotEnv.env['API_URL']}${dotEnv.env['NOTIFICATIONS_ENDPOINT']}${dotEnv.env['PROFESSIONALS_ENDPOINT']}?filter=$queryParameter';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200
    );
  }

  Future<ResponseModel> updateNotificationState(String id) async {
    final path = 
    '${dotEnv.env['API_URL']}${dotEnv.env['NOTIFICATIONS_ENDPOINT']}$id/${dotEnv.env['STATE_ENDPOINT']}';
    final resp = await genericProvider.put(path);
    return ResponseModel(
        message: resp.reasonPhrase,
        statusCode: resp.statusCode,
        body: json.decode(resp.body),
        success: resp.statusCode == 200
    );
  }
}