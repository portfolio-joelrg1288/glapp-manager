import 'dart:convert';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/providers/generic_provider.dart';

class AuthProvider {
  final dotenv = DotEnv();
  final storage = new FlutterSecureStorage();
  final GenericProvider genericProvider = new GenericProvider();

  Future<ResponseModel> login(Map<String, String> body) async {
    final path =
        '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${dotenv.env['LOGIN_ENDPOINT']}';
    final resp = await genericProvider.post(path, body: body);
    if (resp.statusCode == 200)
      await storage.write(key: 'user', value: resp.body);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> signupLocal(Map<String, String> body) async {
    final path =
        '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${dotenv.env['SIGNUP_ENDPOINT']}${dotenv.env['LOCAL_ENDPOINT']}';
    final resp = await genericProvider.post(path, body: body);
    return ResponseModel(
        message: resp.reasonPhrase,
        statusCode: resp.statusCode,
        body: json.decode(resp.body),
        success: resp.statusCode == 201);
  }

  Future<ResponseModel> signupFreelance(Map<String, String> body) async {
    final path =
        '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${dotenv.env['SIGNUP_ENDPOINT']}${dotenv.env['FREELANCE_ENDPOINT']}';
    final resp = await genericProvider.post(path, body: body);
    return ResponseModel(
        message: resp.reasonPhrase,
        statusCode: resp.statusCode,
        body: json.decode(resp.body),
        success: resp.statusCode == 201);
  }
}
