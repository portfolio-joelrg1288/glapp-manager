import 'dart:convert';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:glapp_manager/src/models/response_model.dart';
import 'package:glapp_manager/src/providers/generic_provider.dart';

class AppointmentsProvider {
  final dotenv = DotEnv();
  final storage = new FlutterSecureStorage();
  final GenericProvider genericProvider = new GenericProvider();

  Future<ResponseModel> getAppointments() async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${dotenv.env['APPOINTMENTS_ENDPOINT']}';
    final resp = await genericProvider.get(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> appointmentAction(String appointmentId, String action) async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['BOOKING_ENDPOINT']}$appointmentId/$action';
    final resp = await genericProvider.put(path);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> finishAppointment(String appointmentId, Map<String, dynamic> body) async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['BOOKING_ENDPOINT']}$appointmentId/pago';
    final resp = await genericProvider.put(path, body: body);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

  Future<ResponseModel> lockSchedule(Map<String, dynamic> body) async {
    final path = '${dotenv.env['API_URL']}${dotenv.env['BOOKING_ENDPOINT']}bloqueo/mobile';
    final resp = await genericProvider.post(path, body: body);
    return ResponseModel(
      message: resp.reasonPhrase,
      statusCode: resp.statusCode,
      body: json.decode(resp.body),
      success: resp.statusCode == 200,
    );
  }

}