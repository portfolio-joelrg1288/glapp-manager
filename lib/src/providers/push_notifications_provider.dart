import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:jaguar_jwt/jaguar_jwt.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class PushNotificationsProvider {

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final _messagesStreamController = StreamController<String>.broadcast();
  FlutterSecureStorage storage = new FlutterSecureStorage();
  final dotenv = DotEnv();

  Stream<String> get mensajesStream => _messagesStreamController.stream;

  initNotifications() async {
    // Get user from storage to get the user token
    final user = await storage.read(key: 'user');
    try {
      final token = json.decode(user)['token'];
      final fcmToken = '';
      // Get the user id from the token
      final decClaimSet = verifyJwtHS256Signature(token, "miclavedetokens");
      var client = new http.Client();

      //Config firebaseMessaging
      _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true)
      );
      _firebaseMessaging.onIosSettingsRegistered
      .listen((IosNotificationSettings settings) {});

      //If there is no token in local storage, the app gets the token from firebase and
      //puts this token in the api
      if (fcmToken == null || fcmToken.isEmpty) {
        //The token doesn't change unless the app is uninstalled
        var tokenFcm = await _firebaseMessaging.getToken();
        final path =
          '${dotenv.env['API_URL']}${dotenv.env['PROFESSIONAL_ENDPOINT']}${decClaimSet.subject}/${dotenv.env['FCM_ENDPOINT']}';
        assert(tokenFcm != null); //Validates if the token was gotten succesfuly
        //Saves the token in the api
        client = new http.Client();
        await client.put(
          path,
          body: {"fcmToken": tokenFcm},
          headers: {HttpHeaders.authorizationHeader: "Baerer " + token});
      }
      //Configure actions for the notification
      _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('Message: $message');
          print(message['notification']['title']);
          print(message['notification']['body']);
        },
        onBackgroundMessage: onBackgroundMessage,
        onLaunch: (Map<String, dynamic> message) async {},
        onResume: (Map<String, dynamic> message) async {},
      );
    } on NoSuchMethodError {
      print("There is no token");
    }
  }

  static Future<dynamic> onBackgroundMessage(Map<String, dynamic> message) async {

    if (message.containsKey('data')) {
      // Handle data message
      final dynamic data = message['data'];
    }

    if (message.containsKey('notification')) {
      // Handle notification message
      final dynamic notification = message['notification'];
    }

    // Or do other work.
  }

  Future<dynamic> onMessage(Map<String, dynamic> message) async {

    print('====== onMessage ====== ');
    print('message: $message');
    // print('argument: $argument');
    String argument = 'no-data';

    if ( Platform.isAndroid ) {
      argument = message['data']['comida'] ?? 'no-data';
    } else {
      argument = message['comida'] ?? 'no-data';
    }

    _messagesStreamController.sink.add( argument );

  }

  Future<dynamic> onLaunch(Map<String, dynamic> message) async {

    print('====== onLaunch ====== ');
    // print('message: $message');
    // print('argument: $argument');
    String argument = 'no-data';

    if ( Platform.isAndroid ) {
      argument = message['data']['comida'] ?? 'no-data';
    } else {
      argument = message['comida'] ?? 'no-data';
    }

    _messagesStreamController.sink.add( argument );
  }

  Future<dynamic> onResume(Map<String, dynamic> message) async {

    print('====== onResume ====== ');
    // print('message: $message');
    // print('argument: $argument');
    String argument = 'no-data';

    if ( Platform.isAndroid ) {
      argument = message['data']['comida'] ?? 'no-data';
    } else {
      argument = message['comida'] ?? 'no-data';
    }

    _messagesStreamController.sink.add( argument );


  }


  dispose() {
    _messagesStreamController?.close();
  }

}