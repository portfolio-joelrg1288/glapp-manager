import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

import 'package:glapp_manager/src/pages/home_page.dart';
import 'package:glapp_manager/src/utils/constants.dart';
import 'package:glapp_manager/src/pages/login_page.dart';
import 'package:glapp_manager/src/models/data_model.dart';
import 'package:glapp_manager/src/pages/profile_page.dart';
import 'package:glapp_manager/src/models/error_model.dart';
import 'package:glapp_manager/src/models/add_services_model.dart';
import 'package:glapp_manager/src/providers/location_provider.dart';
import 'package:glapp_manager/src/providers/push_notifications_provider.dart';
import 'package:glapp_manager/src/models/filters_model.dart';
import 'package:glapp_manager/src/models/business_model.dart';
import 'package:glapp_manager/src/models/menu_item_model.dart';
import 'package:glapp_manager/src/pages/appointment_page.dart';
import 'package:glapp_manager/src/pages/notification_page.dart';
import 'package:glapp_manager/src/pages/month_incomes_page.dart';
import 'package:glapp_manager/src/pages/register_user_page.dart';
import 'package:glapp_manager/src/pages/splash_screen_page.dart';
import 'package:glapp_manager/src/pages/schedule_lock_page.dart';
import 'package:glapp_manager/src/models/appointment_model.dart';
import 'package:glapp_manager/src/models/professional_model.dart';
import 'package:glapp_manager/src/models/country_field_model.dart';
import 'package:glapp_manager/src/pages/register_success_page.dart';
import 'package:glapp_manager/src/pages/register_options_page.dart';
import 'package:glapp_manager/src/pages/account_settings_page.dart';
import 'package:glapp_manager/src/pages/register_business_page.dart';

Future main() async {
  await DotEnv().load('.env');
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => new DataModel()),
      ChangeNotifierProvider(create: (_) => new ErrorModel()),
      ChangeNotifierProvider(create: (_) => new FiltersModel()),
      ChangeNotifierProvider(create: (_) => new MenuItemModel()),
      ChangeNotifierProvider(create: (_) => new BusinessModel()),
      ChangeNotifierProvider(create: (_) => new AppointmentModel()),
      ChangeNotifierProvider(create: (_) => new ProfessionalModel()),
      ChangeNotifierProvider(create: (_) => new CountryFieldModel()),
      ChangeNotifierProvider(create: (_) => new AddServicesModel()),
    ],
    child: Constants(child: MyApp()),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  @override
  void didChangeDependencies() {
    final pushProvider = PushNotificationsProvider();
    final locationProvider = LocationProvider(); //gets the instance of location provider
    pushProvider.initNotifications();
    locationProvider.initGeolocation(context);
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Glapp Manager',
      theme: ThemeData(
        fontFamily: 'OpenSans',
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),

      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],

      supportedLocales: [
        const Locale('en', 'US'),
        const Locale('es', 'ES'), 
      ],

      initialRoute: 'splash',
      routes: {
        'home': (BuildContext context) => HomePage(),
        'login': (BuildContext context) => LoginPage(),
        'profile': (BuildContext context) => ProfilePage(),
        'splash': (BuildContext context) => SplashScreenPage(),
        'notification': (BuildContext context) => NotificationPage(),
        'register-user': (BuildContext context) => RegisterUserPage(),
        'schedule-lock': (BuildContext context) => ScheduleLockPage(),
        'month-incomes': (BuildContext context) => MonthIncomesPage(),
        'appointment-page': (BuildContext context) => AppointmentPage(),
        'register-options': (BuildContext context) => RegisterOptionsPage(),
        'register-success': (BuildContext context) => RegisterSuccessPage(),
        'account-settings': (BuildContext context) => AccountSettingsPage(),
        'register-business': (BuildContext context) => RegisterBussinesPage(),
      },
    );
  }
}
